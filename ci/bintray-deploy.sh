#!/usr/bin/env bash

. ./ci/variables.sh

if [[ -n "${CI_COMMIT_TAG}" ]]; then
  ./gradlew bintrayUpload -Pversion=${APP_VERSION} --info
fi

