#!/usr/bin/env bash

./ci/prepare-db.sh
. ./ci/variables.sh

./gradlew build -Pversion=${APP_VERSION} --info
