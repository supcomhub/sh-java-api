#!/usr/bin/env bash

./ci/prepare-docker.sh

git clone --depth 1 https://gitlab-ci-token:${CI_JOB_TOKEN}@gitlab.com/supcomhub/sh-stack.git -b develop

pushd sh-stack
cp -r config.template config
cp .env.template .env
./scripts/init-core-db.sh
./scripts/init-shared-mysql-db.sh
popd
