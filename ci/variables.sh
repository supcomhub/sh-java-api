#!/usr/bin/env bash

APP_VERSION=$([[ -n "${CI_COMMIT_TAG}" ]] && echo "${CI_COMMIT_TAG#*v}" || echo "${CI_COMMIT_REF_NAME##*/}")
