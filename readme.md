# SupCom Hub REST API
 
This is the official SupCom Hub API. Amongst others, the API offers the following functionality:
- OAuth 2.0 compliant authentication server
- User management (registration, password reset, etc.)
- [JSON-API](https://jsonapi.org/) compliant REST API for lots of community data (i.e. map, mod & replay vault, game statistics, etc.)
- Leaderboards
- Clan management
- Vault upload functionality
- Internal SupCom Hub featured mod deployment
- [Challonge](https://challonge.com) proxy
 
## How to run

### From Source

#### Using IntelliJ

1. Clone the repository
1. Import as Gradle Project into IntelliJ. As this overwrites the `.idea` folder, please revert all deleted files after import (Version Control (Alt+F9) -> Local Changes)
1. Configure your JDK if you haven't already
1. Make sure you have the [IntelliJ Lombok plugin](https://plugins.jetbrains.com/plugin/6317-lombok-plugin) installed
1. Set up an [SH core database](https://gitlab.com/supcomhub/sh-core-db-migrations#usage-with-sh-stack).
1. Launch `ShApiApplication`

#### Using Gradle

1. Clone the repository
1. Make sure your JAVA_HOME points to the correct JDK
1. Set up an [SH core database](https://gitlab.com/supcomhub/sh-core-db-migrations#usage-with-sh-stack).
1. (TODO: make sure the application plugin is configured correctly) Execute `.\gradlew.bat run` (Windows) or `./gradlew run` (Linux)
 
### Using Docker

Given the number of required configuration values, it's easiest to run the API using [sh-stack](https://gitlab.com/supcomhub/sh-stack):

    docker-compose up -d sh-java-api

## Sample Routes

* [API documentation](http://localhost:8010)
* [List event definitions](http://localhost:8010/data/event)
* [List 5 maps with more than 8 players](http://localhost:8010/data/mapVersion?filter=(maxPlayers=gt=8)&page[size]=5)
* [List UI mods, sorted by last updated ascending](http://localhost:8010/data/modVersion?filter=(type=='UI')&sort=-updateTime)
* [List all accounts](http://localhost:8010/data/account)
* [List events of players](http://localhost:8010/data/playerEvent)
* [List replays](http://localhost:8010/data/game?filter=(endTime=isnull=true))

## Technology Stack

This project uses:

* Java 8 as the programming language
* [Spring Boot](https://projects.spring.io/spring-boot/) as framework
* [Hibernate ORM](http://hibernate.org/orm/) as ORM mapper
* [Elide](http://elide.io/) with [RSQL filters](http://elide.io/pages/guide/08-filters.html#rsql) to serve [JSON-API](http://jsonapi.org/) conform data
* [Gradle](https://gradle.org/) as build automation tool
* [Docker](https://www.docker.com/) to deploy and run the application
* [Swagger](http://swagger.io/) as API documentation tool
