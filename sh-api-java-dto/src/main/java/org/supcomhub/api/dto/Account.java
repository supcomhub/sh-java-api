package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
@Setter
@Type(Account.TYPE_NAME)
@FieldNameConstants
public class Account extends AbstractEntity {
  public static final String TYPE_NAME = "account";

  private String displayName;
  @Relationship("nameRecords")
  @JsonManagedReference("nameRecords")
  private List<NameRecord> nameRecords;
  private String userAgent;
  private String steamId;
  private String lastLoginUserAgent;
  private String lastLoginIpAddress;
  private OffsetDateTime lastLogin;
  @Relationship("bans")
  @JsonManagedReference("bans")
  private List<Ban> bans;
  @Relationship("avatarAssignments")
  @JsonIgnore
  private List<AvatarAssignment> avatarAssignments;
  @JsonBackReference("reporterOnModerationReports")
  @Relationship("reporterOnModerationReports")
  private Set<ModerationReport> reporterOnModerationReports;
  @Relationship("emailAddresses")
  @JsonManagedReference("emailAddresses")
  private Set<EmailAddress> emailAddresses = new HashSet<>();
  @Relationship("userNotes")
  private Set<UserNote> userNotes;
  @Relationship("clanMemberships")
  private Set<ClanMembership> clanMemberships = new HashSet<>();
  @Relationship("accountVerifications")
  @JsonManagedReference("accountVerifications")
  private Set<AccountVerification> accountVerifications = new HashSet<>();
  @Relationship("reportedOnModerationReports")
  private Set<ModerationReport> reportedOnModerationReports = new HashSet<>();
  @Relationship("userGroups")
  private Set<UserGroup> userGroups = new HashSet<>();

  @Override
  public String toString() {
    return String.format("%s [id %s]", displayName, id);
  }
}
