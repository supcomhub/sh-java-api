package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type("accountVerification")
@FieldNameConstants
public class AccountVerification extends AbstractEntity {
  @Relationship("account")
  @JsonBackReference("accountVerifications")
  private Account account;
  private Type type;
  private String value;

  public enum Type {
    /**
     * The account was migrated from FAF.
     */
    FAF,
    /**
     * The account holder proved that he also owns a Steam account that has Forged Alliance: Forever. The value to be
     * stored is the Steam account ID.
     */
    STEAM,
    /** The account holder verified his identity via Telegram. The value to be stored in the Telegram ID. */
    TELEGRAM,
    /**
     * The account holder verified himself personally, e.g. is a known member of the community who others have spoken
     * to. The value to be stored is a free text, e.g. "Well-known community member". This is for internal use only and
     * not meant to be displayed to other users.
     */
    PERSONAL
  }
}
