package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@ToString(of = {"name"}, callSuper = true)
@Type("achievement")
@FieldNameConstants
public class Achievement extends AbstractEntity {

  private String description;
  private int experiencePoints;
  private AchievementState initialState;
  private String name;
  private String revealedIconUrl;
  private Integer totalSteps;
  private AchievementType type;
  private String unlockedIconUrl;
  private int order;
}
