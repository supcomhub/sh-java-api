package org.supcomhub.api.dto;


public enum AchievementState {
  HIDDEN,
  REVEALED,
  UNLOCKED
}
