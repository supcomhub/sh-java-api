package org.supcomhub.api.dto;

public enum AchievementType {
  STANDARD,
  INCREMENTAL
}
