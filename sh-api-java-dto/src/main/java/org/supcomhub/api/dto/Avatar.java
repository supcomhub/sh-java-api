package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Type(Avatar.TYPE_NAME)
@Getter
@Setter
@FieldNameConstants
public class Avatar extends AbstractEntity {

  public static final String TYPE_NAME = "avatar";

  private String url;
  private String description;

  @Relationship("assignments")
  @JsonIgnore
  private List<AvatarAssignment> assignments;
}
