package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.time.OffsetDateTime;

@Type(AvatarAssignment.TYPE_NAME)
@Getter
@Setter
@FieldNameConstants
public class AvatarAssignment extends AbstractEntity {

  public static final String TYPE_NAME = "avatarAssignment";

  private Boolean selected;
  private OffsetDateTime expiresAt;

  @Relationship(Account.TYPE_NAME)
  @JsonIgnore
  private Account account;

  @Relationship(Avatar.TYPE_NAME)
  @JsonIgnore
  private Avatar avatar;
}
