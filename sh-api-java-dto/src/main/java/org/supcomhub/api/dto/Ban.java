package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.time.OffsetDateTime;

@Type(Ban.TYPE_NAME)
@RestrictedVisibility("HasBanRead")
@Getter
@Setter
@FieldNameConstants
public class Ban extends AbstractEntity {

  public static final String TYPE_NAME = "ban";

  private String reason;
  private OffsetDateTime expiryTime;
  private OffsetDateTime revocationTime;
  private String revocationReason;
  private BanScope scope;

  @JsonBackReference("bans")
  @Relationship("account")
  private Account account;

  @Relationship("author")
  @JsonIgnore
  private Account author;

  @Relationship("revocationAuthor")
  @JsonIgnore
  private Account revocationAuthor;

  @Relationship("moderationReport")
  @JsonIgnore
  private ModerationReport moderationReport;
}
