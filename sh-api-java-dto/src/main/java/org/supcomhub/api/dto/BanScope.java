package org.supcomhub.api.dto;

public enum BanScope {
  CHAT, GLOBAL
}
