package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Getter
@Setter
@Type("clan")
@FieldNameConstants
public class Clan extends AbstractEntity {

  private String name;
  private String tag;
  private String description;
  private String websiteUrl;

  @Relationship("founder")
  private Account founder;

  @Relationship("leader")
  private Account leader;

  @Relationship("memberships")
  private List<ClanMembership> memberships;
}
