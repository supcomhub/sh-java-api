package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type("clanMembership")
@FieldNameConstants
public class ClanMembership extends AbstractEntity {
  @Relationship("clan")
  private Clan clan;

  @Relationship("member")
  private Account member;
}
