package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Type("coopMission")
@FieldNameConstants
public class CoopMission {
  @Id
  private String id;
  private String name;
  private String description;
  private String category;
  private String downloadUrl;
  private String thumbnailUrlSmall;
  private String thumbnailUrlLarge;
  private CoopMissionType type;

  @Relationship("map")
  private MapVersion map;
}
