package org.supcomhub.api.dto;

public enum CoopMissionType {
  FA, AEON, CYBRAN, UEF, CUSTOM
}
