package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.time.Duration;

@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Type("coopResult")
@FieldNameConstants
public class CoopResult {
  @Id
  private String id;
  private Duration duration;
  private String playerNames;
  private boolean secondaryObjectives;
  private int ranking;
  private int playerCount;

  @Relationship("game")
  private Game game;

  @Relationship("mission")
  private CoopMission mission;
}
