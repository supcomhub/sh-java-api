package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type("emailAddress")
@FieldNameConstants
public class EmailAddress extends AbstractEntity {
  private String plain;
  private String distilled;
  private Boolean primary;
  @JsonBackReference("emailAddresses")
  @Relationship("owner")
  private Account owner;
}
