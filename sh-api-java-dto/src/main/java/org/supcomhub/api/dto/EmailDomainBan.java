package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Type;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@RestrictedVisibility("IsModerator")
@Getter
@Setter
@EqualsAndHashCode(of = "domain", callSuper = false)
@Type(EmailDomainBan.TYPE_NAME)
@FieldNameConstants
public class EmailDomainBan extends AbstractEntity {

  public static final String TYPE_NAME = "emailDomainBan";

  String domain;
}
