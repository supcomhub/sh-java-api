package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(Event.TYPE_NAME)
@FieldNameConstants
public class Event extends AbstractEntity {

  public static final String TYPE_NAME = "event";

  private String name;
  private String imageUrl;
  private Type type;

  public enum Type {
    NUMERIC, TIME
  }
}
