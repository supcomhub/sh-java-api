package org.supcomhub.api.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.FieldNameConstants;

import java.util.HashMap;
import java.util.Map;

@Getter
@AllArgsConstructor
@FieldNameConstants
public enum Faction {
  // Order is crucial
  AEON("aeon"),
  CYBRAN("cybran"),
  UEF("uef"),
  SERAPHIM("seraphim"),
  NOMAD("nomad"),
  CIVILIAN("civilian");

  private static final Map<String, Faction> fromString;

  static {
    fromString = new HashMap<>();
    for (Faction faction : values()) {
      fromString.put(faction.string, faction);
    }
  }

  private final String string;

  public static Faction fromString(String string) {
    return fromString.get(string);
  }
}
