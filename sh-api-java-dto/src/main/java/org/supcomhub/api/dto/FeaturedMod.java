package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(FeaturedMod.TYPE_NAME)
@FieldNameConstants
public class FeaturedMod extends AbstractEntity {
  public static final String TYPE_NAME = "featuredMod";

  private String description;
  private String displayName;
  private int order;
  private String gitBranch;
  private String gitUrl;
  private String bireusUrl;
  private String technicalName;
  private boolean visible;
}
