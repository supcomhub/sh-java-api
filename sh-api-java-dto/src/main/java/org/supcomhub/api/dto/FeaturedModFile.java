package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(FeaturedModFile.TYPE_NAME)
@FieldNameConstants
public class FeaturedModFile extends AbstractEntity {

  public static final String TYPE_NAME = "featuredModFile";

  private String version;
  private String group;
  private String name;
  private String md5;
  private String url;
}
