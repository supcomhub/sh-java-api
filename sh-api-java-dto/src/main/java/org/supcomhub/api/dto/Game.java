package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.time.OffsetDateTime;
import java.util.List;

@Getter
@Setter
@Type("game")
@FieldNameConstants
public class Game extends AbstractEntity {
  private String name;
  private OffsetDateTime startTime;
  private OffsetDateTime endTime;
  private Validity validity;
  private VictoryCondition victoryCondition;

  @Relationship("reviews")
  private List<GameReview> reviews;

  @Relationship("gameParticipants")
  private List<GameParticipant> playerStats;

  @Relationship("host")
  private Account host;

  @Relationship("featuredMod")
  private FeaturedMod featuredMod;

  @Relationship("mapVersion")
  private MapVersion mapVersion;
}
