package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.jetbrains.annotations.Nullable;

import java.time.OffsetDateTime;

@Getter
@Setter
@Type(GameParticipant.TYPE_NAME)
@FieldNameConstants
public class GameParticipant extends AbstractEntity {

  public static final String TYPE_NAME = "gameParticipant";

  private Boolean ai;
  private Faction faction;
  private Short color;
  private Short team;
  private Short startSpot;
  private Integer rankBefore;
  private Integer rankAfter;
  private Short score;
  private Boolean won;
  private GameParticipantOutcome outcome;

  @Nullable
  private OffsetDateTime finishTime;

  @Relationship("game")
  private Game game;

  @Relationship("participant")
  private Account participant;
}
