package org.supcomhub.api.dto;

public enum GameParticipantOutcome {
  VICTORY,
  DRAW,
  DEFEAT
}
