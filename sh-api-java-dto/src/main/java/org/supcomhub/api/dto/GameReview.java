package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(GameReview.TYPE_NAME)
@FieldNameConstants
public class GameReview extends Review {

  public static final String TYPE_NAME = "gameReview";

  @Relationship("game")
  private Game game;
}
