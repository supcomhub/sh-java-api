package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(GameReviewSummary.TYPE_NAME)
@FieldNameConstants
public class GameReviewSummary extends ReviewSummary {

  public static final String TYPE_NAME = "gameReviewSummary";

  @Relationship("game")
  private Game game;
}
