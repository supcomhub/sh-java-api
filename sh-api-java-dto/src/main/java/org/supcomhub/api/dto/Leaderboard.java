package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(Leaderboard.TYPE_NAME)
@FieldNameConstants
public class Leaderboard extends AbstractEntity {

  public static final String TYPE_NAME = "leaderboard";

  private String technicalName;

  private String nameKey;

  private String descriptionKey;
}
