package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(LeaderboardEntry.TYPE_NAME)
@FieldNameConstants
public class LeaderboardEntry extends AbstractEntity {

  public static final String TYPE_NAME = "leaderboardEntry";

  private String playerName;
  private Integer rating;
  private Integer totalGames;
  private Integer wonGames;
  private Leaderboard leaderboard;
  private int position;
}
