package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Getter
@Setter
@Type(Map.TYPE_NAME)
@FieldNameConstants
public class Map extends AbstractEntity {

  public static final String TYPE_NAME = "map";

  private String battleType;
  private String displayName;
  private String mapType;

  @Relationship("uploader")
  private Account uploader;

  @Relationship("latestVersion")
  @JsonIgnore
  private MapVersion latestVersion;

  @Relationship("versions")
  @JsonIgnore
  private List<MapVersion> versions;
}
