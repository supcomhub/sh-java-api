package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(MapReview.TYPE_NAME)
@FieldNameConstants
public class MapReview extends Review {

  public static final String TYPE_NAME = "mapReview";

  @Relationship("map")
  private Map map;
}
