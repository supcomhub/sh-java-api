package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.jetbrains.annotations.Nullable;

import java.net.URL;
import java.util.List;

@Getter
@Setter
@Type(MapVersion.TYPE_NAME)
@FieldNameConstants
public class MapVersion extends AbstractEntity {

  public static final String TYPE_NAME = "mapVersion";

  private String description;
  private Integer maxPlayers;
  private Integer width;
  private Integer height;
  private String version;
  private String folderName;
  // TODO name consistently with folderName
  private String filename;
  private boolean ranked;
  private boolean hidden;
  private URL thumbnailUrlSmall;
  private URL thumbnailUrlLarge;
  private URL downloadUrl;

  @Relationship("map")
  private Map map;

  @Relationship("reviewSummary")
  private MapVersionReviewSummary reviewSummary;

  @Relationship("reviews")
  private List<MapVersionReview> reviews;
}
