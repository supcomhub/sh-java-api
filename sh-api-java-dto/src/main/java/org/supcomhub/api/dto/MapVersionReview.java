package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(MapVersionReview.TYPE_NAME)
@FieldNameConstants
public class MapVersionReview extends Review {

  protected static final String TYPE_NAME = "mapVersionReview";

  @Relationship("mapVersion")
  private MapVersion mapVersion;
}
