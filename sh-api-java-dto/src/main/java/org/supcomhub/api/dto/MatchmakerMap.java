package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(MatchmakerMap.TYPE_NAME)
@FieldNameConstants
public class MatchmakerMap extends AbstractEntity {

  public static final String TYPE_NAME = "matchmakerMap";

  @Relationship("mapVersion")
  private MapVersion mapVersion;

  @Relationship("matchmakerPool")
  private MatchmakerPool matchmakerPool;
}
