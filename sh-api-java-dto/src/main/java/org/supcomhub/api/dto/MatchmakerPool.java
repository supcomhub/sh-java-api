package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(MatchmakerPool.TYPE_NAME)
@FieldNameConstants
public class MatchmakerPool extends AbstractEntity {
  public static final String TYPE_NAME = "matchmakerPool";

  private String nameKey;
  private String technicalName;
  @Relationship("featuredMod")
  private FeaturedMod featuredMod;
  @Relationship("leaderboard")
  private Leaderboard leaderboard;

}
