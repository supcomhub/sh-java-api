package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Id;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;
import org.supcomhub.api.dto.elide.ElideEntity;

import java.util.Set;

@Type(MeResult.TYPE_NAME)
@Getter
@Setter
@FieldNameConstants
public class MeResult implements ElideEntity {

  public static final String TYPE_NAME = "me";

  @Id
  private String id;
  private Integer userId;
  private String displayName;
  private String primaryEmailAddress;
  private Set<String> groups;
  private Set<String> permissions;
}
