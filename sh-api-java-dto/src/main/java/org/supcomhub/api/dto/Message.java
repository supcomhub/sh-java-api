package org.supcomhub.api.dto;


import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(Message.TYPE_NAME)
@FieldNameConstants
public class Message extends AbstractEntity {
  public static final String TYPE_NAME = "message";

  private String key;
  private String language;
  private String region;
  private String value;
}
