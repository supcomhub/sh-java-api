package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.*;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Getter
@Setter
@Type("mod")
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(of = "displayName", callSuper = true)
@FieldNameConstants
public class Mod extends AbstractEntity {

  private String displayName;
  private String author;

  @Relationship("uploader")
  private Account uploader;

  @Relationship("versions")
  private List<ModVersion> versions;

  @Relationship("latestVersion")
  private ModVersion latestVersion;
}
