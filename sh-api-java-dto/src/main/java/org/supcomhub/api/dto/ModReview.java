package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(ModReview.TYPE_NAME)
@FieldNameConstants
public class ModReview extends Review {

  public static final String TYPE_NAME = "modReview";

  @Relationship("map")
  private Mod mod;
}
