package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(ModReviewSummary.TYPE_NAME)
@FieldNameConstants
public class ModReviewSummary extends ReviewSummary {

  public static final String TYPE_NAME = "modReviewSummary";

  @Relationship("mod")
  private Mod mod;
}
