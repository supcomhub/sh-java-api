package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.net.URL;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Type("modVersion")
@FieldNameConstants
public class ModVersion extends AbstractEntity {

  public static final String TYPE_NAME = "modVersion";

  private UUID uuid;
  private ModType type;
  private String description;
  private Short version;
  private String filename;
  private String icon;
  private Boolean ranked;
  private Boolean hidden;
  private URL thumbnailUrl;
  private URL downloadUrl;

  @Relationship("mod")
  private Mod mod;

  @Relationship("reviews")
  private List<ModVersionReview> reviews;

  @Relationship("reviewSummary")
  private ModVersionReviewSummary reviewSummary;
}
