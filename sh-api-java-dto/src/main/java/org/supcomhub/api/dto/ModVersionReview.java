package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type("modVersionReview")
@FieldNameConstants
public class ModVersionReview extends Review {

  @Relationship("modVersion")
  private ModVersion modVersion;
}
