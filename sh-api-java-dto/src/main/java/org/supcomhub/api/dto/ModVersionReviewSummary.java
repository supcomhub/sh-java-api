package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(ModVersionReviewSummary.TYPE_NAME)
@FieldNameConstants
public class ModVersionReviewSummary extends ReviewSummary {

  public static final String TYPE_NAME = "modVersionReviewSummary";

  @Relationship("modVersion")
  private ModVersion modVersion;
}
