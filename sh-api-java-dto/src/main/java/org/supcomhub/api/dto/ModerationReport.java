package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.util.Set;

@Type(ModerationReport.TYPE_NAME)
@Getter
@Setter
@FieldNameConstants
public class ModerationReport extends AbstractEntity {
  public static final String TYPE_NAME = "moderationReport";

  private String reportDescription;
  private ModerationReportStatus reportStatus;
  private String gameIncidentTimecode;
  private String publicNote;
  private String privateNote;

  @Relationship("bans")
  @JsonIgnore
  private Set<Ban> bans;
  @Relationship("reporter")
  @JsonIgnore
  private Account reporter;
  @Relationship("game")
  @JsonIgnore
  private Game game;
  @Relationship("lastModerator")
  @JsonIgnore
  private Account lastModerator;
  @Relationship("reportedUsers")
  @JsonIgnore
  private Set<Account> reportedUsers;
}
