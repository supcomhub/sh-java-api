package org.supcomhub.api.dto;

public enum ModerationReportStatus {
  AWAITING, PROCESSING, COMPLETED, DISCARDED
}
