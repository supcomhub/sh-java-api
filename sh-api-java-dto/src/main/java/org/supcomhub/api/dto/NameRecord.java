package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.experimental.FieldNameConstants;

@Getter
@Type(NameRecord.TYPE_NAME)
@FieldNameConstants
public class NameRecord extends AbstractEntity {

  public static final String TYPE_NAME = "nameRecord";

  private String newName;
  private String previousName;

  @JsonBackReference("nameRecords")
  @Relationship("account")
  private Account account;
}
