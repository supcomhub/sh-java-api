package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Type(NewsPost.TYPE_NAME)
@Getter
@Setter
@FieldNameConstants
public class NewsPost extends AbstractEntity {
  public static final String TYPE_NAME = "newsPost";

  @Relationship("author")
  private Account author;
  private String headline;
  private String body;
  private String tags;
  private Boolean pinned;
}
