package org.supcomhub.api.dto;


import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type("playerAchievement")
@FieldNameConstants
public class PlayerAchievement extends AbstractEntity {

  public static final String TYPE_NAME = "playerAchievement";

  private AchievementState state;
  private Integer currentSteps;
  private Integer playerId;

  @Relationship("achievement")
  private Achievement achievement;
}
