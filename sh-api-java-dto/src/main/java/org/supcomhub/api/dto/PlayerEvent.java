package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type("playerEvent")
@FieldNameConstants
public class PlayerEvent extends AbstractEntity {

  public static final String TYPE_NAME = "playerEvent";

  private int count;

  @Relationship("event")
  private Event event;
}
