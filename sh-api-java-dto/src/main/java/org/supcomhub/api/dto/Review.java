package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@FieldNameConstants
public class Review extends AbstractEntity {
  private String text;
  private Short score;

  @Relationship("reviewer")
  private Account reviewer;
}
