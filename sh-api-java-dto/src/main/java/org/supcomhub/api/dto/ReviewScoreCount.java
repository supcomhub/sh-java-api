package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(ReviewScoreCount.TYPE_NAME)
@FieldNameConstants
public class ReviewScoreCount extends AbstractEntity {

  public static final String TYPE_NAME = "reviewScoreCount";

  private int count;
  private byte score;
}
