package org.supcomhub.api.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Getter
@Setter
@FieldNameConstants
public abstract class ReviewSummary extends AbstractEntity {

  private Float positive;
  private Float negative;
  private Float score;
  private Integer reviews;
  private Float lowerBound;
  private List<ReviewScoreCount> scoreCounts;
}
