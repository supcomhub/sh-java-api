package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.time.OffsetDateTime;

@Getter
@Setter
@Type(TeamkillReport.TYPE_NAME)
@RestrictedVisibility("IsModerator")
@FieldNameConstants
public class TeamkillReport extends AbstractEntity {

  public static final String TYPE_NAME = "teamkillReport";

  /** How many seconds into the game, in simulation time. */
  private Integer gameTime;
  private OffsetDateTime reportedAt;

  @Relationship("teamkiller")
  private Account teamkiller;

  @Relationship("victim")
  private Account victim;

  @Relationship("game")
  private Game game;
}
