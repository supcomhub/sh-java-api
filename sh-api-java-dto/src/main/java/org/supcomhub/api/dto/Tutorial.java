package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(Tutorial.TYPE_NAME)
@FieldNameConstants
public class Tutorial extends AbstractEntity {

  public static final String TYPE_NAME = "tutorial";

  private String descriptionKey;
  private String description;
  private String titleKey;
  private String title;
  private String image;
  private String imageUrl;
  private Integer ordinal;
  private Boolean launchable;
  private String technicalName;

  @Relationship("category")
  private TutorialCategory category;

  @Relationship("mapVersion")
  private MapVersion mapVersion;
}
