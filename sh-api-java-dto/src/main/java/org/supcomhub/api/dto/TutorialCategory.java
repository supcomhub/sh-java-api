package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Getter
@Setter
@Type(TutorialCategory.TYPE_NAME)
@ToString(exclude = "tutorials")
@FieldNameConstants
public class TutorialCategory extends AbstractEntity {

  public static final String TYPE_NAME = "tutorialCategory";

  private String categoryKey;
  private String category;
  private String technicalName;

  @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
  @Relationship("tutorials")
  private List<Tutorial> tutorials;

}
