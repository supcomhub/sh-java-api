package org.supcomhub.api.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Getter
@Setter
@Type(UserNote.TYPE_NAME)
@RestrictedVisibility("IsModerator")
@FieldNameConstants
public class UserNote extends AbstractEntity {

  public static final String TYPE_NAME = "userNote";

  @Relationship("account")
  @JsonIgnore
  private Account account;
  @JsonIgnore
  @Relationship("author")
  private Account author;
  private boolean watched;
  private String note;
}
