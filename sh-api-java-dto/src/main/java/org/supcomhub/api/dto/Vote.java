package org.supcomhub.api.dto;

import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

import java.util.List;

@Getter
@Setter
@Type(Vote.TYPE_NAME)
@FieldNameConstants
public class Vote extends AbstractEntity {
  public static final String TYPE_NAME = "vote";

  @Relationship("voter")
  private Account voter;

  @Relationship("votingSubject")
  private VotingSubject votingSubject;

  @Relationship("votingAnswers")
  private List<VotingAnswer> votingAnswers;
}
