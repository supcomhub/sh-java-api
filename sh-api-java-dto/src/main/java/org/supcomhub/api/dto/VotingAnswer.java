package org.supcomhub.api.dto;


import com.github.jasminb.jsonapi.annotations.Relationship;
import com.github.jasminb.jsonapi.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.FieldNameConstants;

@Setter
@Getter
@Type(VotingAnswer.TYPE_NAME)
@FieldNameConstants
public class VotingAnswer extends AbstractEntity {
  public static final String TYPE_NAME = "votingAnswer";

  @Relationship("vote")
  private Vote vote;
  private Integer alternativeOrdinal;
  @Relationship("votingChoice")
  private VotingChoice votingChoice;
}
