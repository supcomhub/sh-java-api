package org.supcomhub.api.dto.challonge;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.OffsetDateTime;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Tournament {

  private String id;
  private String name;
  private String description;
  private String tournamentType;
  private Integer participantCount;
  private String challongeUrl;
  private String liveImageUrl;
  private String signUpUrl;
  private Boolean openForSignup;
  private OffsetDateTime createdAt;
  private OffsetDateTime startingAt;
  private OffsetDateTime completedAt;
}
