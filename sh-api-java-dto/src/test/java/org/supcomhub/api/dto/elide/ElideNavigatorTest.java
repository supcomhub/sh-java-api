package org.supcomhub.api.dto.elide;

import org.junit.Test;
import org.supcomhub.api.dto.MatchmakerMap;
import org.supcomhub.api.dto.MapVersion;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class ElideNavigatorTest {
  @Test
  public void testGetList() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .collection()
      .build(), is("/data/matchmakerMap"));
  }

  @Test
  public void testGetId() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .id("5")
      .build(), is("/data/matchmakerMap/5"));
  }

  @Test
  public void testGetListSingleInclude() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .collection()
      .addIncludeOnCollection("mapVersion")
      .build(), is("/data/matchmakerMap?include=mapVersion"));
  }

  @Test
  public void testGetListMultipleInclude() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .collection()
      .addIncludeOnCollection("mapVersion")
      .addIncludeOnCollection("mapVersion.map")
      .build(), is("/data/matchmakerMap?include=mapVersion,mapVersion.map"));
  }

  @Test
  public void testGetListFiltered() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .collection()
      .addFilter(
        ElideNavigator.qBuilder()
          .intNum("mapVersion.id").gt(10)
          .or()
          .string("hello").eq("nana")
      )
      .build(), is("/data/matchmakerMap?filter=mapVersion.id=gt=\"10\",hello==\"nana\""));
  }

  @Test
  public void testGetListCombinedFilter() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .collection()
      .addIncludeOnCollection("mapVersion")
      .addIncludeOnCollection("mapVersion.map")
      .pageSize(10)
      .pageNumber(3)
      .addFilter(
        ElideNavigator.qBuilder()
          .intNum("mapVersion.id").gt(10)
          .or()
          .string("hello").eq("nana")
      )
      .build(), is("/data/matchmakerMap?include=mapVersion,mapVersion.map&filter=mapVersion.id=gt=\"10\",hello==\"nana\"&page[size]=10&page[number]=3"));
  }

  @Test
  public void testGetIdMultipleInclude() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .id("5")
      .addIncludeOnId("mapVersion")
      .addIncludeOnId("mapVersion.map")
      .build(), is("/data/matchmakerMap/5?include=mapVersion,mapVersion.map"));
  }

  @Test
  public void testGetListSorted() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .collection()
      .addSortingRule("sortCritASC", true)
      .addSortingRule("sortCritDESC", false)
      .build(), is("/data/matchmakerMap?sort=+sortCritASC,-sortCritDESC"));
  }

  @Test
  public void testNavigateFromIdToId() {
    assertThat(ElideNavigator.of(MatchmakerMap.class)
      .id("5")
      .navigateRelationship(MapVersion.class, "mapVersion")
      .id("1234")
      .build(), is("/data/matchmakerMap/5/mapVersion/1234"));
  }
}
