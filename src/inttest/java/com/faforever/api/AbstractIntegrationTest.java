package com.faforever.api;

import com.faforever.api.config.ApplicationProfile;
import com.faforever.api.error.ErrorCode;
import com.faforever.api.utils.OAuthHelper;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.github.jasminb.jsonapi.JSONAPIDocument;
import com.github.jasminb.jsonapi.ResourceConverter;
import com.github.jasminb.jsonapi.exceptions.DocumentSerializationException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.mockito.internal.util.collections.Sets;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.RequestPostProcessor;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.supcomhub.api.dto.AbstractEntity;
import org.supcomhub.api.dto.Account;
import org.supcomhub.api.dto.Avatar;
import org.supcomhub.api.dto.AvatarAssignment;
import org.supcomhub.api.dto.Ban;
import org.supcomhub.api.dto.EmailDomainBan;
import org.supcomhub.api.dto.ModerationReport;
import org.supcomhub.api.dto.NewsPost;
import org.supcomhub.api.dto.Tutorial;

import javax.transaction.Transactional;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.Set;

import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles(ApplicationProfile.INTEGRATION_TEST)
@Import(OAuthHelper.class)
@Transactional
public abstract class AbstractIntegrationTest {
  protected static final String NO_SCOPE = null;
  protected static final String NO_AUTHORITIES = null;
  protected static final DateTimeFormatter OFFSET_DATE_TIME_FORMATTER = DateTimeFormatter.ISO_OFFSET_DATE_TIME;

  protected static final String AUTH_WEBSITE = "WEBSITE";
  protected static final String AUTH_USER = "user@supcomhub.org";
  protected static final String AUTH_USER_PASSWORD = "USER";
  protected static final String USER_DISPLAY_NAME = "USER";
  protected static final String AUTH_MODERATOR = "moderator@supcomhub.org";
  protected static final String AUTH_MODERATOR_DISPLAY_NAME = "MODERATOR";
  protected static final String AUTH_ADMIN = "admin@supcomhub.org";
  @Autowired
  protected OAuthHelper oAuthHelper;
  protected MockMvc mockMvc;
  @Autowired
  protected WebApplicationContext context;
  protected ObjectMapper objectMapper;
  protected ResourceConverter resourceConverter;

  @Before
  public void setUp() {
    this.mockMvc = MockMvcBuilders
      .webAppContextSetup(this.context)
      .apply(springSecurity())
      .build();

    this.objectMapper = new ObjectMapper();
    this.objectMapper.registerModule(new JavaTimeModule());
    this.objectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
    this.objectMapper.setSerializationInclusion(Include.NON_EMPTY);

    // maybe use reflections library to find all subtypes of AbstractEntity
    resourceConverter = new ResourceConverter(
      objectMapper,
      ModerationReport.class,
      Account.class,
      Tutorial.class,
      Avatar.class,
      AvatarAssignment.class,
      Ban.class,
      EmailDomainBan.class,
      NewsPost.class
    );
  }

  protected RequestPostProcessor getOAuthTokenWithoutUser(String... scope) {
    return oAuthHelper.addBearerToken(Sets.newSet(scope), null);
  }

  protected RequestPostProcessor getOAuthTokenWithTestUser(String scope, String authority) {
    return getOAuthTokenWithTestUser(scope == null ? Collections.emptySet() : Collections.singleton(scope),
      authority == null ? Collections.emptySet() : Collections.singleton(authority));
  }

  protected RequestPostProcessor getOAuthTokenWithTestUser(Set<String> scope, Set<String> authorities) {
    return oAuthHelper.addBearerToken(5, "testUser", scope, authorities);
  }

  protected void assertApiError(MvcResult mvcResult, ErrorCode errorCode) throws Exception {
    JSONObject resonseJson = new JSONObject(mvcResult.getResponse().getContentAsString());
    JSONAssert.assertEquals(String.format("{\"errors\":[{\"code\":\"%s\"}]}", errorCode.getCode()), resonseJson, false);
  }

  protected <T extends AbstractEntity> byte[] createJsonApiContent(T entity) throws DocumentSerializationException {
    return resourceConverter.writeDocument(new JSONAPIDocument<>(entity));
  }
}
