package com.faforever.api.clan;

import com.faforever.api.AbstractIntegrationTest;
import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.Clan;
import com.faforever.api.error.ErrorCode;
import com.faforever.api.security.FafUserDetails;
import com.faforever.api.user.AccountRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.util.MultiValueMap;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNull.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/globalCleanUp.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepDefaultUser.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepClanData.sql")
public class ClanControllerTest extends AbstractIntegrationTest {
  private static final String AUTH_CLAN_MEMBER = "clan_member@supcomhub.org";
  private static final String NEW_CLAN_NAME = "New Clan Name";
  private static final String NEW_CLAN_TAG = "new";
  private static final String NEW_CLAN_DESCRIPTION = "spaces Must Be Encoded";
  private static final String EXISTING_CLAN = "Alpha Clan";

  @Autowired
  AccountRepository accountRepository;
  @Autowired
  ClanRepository clanRepository;
  @Autowired
  ClanMembershipRepository clanMembershipRepository;

  private Account getAccount() {
    FafUserDetails authentication = (FafUserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    return accountRepository.getOne(authentication.getId());
  }

  @Test
  @WithUserDetails(AUTH_USER)
  public void meDataWithoutClan() throws Exception {
    Account account = getAccount();

    mockMvc.perform(get("/clans/me/"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.member.id", is(account.getId())))
      .andExpect(jsonPath("$.member.displayName", is(account.getDisplayName())))
      .andExpect(jsonPath("$.clan", is(nullValue())));
  }

  @Test
  @WithUserDetails(AUTH_CLAN_MEMBER)
  public void meDataWithClan() throws Exception {
    Account account = getAccount();
    Clan clan = clanRepository.getOne(1);

    mockMvc.perform(
      get("/clans/me/"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.member.id", is(account.getId())))
      .andExpect(jsonPath("$.member.displayName", is(account.getDisplayName())))
      .andExpect(jsonPath("$.clan.id", is(clan.getId())))
      .andExpect(jsonPath("$.clan.tag", is(clan.getTag())))
      .andExpect(jsonPath("$.clan.name", is(clan.getName())));
  }

  @Test
  @WithUserDetails(AUTH_USER)
  public void createClanWithSuccess() throws Exception {
    Account account = getAccount();

    assertNull(account.getClan());
    assertFalse(clanRepository.findOneByName(NEW_CLAN_NAME).isPresent());

    MultiValueMap<String, String> params = new HttpHeaders();
    params.add("name", NEW_CLAN_NAME);
    params.add("tag", NEW_CLAN_TAG);
    params.add("description", NEW_CLAN_DESCRIPTION);

    ResultActions action = mockMvc.perform(
      post("/clans/create")
        .params(params));

    Clan clan = clanRepository.findOneByName(NEW_CLAN_NAME)
      .orElseThrow(() -> new IllegalStateException("Clan not found - but should be created"));

    action.andExpect(status().isOk())
      .andExpect(jsonPath("$.id", is(clan.getId())))
      .andExpect(jsonPath("$.type", is("clan")));
  }

  @Test
  public void createClanWithoutAuth() throws Exception {
    MultiValueMap<String, String> params = new HttpHeaders();
    params.add("name", NEW_CLAN_NAME);
    params.add("tag", NEW_CLAN_TAG);
    params.add("description", NEW_CLAN_DESCRIPTION);

    mockMvc.perform(
      post("/clans/create")
        .params(params))
      .andExpect(status().isForbidden());
  }

  @Test
  @WithUserDetails(AUTH_USER)
  public void createClanWithExistingName() throws Exception {
    Account account = getAccount();

    assertNull(account.getClan());
    assertTrue(clanRepository.findOneByName(EXISTING_CLAN).isPresent());

    MultiValueMap<String, String> params = new HttpHeaders();
    params.add("name", EXISTING_CLAN);
    params.add("tag", NEW_CLAN_TAG);
    params.add("description", NEW_CLAN_DESCRIPTION);

    MvcResult result = mockMvc.perform(
      post("/clans/create")
        .params(params))
      .andExpect(status().isUnprocessableEntity())
      .andReturn();

    assertApiError(result, ErrorCode.CLAN_NAME_EXISTS);
  }

  @Test
  @WithUserDetails(AUTH_USER)
  public void createClanWithExistingTag() throws Exception {
    Account account = getAccount();

    assertNull(account.getClan());
    assertFalse(clanRepository.findOneByName(NEW_CLAN_NAME).isPresent());

    MultiValueMap<String, String> params = new HttpHeaders();
    params.add("name", NEW_CLAN_NAME);
    params.add("tag", "123");
    params.add("description", NEW_CLAN_DESCRIPTION);

    MvcResult result = mockMvc.perform(
      post("/clans/create")
        .params(params))
      .andExpect(status().isUnprocessableEntity())
      .andReturn();

    assertApiError(result, ErrorCode.CLAN_TAG_EXISTS);
  }

  @Test
  @WithUserDetails(AUTH_CLAN_MEMBER)
  public void createSecondClan() throws Exception {
    Account account = getAccount();

    assertNotNull(account.getClan());
    assertFalse(clanRepository.findOneByName(NEW_CLAN_NAME).isPresent());

    MultiValueMap<String, String> params = new HttpHeaders();
    params.add("name", NEW_CLAN_NAME);
    params.add("tag", NEW_CLAN_TAG);
    params.add("description", NEW_CLAN_DESCRIPTION);

    MvcResult result = mockMvc.perform(
      post("/clans/create")
        .params(params))
      .andExpect(status().isUnprocessableEntity())
      .andReturn();

    assertApiError(result, ErrorCode.CLAN_CREATE_CREATOR_IS_IN_A_CLAN);
  }
}
