package com.faforever.api.data;

import com.faforever.api.AbstractIntegrationTest;
import com.faforever.api.data.domain.GroupPermission;
import com.faforever.api.security.OAuthScope;
import org.junit.Test;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.springframework.test.web.servlet.ResultActions;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/globalCleanUp.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepDefaultUser.sql")
public class AccountElidePermissionTest extends AbstractIntegrationTest {
  private void assertMinimumAccess(ResultActions mockMvcPerform) throws Exception {
    mockMvcPerform
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id").exists())
      .andExpect(jsonPath("$.data.attributes.updateTime").exists())
      .andExpect(jsonPath("$.data.attributes.createTime").exists())
      .andExpect(jsonPath("$.data.attributes.displayName").exists())
      .andExpect(jsonPath("$.data.relationships.bans").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.emailAddresses").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.password").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userNotes").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.clanMemberships").exists())
      .andExpect(jsonPath("$.data.relationships.nameRecords").exists())
      .andExpect(jsonPath("$.data.relationships.accountVerifications").exists())
      .andExpect(jsonPath("$.data.relationships.avatarAssignments").exists())
      .andExpect(jsonPath("$.data.relationships.reporterOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.reportedOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userGroups").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginUserAgent").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginIpAddress").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginTime").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.primaryEmailAddress").doesNotExist());
  }

  @Test
  public void testGetAccountNoPermissions() throws Exception {
    assertMinimumAccess(mockMvc.perform(get("/data/account/1")));
  }

  @Test
  @WithUserDetails(AUTH_USER)
  public void testGetAccountAsEntityOwner() throws Exception {
    mockMvc.perform(get("/data/account/1"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id").exists())
      .andExpect(jsonPath("$.data.attributes.updateTime").exists())
      .andExpect(jsonPath("$.data.attributes.createTime").exists())
      .andExpect(jsonPath("$.data.attributes.displayName").exists())
      .andExpect(jsonPath("$.data.relationships.bans").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.emailAddresses").exists()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.password").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userNotes").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.clanMemberships").exists())
      .andExpect(jsonPath("$.data.relationships.nameRecords").exists())
      .andExpect(jsonPath("$.data.relationships.accountVerifications").exists())
      .andExpect(jsonPath("$.data.relationships.avatarAssignments").exists())
      .andExpect(jsonPath("$.data.relationships.reporterOnModerationReports").exists()) // additionally visible
      .andExpect(jsonPath("$.data.relationships.reportedOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userGroups").exists()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.lastLoginUserAgent").isEmpty()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.lastLoginIpAddress").isEmpty()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.lastLoginTime").isEmpty()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.primaryEmailAddress").exists()); // additionally visible
  }

  @Test
  public void testGetAccount_AdminAccountBanCheck() throws Exception {
    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))));

    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_ADMIN_ACCOUNT_BAN))));

    mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_ADMIN_ACCOUNT_BAN)))
      .andExpect(status().isOk())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id").exists())
      .andExpect(jsonPath("$.data.attributes.updateTime").exists())
      .andExpect(jsonPath("$.data.attributes.createTime").exists())
      .andExpect(jsonPath("$.data.attributes.displayName").exists())
      .andExpect(jsonPath("$.data.relationships.bans").exists()) // additionally visible
      .andExpect(jsonPath("$.data.relationships.emailAddresses").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.password").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userNotes").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.clanMemberships").exists())
      .andExpect(jsonPath("$.data.relationships.nameRecords").exists())
      .andExpect(jsonPath("$.data.relationships.accountVerifications").exists())
      .andExpect(jsonPath("$.data.relationships.avatarAssignments").exists())
      .andExpect(jsonPath("$.data.relationships.reporterOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.reportedOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userGroups").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginUserAgent").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginIpAddress").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginTime").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.primaryEmailAddress").doesNotExist());
  }

  @Test
  public void testGetAccount_AdminAccountNoteCheck() throws Exception {
    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._READ_SENSIBLE_USERDATA, NO_AUTHORITIES))));

    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_ADMIN_ACCOUNT_NOTE))));

    mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._READ_SENSIBLE_USERDATA, GroupPermission.ROLE_ADMIN_ACCOUNT_NOTE)))
      .andExpect(status().isOk())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id").exists())
      .andExpect(jsonPath("$.data.attributes.updateTime").exists())
      .andExpect(jsonPath("$.data.attributes.createTime").exists())
      .andExpect(jsonPath("$.data.attributes.displayName").exists())
      .andExpect(jsonPath("$.data.relationships.bans").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.emailAddresses").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.password").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userNotes").exists()) // additionally visible
      .andExpect(jsonPath("$.data.relationships.clanMemberships").exists())
      .andExpect(jsonPath("$.data.relationships.nameRecords").exists())
      .andExpect(jsonPath("$.data.relationships.accountVerifications").exists())
      .andExpect(jsonPath("$.data.relationships.avatarAssignments").exists())
      .andExpect(jsonPath("$.data.relationships.reporterOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.reportedOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userGroups").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginUserAgent").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginIpAddress").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginTime").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.primaryEmailAddress").doesNotExist());
  }

  @Test
  public void testGetAccount_AdminModerationReportCheck() throws Exception {
    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))));

    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_ADMIN_MODERATION_REPORT))));

    mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_ADMIN_MODERATION_REPORT)))
      .andExpect(status().isOk())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id").exists())
      .andExpect(jsonPath("$.data.attributes.updateTime").exists())
      .andExpect(jsonPath("$.data.attributes.createTime").exists())
      .andExpect(jsonPath("$.data.attributes.displayName").exists())
      .andExpect(jsonPath("$.data.relationships.bans").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.emailAddresses").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.password").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userNotes").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.clanMemberships").exists())
      .andExpect(jsonPath("$.data.relationships.nameRecords").exists())
      .andExpect(jsonPath("$.data.relationships.accountVerifications").exists())
      .andExpect(jsonPath("$.data.relationships.avatarAssignments").exists())
      .andExpect(jsonPath("$.data.relationships.reporterOnModerationReports").exists()) // additionally visible
      .andExpect(jsonPath("$.data.relationships.reportedOnModerationReports").exists()) // additionally visible
      .andExpect(jsonPath("$.data.relationships.userGroups").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginUserAgent").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginIpAddress").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginTime").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.primaryEmailAddress").doesNotExist());
  }

  @Test
  public void testGetAccount_ReadAccountPrivateDetailsCheck() throws Exception {
    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._READ_SENSIBLE_USERDATA, NO_AUTHORITIES))));

    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_READ_ACCOUNT_PRIVATE_DETAILS))));

    mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._READ_SENSIBLE_USERDATA, GroupPermission.ROLE_READ_ACCOUNT_PRIVATE_DETAILS)))
      .andExpect(status().isOk())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id").exists())
      .andExpect(jsonPath("$.data.attributes.updateTime").exists())
      .andExpect(jsonPath("$.data.attributes.createTime").exists())
      .andExpect(jsonPath("$.data.attributes.displayName").exists())
      .andExpect(jsonPath("$.data.relationships.bans").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.emailAddresses").exists()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.password").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userNotes").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.clanMemberships").exists())
      .andExpect(jsonPath("$.data.relationships.nameRecords").exists())
      .andExpect(jsonPath("$.data.relationships.accountVerifications").exists())
      .andExpect(jsonPath("$.data.relationships.avatarAssignments").exists())
      .andExpect(jsonPath("$.data.relationships.reporterOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.reportedOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userGroups").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginUserAgent").isEmpty()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.lastLoginIpAddress").isEmpty()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.lastLoginTime").isEmpty()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.primaryEmailAddress").exists()); // additionally visible
  }

  @Test
  public void testGetAccount_ReadUserGroupCheck() throws Exception {
    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._READ_SENSIBLE_USERDATA, NO_AUTHORITIES))));

    assertMinimumAccess(mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_READ_USER_GROUP))));

    mockMvc.perform(get("/data/account/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._READ_SENSIBLE_USERDATA, GroupPermission.ROLE_READ_USER_GROUP)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id").exists())
      .andExpect(jsonPath("$.data.attributes.updateTime").exists())
      .andExpect(jsonPath("$.data.attributes.createTime").exists())
      .andExpect(jsonPath("$.data.attributes.displayName").exists())
      .andExpect(jsonPath("$.data.relationships.bans").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.emailAddresses").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.password").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userNotes").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.clanMemberships").exists())
      .andExpect(jsonPath("$.data.relationships.nameRecords").exists())
      .andExpect(jsonPath("$.data.relationships.accountVerifications").exists())
      .andExpect(jsonPath("$.data.relationships.avatarAssignments").exists())
      .andExpect(jsonPath("$.data.relationships.reporterOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.reportedOnModerationReports").doesNotExist())
      .andExpect(jsonPath("$.data.relationships.userGroups").exists()) // additionally visible
      .andExpect(jsonPath("$.data.attributes.lastLoginUserAgent").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginIpAddress").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.lastLoginTime").doesNotExist())
      .andExpect(jsonPath("$.data.attributes.primaryEmailAddress").doesNotExist());
  }

}
