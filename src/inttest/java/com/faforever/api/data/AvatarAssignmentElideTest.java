package com.faforever.api.data;

import com.faforever.api.AbstractIntegrationTest;
import com.faforever.api.data.domain.GroupPermission;
import com.faforever.api.security.OAuthScope;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.supcomhub.api.dto.Account;
import org.supcomhub.api.dto.Avatar;
import org.supcomhub.api.dto.AvatarAssignment;

import java.time.OffsetDateTime;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/globalCleanUp.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepDefaultUser.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepAvatarData.sql")
public class AvatarAssignmentElideTest extends AbstractIntegrationTest {


  @Test
  public void getUnusedAvatar() throws Exception {
    mockMvc.perform(get("/data/avatar/1"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id", is("1")))
      .andExpect(jsonPath("$.data.type", is("avatar")))
      .andExpect(jsonPath("$.data.attributes.description", is("Avatar No. 1")))
      .andExpect(jsonPath("$.data.attributes.url", is("http://localhost/faf/avatars/avatar1.png")))
      .andExpect(jsonPath("$.data.relationships.assignments.data", hasSize(0)));
  }

  @Test
  public void getAvatarWithPlayer() throws Exception {
    mockMvc.perform(get("/data/avatar/2"))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.id", is("2")))
      .andExpect(jsonPath("$.data.type", is("avatar")))
      .andExpect(jsonPath("$.data.attributes.description", is("Avatar No. 2")))
      .andExpect(jsonPath("$.data.attributes.url", is("http://localhost/faf/avatars/avatar2.png")))
      .andExpect(jsonPath("$.data.relationships.assignments.data", hasSize(2)));
  }

  @Test
  public void canAssignAvatarWithScopeAndRole() throws Exception {
    final Avatar avatar = (Avatar) new Avatar().setId("1");
    final Account account = (Account) new Account().setId("1");
    final AvatarAssignment avatarAssignment = new AvatarAssignment()
      .setAvatar(avatar)
      .setSelected(false);
    mockMvc.perform(
      post("/data/account/{playerId}/avatarAssignments", account.getId())
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_AVATAR))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(avatarAssignment)))
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.data.relationships.account.data.id", is(account.getId())))
      .andExpect(jsonPath("$.data.relationships.avatar.data.id", is(avatar.getId())));
  }

  @Test
  public void cannotAssignAvatarWithoutScope() throws Exception {
    final Avatar avatar = (Avatar) new Avatar().setId("1");
    final Account account = (Account) new Account().setId("1");
    final AvatarAssignment avatarAssignment = new AvatarAssignment()
      .setAvatar(avatar)
      .setSelected(false);
    mockMvc.perform(
      post("/data/account/{playerId}/avatarAssignments", account.getId())
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_AVATAR))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(avatarAssignment)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotAssignAvatarWithoutRole() throws Exception {
    final Avatar avatar = (Avatar) new Avatar().setId("1");
    final Account account = (Account) new Account().setId("1");
    final AvatarAssignment avatarAssignment = new AvatarAssignment()
      .setAvatar(avatar)
      .setSelected(false);
    mockMvc.perform(
      post("/data/account/{playerId}/avatarAssignments", account.getId())
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(avatarAssignment)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void canDeleteAvatarAssignmentWithScopeAndRole() throws Exception {
    mockMvc.perform(
      delete("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_AVATAR))
    ).andExpect(status().isNoContent());
    mockMvc.perform(
      get("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_AVATAR))
    ).andExpect(status().isNotFound());
  }

  @Test
  public void cannotDeleteAvatarAssignmentWithoutScope() throws Exception {
    mockMvc.perform(
      delete("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_AVATAR))
    ).andExpect(status().isForbidden());
  }

  @Test
  public void cannotDeleteAvatarAssignmentWithoutRole() throws Exception {
    mockMvc.perform(
      delete("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
    ).andExpect(status().isForbidden());
  }

  @Test
  public void canUpdateAvatarAssignmentExpirationWithScopeAndRole() throws Exception {
    final OffsetDateTime now = OffsetDateTime.now();
    final AvatarAssignment avatarAssignment = (AvatarAssignment) new AvatarAssignment()
      .setExpiresAt(now)
      .setId("1");
    mockMvc.perform(
      patch("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_AVATAR))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(avatarAssignment)))
      .andExpect(status().isNoContent());
    mockMvc.perform(
      get("/data/avatarAssignment/{assignmentId}", 1))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.attributes.expiresAt", is(OFFSET_DATE_TIME_FORMATTER.format(now))));
  }

  @Test
  public void cannotUpdateAvatarAssignmentExpirationWithoutScope() throws Exception {
    final OffsetDateTime now = OffsetDateTime.now();
    final AvatarAssignment avatarAssignment = (AvatarAssignment) new AvatarAssignment()
      .setExpiresAt(now)
      .setId("1");
    mockMvc.perform(
      patch("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_AVATAR))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(avatarAssignment)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotUpdateAvatarAssignmentExpirationWithoutRole() throws Exception {
    final OffsetDateTime now = OffsetDateTime.now();
    final AvatarAssignment avatarAssignment = (AvatarAssignment) new AvatarAssignment()
      .setExpiresAt(now)
      .setId("1");
    mockMvc.perform(
      patch("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(avatarAssignment)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void ownerCanUpdateAvatarAssignmentSelection() throws Exception {
    final AvatarAssignment avatarAssignment = (AvatarAssignment) new AvatarAssignment()
      .setSelected(true)
      .setId("1");
    mockMvc.perform(
      patch("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(NO_SCOPE, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(avatarAssignment)))
      .andExpect(status().isNoContent());
    mockMvc.perform(
      get("/data/avatarAssignment/{assignmentId}", 1)
        .with(getOAuthTokenWithTestUser(NO_SCOPE, NO_AUTHORITIES)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data.attributes.selected", is(true)));
  }

  @Test
  public void nonOwnerCannotUpdateAvatarAssignmentSelection() throws Exception {
    final AvatarAssignment avatarAssignment = (AvatarAssignment) new AvatarAssignment()
      .setSelected(false)
      .setId("2");
    mockMvc.perform(
      patch("/data/avatarAssignment/{assignmentId}", 2)
        .with(getOAuthTokenWithTestUser(OAuthScope._PUBLIC_PROFILE, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(avatarAssignment)))
      .andExpect(status().isForbidden());
  }
}
