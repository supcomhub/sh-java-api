package com.faforever.api.data;

import com.faforever.api.AbstractIntegrationTest;
import com.faforever.api.data.domain.GroupPermission;
import com.faforever.api.security.OAuthScope;
import com.faforever.api.user.AccountRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.supcomhub.api.dto.Account;
import org.supcomhub.api.dto.Ban;
import org.supcomhub.api.dto.BanScope;
import org.supcomhub.api.dto.ModerationReport;

import static com.faforever.api.data.domain.GroupPermission.ROLE_ADMIN_ACCOUNT_BAN;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/globalCleanUp.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepDefaultUser.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepMapData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepGameData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepLeaderboardData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepMatchmaker.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepModerationReportData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepBanData.sql")
public class BanTest extends AbstractIntegrationTest {
  /*
  {
      "data": {
          "type": "ban",
          "attributes": {
              "scope": "CHAT",
              "reason": "This ban will be posted"
          },
          "relationships": {
              "author": {
                  "data": {
                      "type": "account",
                      "id": "1"
                  }
              },
              "account": {
                  "data": {
                      "type": "account",
                      "id": "3"
                  }
              }
          }
      }
  }
   */
  private static final String testPost = "{\"data\":{\"type\":\"ban\",\"attributes\":{\"scope\":\"CHAT\",\"reason\":\"This test ban should be revoked\"},\"relationships\":{\"author\":{\"data\":{\"type\":\"account\",\"id\":\"1\"}},\"account\":{\"data\":{\"type\":\"account\",\"id\":\"3\"}}}}}";
  @Autowired
  AccountRepository accountRepository;

  @Test
  public void cannotReadSpecificBanWithoutScope() throws Exception {
    mockMvc.perform(get("/data/ban/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, ROLE_ADMIN_ACCOUNT_BAN)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotReadSpecificBanWithoutRole() throws Exception {
    mockMvc.perform(get("/data/ban/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void canReadSpecificBanWithScopeAndRole() throws Exception {
    mockMvc.perform(get("/data/ban/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_ADMIN_ACCOUNT_BAN)))
      .andExpect(status().isOk());
  }

  @Test
  public void canReadBansWithoutScope() throws Exception {
    mockMvc.perform(get("/data/ban")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, ROLE_ADMIN_ACCOUNT_BAN)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", hasSize(0)));
  }

  @Test
  public void canReadBansWithoutRole() throws Exception {
    mockMvc.perform(get("/data/ban")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", hasSize(0)));
  }

  @Test
  public void canReadBansWithScopeAndRole() throws Exception {
    mockMvc.perform(get("/data/ban")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_ADMIN_ACCOUNT_BAN)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", hasSize(2)));
  }

  @Test
  public void cannotCreateBanWithoutScope() throws Exception {
    mockMvc.perform(post("/data/ban")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, ROLE_ADMIN_ACCOUNT_BAN))
      .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
      .content(testPost))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotCreateBanWithoutRole() throws Exception {
    mockMvc.perform(post("/data/ban")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
      .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
      .content(testPost))
      .andExpect(status().isForbidden());
  }

  @Test
  public void canCreateBanWithScopeAndRole() throws Exception {
    assertThat(accountRepository.getOne(3).getBans().size(), is(0));

    mockMvc.perform(post("/data/ban")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_ADMIN_ACCOUNT_BAN))
      .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
      .content(testPost))
      .andExpect(status().isCreated());

    assertThat(accountRepository.getOne(3).getBans().size(), is(1));
  }

  @Test
  public void canCreateBanWithModerationWithScopeAndRole() throws Exception {

    final Ban ban = new Ban()
      .setScope(BanScope.CHAT)
      .setReason("Ban reason")
      .setAccount((Account) new Account().setId("3"))
      .setModerationReport((ModerationReport) new ModerationReport().setId("1"));

    mockMvc.perform(post("/data/ban")
      .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_ADMIN_ACCOUNT_BAN))
      .content(createJsonApiContent(ban)))
      .andExpect(status().isCreated())
      .andExpect(jsonPath("$.data.relationships.moderationReport.data.id", is("1")))
      .andExpect(jsonPath("$.data.relationships.account.data.id", is("3")));
  }
}
