package com.faforever.api.data;

import com.faforever.api.AbstractIntegrationTest;
import com.faforever.api.data.domain.GroupPermission;
import com.faforever.api.security.OAuthScope;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.supcomhub.api.dto.EmailDomainBan;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/globalCleanUp.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepEmailDomainBanData.sql")
public class EmailDomainBanTest extends AbstractIntegrationTest {

  private EmailDomainBan emailDomainBan;

  @Before
  public void setup() {
    emailDomainBan = new EmailDomainBan()
      .setDomain("google.com");
  }

  @Test
  public void canReadDomainBansWithScopeAndRole() throws Exception {
    mockMvc.perform(get("/data/emailDomainBan")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", hasSize(1)));
  }

  @Test
  public void canReadDomainBansWithoutScope() throws Exception {
    mockMvc.perform(get("/data/emailDomainBan")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", hasSize(0)));
  }

  @Test
  public void canReadDomainBansWithoutRole() throws Exception {
    mockMvc.perform(get("/data/emailDomainBan")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", hasSize(0)));
  }

  @Test
  public void canReadSpecificDomainBanWithScopeAndRole() throws Exception {
    mockMvc.perform(get("/data/emailDomainBan/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN)))
      .andExpect(status().isOk());
  }

  @Test
  public void cannotReadSpecificDomainBanWithoutScope() throws Exception {
    mockMvc.perform(get("/data/emailDomainBan/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotReadSpecificDomainBanWithoutRole() throws Exception {
    mockMvc.perform(get("/data/emailDomainBan/1")
      .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void canCreateDomainBanWithScopeAndRole() throws Exception {
    mockMvc.perform(
      post("/data/emailDomainBan")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(emailDomainBan)))
      .andExpect(status().isCreated());
  }

  @Test
  public void cannotCreateDomainBanWithoutScope() throws Exception {
    mockMvc.perform(
      post("/data/emailDomainBan")
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(emailDomainBan)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotCreateDomainBanWithoutRole() throws Exception {
    mockMvc.perform(
      post("/data/emailDomainBan")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(emailDomainBan)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void canUpdateDomainBanWithScopeAndRole() throws Exception {
    mockMvc.perform(
      patch("/data/emailDomainBan/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(emailDomainBan.setDomain("google.de").setId("1"))))
      .andExpect(status().isNoContent());
  }

  @Test
  public void cannotUpdateDomainBanWithoutScope() throws Exception {
    mockMvc.perform(
      patch("/data/emailDomainBan/1")
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(emailDomainBan.setDomain("google.de").setId("1"))))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotUpdateDomainBanWithoutRole() throws Exception {
    mockMvc.perform(
      patch("/data/emailDomainBan/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(emailDomainBan.setDomain("google.de").setId("1"))))
      .andExpect(status().isForbidden());
  }

  @Test
  public void canDeleteDomainBanWithScopeAndRole() throws Exception {
    mockMvc.perform(
      delete("/data/emailDomainBan/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN)))
      .andExpect(status().isNoContent());
  }

  @Test
  public void cannotDeleteDomainBanWithoutScope() throws Exception {
    mockMvc.perform(
      delete("/data/emailDomainBan/1")
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_EMAIL_DOMAIN_BAN)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotDeleteDomainBanWithoutRole() throws Exception {
    mockMvc.perform(
      delete("/data/emailDomainBan/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES)))
      .andExpect(status().isForbidden());
  }
}
