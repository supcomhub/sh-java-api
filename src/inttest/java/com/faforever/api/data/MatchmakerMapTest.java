package com.faforever.api.data;

import com.faforever.api.AbstractIntegrationTest;
import com.faforever.api.data.domain.GroupPermission;
import com.faforever.api.security.OAuthScope;
import org.intellij.lang.annotations.Language;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/globalCleanUp.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepDefaultUser.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepMapData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepGameData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepLeaderboardData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepMatchmaker.sql")
public class MatchmakerMapTest extends AbstractIntegrationTest {
  @Language("JSON")
  private static final String NEW_MATCHMAKER_MAP_BODY = "{\n" +
    "  \"data\": {\n" +
    "    \"type\": \"matchmakerMap\",\n" +
    "    \"relationships\": {\n" +
    "      \"mapVersion\": {\n" +
    "        \"data\": {\n" +
    "          \"type\": \"mapVersion\",\n" +
    "          \"id\": \"2\"\n" +
    "        }\n" +
    "      },\n" +
    "      \"matchmakerPool\": {\n" +
    "        \"data\": {\n" +
    "          \"type\": \"matchmakerPool\",\n" +
    "          \"id\": 1\n" +
    "        }\n" +
    "      }\n" +
    "    }\n" +
    "  }\n" +
    "}";

  @Test
  public void canCreateMatchmakerMapWithScopeAndRole() throws Exception {
    mockMvc.perform(
      post("/data/matchmakerMap")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_MATCHMAKER_MAP))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(NEW_MATCHMAKER_MAP_BODY)) // magic value from prepMapData.sql
      .andExpect(status().isCreated());
  }

  @Test
  public void cannotCreateMatchmakerMapWithoutScope() throws Exception {
    mockMvc.perform(
      post("/data/matchmakerMap")
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_MATCHMAKER_MAP))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(NEW_MATCHMAKER_MAP_BODY)) // magic value from prepMapData.sql
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotCreateMatchmakerMapWithoutRole() throws Exception {
    mockMvc.perform(
      post("/data/matchmakerMap")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(NEW_MATCHMAKER_MAP_BODY)) // magic value from prepMapData.sql
      .andExpect(status().isForbidden());
  }

  @Test
  public void canDeleteMatchmakerMapWithScopeAndRole() throws Exception {
    mockMvc.perform(
      delete("/data/matchmakerMap/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_MATCHMAKER_MAP))) // magic value from prepMapData.sql
      .andExpect(status().isNoContent());
  }

  @Test
  public void cannotDeleteMatchmakerMapWithoutScope() throws Exception {
    mockMvc.perform(
      delete("/data/matchmakerMap/1")
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_MATCHMAKER_MAP))) // magic value from prepMapData.sql
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotDeleteMatchmakerMapWithoutRole() throws Exception {
    mockMvc.perform(
      delete("/data/matchmakerMap/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))) // magic value from prepMapData.sql
      .andExpect(status().isForbidden());
  }
}
