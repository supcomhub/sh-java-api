package com.faforever.api.data;

import com.faforever.api.AbstractIntegrationTest;
import com.faforever.api.data.domain.GroupPermission;
import com.faforever.api.security.OAuthScope;
import org.intellij.lang.annotations.Language;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;
import org.supcomhub.api.dto.Account;
import org.supcomhub.api.dto.NewsPost;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/globalCleanUp.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepDefaultUser.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepNewsPostData.sql")
public class NewsPostTest extends AbstractIntegrationTest {

  /*
{
  "data": {
    "type": "newsPost",
    "attributes": {
      "headline": "someHeadline",
      "body": "some body",
      "tags": "tagA tagB",
      "pinned": false
    }
  }
}
   */
  @Language("JSON")
  private static final String NEW_POST =
    "{\n" +
      "  \"data\": {\n" +
      "    \"type\": \"newsPost\",\n" +
      "    \"attributes\": {\n" +
      "      \"headline\": \"someHeadline\",\n" +
      "      \"body\": \"some body\",\n" +
      "      \"tags\": \"tagA tagB\",\n" +
      "      \"pinned\": false\n" +
      "    }\n" +
      "  }\n" +
      "}";

  private NewsPost newsPost;

  @Before
  public void setup() {
    newsPost = new NewsPost()
      .setHeadline("someHeadline")
      .setBody("someBody to love")
      .setTags("tagA tagB")
      .setPinned(false)
      .setAuthor((Account) new Account().setId("1"));
  }

  @Test
  public void canReadNewsPostsWithoutScopeAndRole() throws Exception {
    mockMvc.perform(get("/data/newsPost")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, NO_AUTHORITIES)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", hasSize(1)));
  }


  @Test
  public void canReadSpecificNewsPostWithoutScopeAndRole() throws Exception {
    mockMvc.perform(get("/data/newsPost/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, NO_AUTHORITIES)))
      .andExpect(status().isOk());
  }

  @Test
  public void canCreateNewsPostWithScopeAndRole() throws Exception {
    mockMvc.perform(
      post("/data/newsPost")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_NEWS_POST))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(newsPost)))
      .andExpect(status().isCreated());
  }

  @Test
  public void cannotCreateNewsPostWithoutScope() throws Exception {
    mockMvc.perform(
      post("/data/newsPost")
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_NEWS_POST))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(newsPost)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotCreateNewsPostWithoutRole() throws Exception {
    mockMvc.perform(
      post("/data/newsPost")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(newsPost)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void canUpdateNewsPostWithScopeAndRole() throws Exception {
    mockMvc.perform(
      patch("/data/newsPost/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_NEWS_POST))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(newsPost.setHeadline("changed").setId("1"))))
      .andExpect(status().isNoContent());
  }

  @Test
  public void cannotUpdateNewsPostWithoutScope() throws Exception {
    mockMvc.perform(
      patch("/data/newsPost/1")
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_NEWS_POST))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(newsPost.setHeadline("changed").setId("1"))))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotUpdateNewsPostWithoutRole() throws Exception {
    mockMvc.perform(
      patch("/data/newsPost/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES))
        .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
        .content(createJsonApiContent(newsPost.setHeadline("changed").setId("1"))))
      .andExpect(status().isForbidden());
  }

  @Test
  public void canDeleteNewsPostWithScopeAndRole() throws Exception {
    mockMvc.perform(
      delete("/data/newsPost/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, GroupPermission.ROLE_WRITE_NEWS_POST)))
      .andExpect(status().isNoContent());
  }

  @Test
  public void cannotDeleteNewsPostWithoutScope() throws Exception {
    mockMvc.perform(
      delete("/data/newsPost/1")
        .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_NEWS_POST)))
      .andExpect(status().isForbidden());
  }

  @Test
  public void cannotDeleteNewsPostWithoutRole() throws Exception {
    mockMvc.perform(
      delete("/data/newsPost/1")
        .with(getOAuthTokenWithTestUser(OAuthScope._ADMINISTRATIVE_ACTION, NO_AUTHORITIES)))
      .andExpect(status().isForbidden());
  }
}
