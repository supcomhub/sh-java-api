package com.faforever.api.leaderboard;

import com.faforever.api.AbstractIntegrationTest;
import com.faforever.api.data.domain.GroupPermission;
import org.junit.Test;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.Sql.ExecutionPhase;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/globalCleanUp.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepDefaultUser.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepMapData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepGameData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepLeaderboardData.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepMatchmaker.sql")
@Sql(executionPhase = ExecutionPhase.BEFORE_TEST_METHOD, scripts = "classpath:sql/prepLeaderboardData.sql")
public class LeaderboardEntryElideTest extends AbstractIntegrationTest {

  @Test
  public void nonEmptyResultLeaderboardEntryWithoutScopeAndRole() throws Exception {
    mockMvc.perform(get("/data/leaderboardEntry")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_TUTORIAL)))
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.data", hasSize(5)));
  }

  @Test
  public void canReadSpecificLeaderboardEntryWithoutScopeAndRole() throws Exception {
    mockMvc.perform(get("/data/leaderboardEntry/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_TUTORIAL)))
      .andExpect(status().isOk());
  }

  @Test
  public void cannotDeleteLeaderboardEntryWithoutScope() throws Exception {
    mockMvc.perform(delete("/data/leaderboardEntry/1")
      .with(getOAuthTokenWithTestUser(NO_SCOPE, GroupPermission.ROLE_WRITE_TUTORIAL)))
      .andExpect(status().isForbidden());
  }
}
