DELETE FROM game_review_summary;
DELETE FROM game_review;
DELETE FROM game_review_summary; -- deleting game_review triggers recalculation of game_review_summary

DELETE FROM mod_version_review_summary;
DELETE FROM mod_version_review;
DELETE FROM mod_version_review_summary;-- see above

DELETE FROM map_version_review_summary;
DELETE FROM map_version_review;
DELETE FROM map_version_review_summary;-- see above

DELETE
FROM sh.public.news_post;
DELETE
FROM account_verification;
DELETE
FROM audit_log;
DELETE
FROM avatar_assignment;
DELETE
FROM avatar;
DELETE
FROM ban;
DELETE
FROM clan_membership;
DELETE
FROM clan;
DELETE
FROM coop_leaderboard;
DELETE
FROM coop_mission;
DELETE
FROM email_address;
DELETE
FROM email_domain_ban;
DELETE
FROM event_definition;
DELETE
FROM group_permission_assignment;
DELETE
FROM group_permission;
DELETE
FROM ladder_division_score;
DELETE
FROM ladder_division;
DELETE
FROM matchmaker_map;
DELETE
FROM matchmaker_pool;
DELETE
FROM user_group_membership;
DELETE
FROM user_group;
DELETE
FROM message;
DELETE
FROM name_record;
DELETE
FROM oauth_client;
DELETE
FROM player_achievement;
DELETE
FROM player_event;
DELETE
FROM leaderboard_rating_journal;
DELETE
FROM leaderboard_rating;
DELETE
FROM leaderboard;
DELETE
FROM reported_user;
DELETE
FROM moderation_report;
DELETE
FROM social_relation;
DELETE
FROM teamkill_report;
DELETE
FROM terms_of_service;
DELETE
FROM tutorial;
DELETE
FROM tutorial_category;
DELETE
FROM voting_answer;
DELETE
FROM vote;
DELETE
FROM voting_choice;
DELETE
FROM voting_question_winner;
DELETE
FROM voting_question;
DELETE
FROM voting_subject;
DELETE
FROM game_participant;
DELETE
FROM game;
DELETE
FROM mod_version;
DELETE
FROM mod;
DELETE
FROM map_version;
DELETE
FROM map;
DELETE
FROM featured_mod;
DELETE
FROM account_note;
DELETE
FROM account;
