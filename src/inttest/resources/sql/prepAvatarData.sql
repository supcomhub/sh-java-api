DELETE
FROM avatar_assignment;
DELETE
FROM avatar;
DELETE
FROM group_permission;
DELETE
FROM group_permission_assignment;

INSERT INTO avatar (id, url, description)
VALUES (1, 'http://localhost/faf/avatars/avatar1.png', 'Avatar No. 1'),
       (2, 'http://localhost/faf/avatars/avatar2.png', 'Avatar No. 2');

INSERT INTO avatar_assignment (id, account_id, avatar_id, selected)
VALUES (1, 5, 2, true),
       (2, 2, 2, true);

INSERT INTO group_permission(id, technical_name, name_key)
VALUES (1, 'UPDATE_AVATAR', 'UPDATE_AVATAR');

INSERT INTO group_permission_assignment (group_id, permission_id)
VALUES (2, 1); -- MODERATOR -> UPDATE_AVATAR

SELECT setval(pg_get_serial_sequence('avatar', 'id'), max(id))
FROM avatar;
SELECT setval(pg_get_serial_sequence('avatar_assignment', 'id'), max(id))
FROM avatar_assignment;
SELECT setval(pg_get_serial_sequence('group_permission', 'id'), max(id))
FROM group_permission;
SELECT setval(pg_get_serial_sequence('group_permission_assignment', 'id'), max(id))
FROM group_permission_assignment;
