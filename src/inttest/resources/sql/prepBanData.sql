DELETE FROM ban;

INSERT INTO ban (id, account_id, author_id, reason, expiry_time, scope, revocation_time, revocation_reason)
VALUES (1, 4, 1, 'Test permaban', CURRENT_DATE + interval '1 day', 'GLOBAL', null, null),
       (2, 2, 1, 'To be revoked ban', CURRENT_DATE + interval '1 day', 'GLOBAL', NOW(), 'Test revoke');

SELECT setval(pg_get_serial_sequence('ban', 'id'), max(id))
FROM ban;
