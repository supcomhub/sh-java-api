DELETE FROM clan_membership;
DELETE FROM clan;

INSERT INTO account (id, display_name, password)
VALUES (10, 'CLAN_FOUNDER', 'not used'),
       (11, 'CLAN_LEADER', 'not used'),
       (12, 'CLAN_MEMBER_A', 'not used'),
       (13, 'CLAN_MEMBER_B', 'not used');

INSERT INTO email_address (account_id, plain, distilled, "primary")
VALUES (10, 'clan_founder@supcomhub.org', 'clan_founder@supcomhub.org', true),
       (11, 'clan_leader@supcomhub.org', 'clan_leader@supcomhub.org', true),
       (12, 'clan_member@supcomhub.org', 'clan_member@supcomhub.org', true),
       (13, 'clan_member_b@supcomhub.org', 'clan_member_b@supcomhub.org', true);

INSERT INTO clan (id, name, tag, founder_id, leader_id, description) VALUES (1,
                                                                             'Alpha Clan',
                                                                             '123',
                                                                             10,
                                                                             11,
                                                                             'Lorem ipsum dolor sit amet, consetetur sadipscing elitr'),
                                                                            (2,
                                                                             'Beta Clan',
                                                                             '345',
                                                                             1,
                                                                             1,
                                                                             'Sed diam nonumy eirmod tempor invidunt ut labore');

INSERT INTO clan_membership (id, clan_id, member_id)
VALUES (1, 1, 11),
       (2, 1, 12),
       (3, 1, 13),
       (4, 2, 10);

SELECT setval(pg_get_serial_sequence('account', 'id'), max(id))
FROM account;
SELECT setval(pg_get_serial_sequence('email_address', 'id'), max(id))
FROM email_address;
SELECT setval(pg_get_serial_sequence('clan', 'id'), max(id))
FROM clan;
SELECT setval(pg_get_serial_sequence('clan_membership', 'id'), max(id))
FROM clan_membership;
