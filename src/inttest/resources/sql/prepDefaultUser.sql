DELETE
FROM leaderboard_rating;
DELETE
FROM avatar_assignment;
DELETE FROM reported_user;
DELETE FROM moderation_report;
DELETE
FROM social_relation;
DELETE
FROM oauth_client;
DELETE FROM ban;
DELETE FROM clan_membership;
DELETE FROM clan;
DELETE
FROM game_participant;
DELETE FROM game_review;
DELETE
FROM game_review_summary;
DELETE
FROM game;
DELETE
FROM matchmaker_map;
DELETE
FROM matchmaker_pool;
DELETE
FROM map_version_review;
DELETE
FROM map_version_review_summary;
DELETE
FROM map_version;
DELETE
FROM map;
DELETE
FROM featured_mod;
DELETE
FROM name_record;
DELETE FROM group_permission_assignment;
DELETE FROM group_permission;
DELETE
FROM user_group_membership;
DELETE FROM user_group;
DELETE
FROM email_address;
DELETE
FROM email_domain_ban;
DELETE
FROM account_verification;
DELETE
FROM account;

INSERT INTO oauth_client (client_id, name, client_secret, redirect_uris, default_redirect_uri, default_scope)
VALUES ('00000000-0000-0000-0000-000000000000',
        'test',
        '{noop}test',
        'http://localhost https://www.getpostman.com/oauth2/callback ',
        'http://localhost',
        'read_events read_achievements upload_map upload_mod upload_avatar write_account_data vote');

INSERT INTO account (id, display_name, password)
VALUES (1, 'USER', '{noop}USER'),
       (2, 'MODERATOR', '{noop}MODERATOR'),
       (3, 'ADMIN', '{noop}ADMIN'),
       (4, 'BANNED', '{noop}not relevant'),
       (5, 'ACTIVE_USER', '{noop}ACTIVE_USER');

INSERT INTO email_address (account_id, plain, distilled, "primary")
VALUES (1, 'user@supcomhub.org', 'user@supcomhub.org', true),
       (2, 'moderator@supcomhub.org', 'moderator@supcomhub.org', true),
       (3, 'admin@supcomhub.org', 'admin@supcomhub.org', true),
       (4, 'banned@supcomhub.org', 'banned@supcomhub.org', true),
       (5, 'test-user@supcomhub.org', 'test-user@supcomhub.org', true);

INSERT INTO account_verification (account_id, type, value)
VALUES (2, 'STEAM', '1234');

INSERT INTO user_group (id, technical_name, name_key, parent_group_id, public)
VALUES (1, 'ADMINISTRATOR', 'administrator', null, true),
       (2, 'MODERATOR', 'moderator', null, true);

INSERT INTO user_group_membership (member_id, group_id)
VALUES (2, 2),
       (3, 1);

INSERT INTO name_record (account_id, previous_name, new_name)
VALUES (2, 'MODERATOR', 'OLD_MODERATOR');

SELECT setval(pg_get_serial_sequence('account', 'id'), max(id))
FROM account;
SELECT setval(pg_get_serial_sequence('user_group', 'id'), max(id))
FROM user_group;
