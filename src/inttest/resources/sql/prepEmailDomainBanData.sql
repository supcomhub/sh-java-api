DELETE
FROM email_domain_ban;

INSERT INTO email_domain_ban (id, domain)
VALUES (1, 'spam.org');

SELECT setval(pg_get_serial_sequence('email_domain_ban', 'id'), max(id))
FROM email_domain_ban;
