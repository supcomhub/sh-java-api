DELETE
FROM game;
DELETE
FROM featured_mod;

INSERT INTO featured_mod (id,
                          short_name,
                          display_name,
                          description,
                          public,
                          git_url,
                          git_branch,
                          current_version,
                          allow_override,
                          ordinal)
VALUES (1,
        'faf',
        'FAF',
        'Forged Alliance Forever',
        true,
        'https://github.com/FAForever/fa.git',
        'deploy/faf',
        1,
        FALSE,
        1),
       (6,
        'ladder1v1',
        'Ladder 1v1',
        'Ladder games',
        true,
        'https://github.com/FAForever/fa.git',
        'deploy/faf',
        1,
        TRUE,
        2),
       (25,
        'coop',
        'Coop',
        'Multiplayer campaign games',
        true,
        'https://github.com/FAForever/fa-coop.git',
        'master',
        1,
        TRUE,
        3);

INSERT INTO game (id, start_time, name, victory_condition, featured_mod_id, host_id, map_version_id, validity)
VALUES (1, NOW(), 'Test game', 'DEMORALIZATION', 6, 1, 1, 'VALID');

SELECT setval(pg_get_serial_sequence('featured_mod', 'id'), max(id))
FROM featured_mod;
SELECT setval(pg_get_serial_sequence('game', 'id'), max(id))
FROM game;
