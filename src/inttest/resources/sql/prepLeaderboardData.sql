DELETE
FROM leaderboard_rating;
DELETE
FROM leaderboard;

insert into leaderboard (id, technical_name, name_key, description_key)
values (1, 'global', 'leaderboard.global.name', 'leaderboard.global.description'),
       (2, 'ladder1v1', 'ladder1v1.global.name', 'ladder1v1.global.description');

insert into leaderboard_rating (id, account_id, mean, deviation, total_games, won_games, leaderboard_id)
values (1, 1, 2000, 125, 10, 10, 1),
       (2, 2, 1500, 75, 10, 7, 1),
       (3, 3, 1650, 62.52, 10, 8, 1),
       (4, 4, 2000, 125, 10, 10, 2),
       (5, 5, 1500, 75, 10, 7, 2);

SELECT setval(pg_get_serial_sequence('leaderboard', 'id'), max(id))
FROM leaderboard;
SELECT setval(pg_get_serial_sequence('leaderboard_rating', 'id'), max(id))
FROM leaderboard_rating;

REFRESH MATERIALIZED VIEW leaderboard_entry;
