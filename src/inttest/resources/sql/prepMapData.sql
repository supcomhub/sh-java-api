DELETE
FROM matchmaker_map;
DELETE
FROM map_version;
DELETE
FROM map;


INSERT INTO map (id, display_name, map_type, battle_type, uploader_id)
VALUES (1, 'SCMP_001', 'FFA', 'skirmish', 1),
       (2, 'SCMP_002', 'FFA', 'skirmish', 1);

INSERT INTO map_version (id, description, max_players, width, height, version, filename, hidden, map_id)
VALUES (1, 'SCMP 001', 8, 5, 5, 1, 'maps/scmp_001.v0001.zip', false, 1),
       (2, 'SCMP 002', 8, 5, 5, 1, 'maps/scmp_002.v0001.zip', false, 2);

SELECT setval(pg_get_serial_sequence('map', 'id'), max(id))
FROM map;
SELECT setval(pg_get_serial_sequence('map_version', 'id'), max(id))
FROM map_version;
SELECT setval(pg_get_serial_sequence('matchmaker_map', 'id'), max(id))
FROM matchmaker_map;
