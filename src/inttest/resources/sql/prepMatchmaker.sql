delete from matchmaker_map;
delete from matchmaker_pool;

insert into matchmaker_pool (id, featured_mod_id, leaderboard_id, name_key, technical_name)
values (1, 6, 2, 'name', 'technical_name');

INSERT INTO matchmaker_map (id, map_version_id, matchmaker_pool_id)
VALUES (1, 1, 1);

SELECT setval(pg_get_serial_sequence('matchmaker_pool', 'id'), max(id))
FROM matchmaker_pool;
SELECT setval(pg_get_serial_sequence('matchmaker_map', 'id'), max(id))
FROM matchmaker_map;
