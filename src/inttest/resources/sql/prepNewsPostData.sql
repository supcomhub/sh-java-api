DELETE
FROM news_post;

INSERT INTO news_post (id, author_id, headline, body, tags, pinned)
  VALUES (1, 1, 'Some news post', 'Some news body', 'ladder cool', false);

SELECT setval(pg_get_serial_sequence('news_post', 'id'), max(id))
FROM news_post;
