DELETE
FROM teamkill_report;

INSERT INTO teamkill_report (id, teamkiller_id, victim_id, game_id, game_time)
VALUES (1, 2, 1, 1, 5000);

SELECT setval(pg_get_serial_sequence('teamkill_report', 'id'), max(id))
FROM teamkill_report;

