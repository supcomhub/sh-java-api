DELETE FROM tutorial;
DELETE FROM tutorial_category;
DELETE
FROM message;

INSERT INTO tutorial_category (id, technical_name, category_key)
VALUES (1, 'category', 'category');

INSERT INTO tutorial (id, title_key, description_key, category, image, ordinal, launchable, technical_name) VALUES (1,
                                                                                                                    'title',
                                                                                                                    'description',
                                                                                                                    1,
                                                                                                                    'image.png',
                                                                                                                    1,
                                                                                                                    true,
                                                                                                                    'tec name');

INSERT INTO message (key, language, region, value)
VALUES ('title', 'en', 'US', 'title');
INSERT INTO message (key, language, region, value)
VALUES ('description', 'en', 'US', 'description');
INSERT INTO message (key, language, region, value)
VALUES ('category', 'en', 'US', 'category');

SELECT setval(pg_get_serial_sequence('tutorial_category', 'id'), max(id))
FROM tutorial_category;
SELECT setval(pg_get_serial_sequence('tutorial', 'id'), max(id))
FROM tutorial;
