DELETE
FROM account_note;

INSERT INTO account_note (id, account_id, author_id, watched, note)
VALUES (1, 4, 1, false, 'Test user note');

SELECT setval(pg_get_serial_sequence('account_note', 'id'), max(id))
FROM account_note;
