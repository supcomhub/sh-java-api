DELETE
FROM voting_question_winner;
DELETE FROM voting_answer;
DELETE FROM vote;
DELETE FROM voting_choice;
DELETE FROM voting_question;
DELETE FROM voting_subject;

INSERT INTO voting_subject (id, subject_key, begin_of_vote_time, end_of_vote_time, min_games_to_vote, description_key, topic_url)
VALUES (1, 'subject', CURRENT_DATE - INTERVAL '1 MINUTE', CURRENT_DATE + INTERVAL '1 YEAR', 0, 'des', 'www.google.de');
SELECT setval(pg_get_serial_sequence('account_note', 'id'), max(id))
FROM account_note;

INSERT INTO voting_subject (id, subject_key, reveal_winner, begin_of_vote_time, end_of_vote_time, min_games_to_vote, description_key, topic_url)
VALUES (2,
        'subject',
        false,
        CURRENT_DATE - INTERVAL '1 YEAR',
        CURRENT_DATE - INTERVAL '45 DAY',
        0,
        'des',
        'www.google.de');

INSERT INTO voting_question (id, max_answers, question_key, voting_subject_id, description_key, ordinal, alternative_voting)
VALUES (1, 2, 'question', 1, 'des', 1, true);

INSERT INTO voting_choice (id, choice_text_key, voting_question_id, description_key, ordinal) VALUES (1,
                                                                                                      'text',
                                                                                                      1,
                                                                                                      'des',
                                                                                                      1),
                                                                                                     (2, 'text', 1, 'des', 2);

INSERT INTO vote (id, voter_id, voting_subject_id)
VALUES
  (1, 1, 1);

INSERT INTO voting_question (id, max_answers, question_key, voting_subject_id, description_key, ordinal, alternative_voting)
VALUES (2, 2, 'question', 2, 'des', 1, true);

INSERT INTO voting_choice (id, choice_text_key, voting_question_id, description_key, ordinal) VALUES
  (3, 'text', 2, 'des', 2);

INSERT INTO vote (id, voter_id, voting_subject_id)
VALUES
  (2, 3, 2);

INSERT INTO voting_answer (id, vote_id, voting_choice_id, alternative_ordinal) VALUES
  (1, 2, 3, 0);

SELECT setval(pg_get_serial_sequence('voting_subject', 'id'), max(id))
FROM voting_subject;
SELECT setval(pg_get_serial_sequence('voting_answer', 'id'), max(id))
FROM voting_answer;
SELECT setval(pg_get_serial_sequence('vote', 'id'), max(id))
FROM vote;
SELECT setval(pg_get_serial_sequence('voting_choice', 'id'), max(id))
FROM voting_choice;
SELECT setval(pg_get_serial_sequence('voting_question', 'id'), max(id))
FROM voting_question;
