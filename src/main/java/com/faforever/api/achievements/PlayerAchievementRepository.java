package com.faforever.api.achievements;

import com.faforever.api.data.domain.PlayerAchievement;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PlayerAchievementRepository extends JpaRepository<PlayerAchievement, UUID> {

  Optional<PlayerAchievement> findOneByAchievementIdAndPlayerId(UUID achievementId, int playerId);
}
