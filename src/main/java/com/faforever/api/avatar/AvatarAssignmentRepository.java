package com.faforever.api.avatar;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.Avatar;
import com.faforever.api.data.domain.AvatarAssignment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AvatarAssignmentRepository extends JpaRepository<AvatarAssignment, Integer> {
  Optional<AvatarAssignment> findOneByAvatarAndAccount(Avatar avatar, Account account);

  Optional<AvatarAssignment> findOneByAvatarIdAndAccountId(int avatarId, int playerId);

  Optional<AvatarAssignment> findOneById(Integer i);
}
