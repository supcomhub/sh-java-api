package com.faforever.api.clan;

import com.faforever.api.clan.result.ClanMemberResult;
import com.faforever.api.clan.result.ClanResult;
import com.faforever.api.clan.result.MeResult;
import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.Clan;
import com.faforever.api.user.AccountService;
import com.google.common.collect.ImmutableMap;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.inject.Inject;
import java.io.Serializable;
import java.util.Map;


@RestController
@RequestMapping(path = ClansController.PATH)
public class ClansController {

  static final String PATH = "/clans";
  private final ClanService clanService;
  private final AccountService accountService;

  @Inject
  public ClansController(ClanService clanService, AccountService accountService) {
    this.clanService = clanService;
    this.accountService = accountService;
  }

  @ApiOperation("Grab data about yourself and the clan")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Success with JSON { account: {id: ?, displayName: ?}, clan: { id: ?, name: ?, tag: ?}}"),
    @ApiResponse(code = 400, message = "Bad Request")})
  @RequestMapping(path = "/me", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public MeResult me(Authentication authentication) {
    Account account = accountService.getAccount(authentication);

    Clan clan = account.getClan();
    ClanResult clanResult = null;
    if (clan != null) {
      clanResult = ClanResult.of(clan);
    }
    return new MeResult(ClanMemberResult.of(account), clanResult);
  }

  // This request cannot be handled by JSON API because we must simultaneously create two resources (a,b)
  // a: the new clan with the leader membership, b: the leader membership with the new clan
  @ApiOperation("Create a clan with correct leader, founder and clan membership")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Success with JSON { id: ?, type: 'clan'}"),
    @ApiResponse(code = 400, message = "Bad Request")})
  @RequestMapping(path = "/create", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @PreAuthorize("hasRole('ROLE_USER')")
  @Transactional
  public Map<String, Serializable> createClan(@RequestParam("name") String name,
                                              @RequestParam("tag") String tag,
                                              @RequestParam(value = "description", required = false) String description,
                                              Authentication authentication) {
    Account account = accountService.getAccount(authentication);
    Clan clan = clanService.create(name, tag, description, account);
    return ImmutableMap.of("id", clan.getId(), "type", "clan");
  }

  @ApiOperation("Generate invitation link")
  @ApiResponses({
    @ApiResponse(code = 200, message = "Success with JSON { jwtToken: ? }"),
    @ApiResponse(code = 400, message = "Bad Request")})
  @RequestMapping(path = "/generateInvitationLink",
      method = RequestMethod.GET,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  public Map<String, Serializable> generateInvitationLink(
    @RequestParam("clanId") Integer clanId,
    @RequestParam("playerId") Integer newMemberId,
    Authentication authentication) {
    Account account = accountService.getAccount(authentication);
    String jwtToken = clanService.generatePlayerInvitationToken(account, newMemberId, clanId);
    return ImmutableMap.of("jwtToken", jwtToken);
  }

  @ApiOperation("Check invitation link and add Member to Clan")
  @RequestMapping(path = "/joinClan",
      method = RequestMethod.POST,
      produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
  @Transactional
  public void joinClan(
    @RequestParam("token") String stringToken,
    Authentication authentication) {
    clanService.acceptPlayerInvitationToken(stringToken, authentication);
  }
}
