package com.faforever.api.clan.result;

import com.faforever.api.data.domain.Account;
import lombok.Data;

@Data
public class ClanMemberResult {
  private final Integer id;
  private final String displayName;

  public static ClanMemberResult of(Account newMember) {
    return new ClanMemberResult(newMember.getId(), newMember.getDisplayName());
  }
}
