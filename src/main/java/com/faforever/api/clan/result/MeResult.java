package com.faforever.api.clan.result;

import lombok.Data;

@Data
public class MeResult {
  private final ClanMemberResult member;
  private final ClanResult clan;
}

