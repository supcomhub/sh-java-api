package com.faforever.api.config;

import com.faforever.api.config.FafApiProperties.Anope;
import com.faforever.api.user.AnopeUserRepository;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class AnopeConfig {

  private final FafApiProperties properties;

  public AnopeConfig(FafApiProperties properties) {
    this.properties = properties;
  }

  @Bean
  public AnopeUserRepository anopeUserRepository() {
    return new AnopeUserRepository(anopeJdbcTemplate());
  }

  private NamedParameterJdbcTemplate anopeJdbcTemplate() {
    return new NamedParameterJdbcTemplate(anopeDataSource());
  }

  private DataSource anopeDataSource() {
    Anope anopeProperties = properties.getAnope();

    HikariConfig config = new HikariConfig();
    config.setJdbcUrl(anopeProperties.getJdbcUrl());
    config.setUsername(anopeProperties.getUsername());
    config.setPassword(anopeProperties.getPassword());

    return new HikariDataSource(config);
  }
}
