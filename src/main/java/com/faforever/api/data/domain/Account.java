package com.faforever.api.data.domain;

import com.faforever.api.data.checks.IsEntityOwner;
import com.faforever.api.data.checks.Prefab;
import com.faforever.api.security.elide.permission.AdminAccountBanCheck;
import com.faforever.api.security.elide.permission.AdminAccountNoteCheck;
import com.faforever.api.security.elide.permission.AdminModerationReportCheck;
import com.faforever.api.security.elide.permission.ReadAccountPrivateDetailsCheck;
import com.faforever.api.security.elide.permission.ReadUserGroupCheck;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yahoo.elide.annotation.ComputedAttribute;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.ReadPermission;
import com.yahoo.elide.annotation.SharePermission;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.OffsetDateTime;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Setter
@Entity
@Table(name = "account")
@Include(rootLevel = true, type = org.supcomhub.api.dto.Account.TYPE_NAME)
// Needed to change leader of a clan
@SharePermission
@ToString(of = {"displayName"}, callSuper = true)
public class Account extends AbstractIntegerIdEntity implements OwnableEntity {

  private String displayName;
  private Set<Ban> bans = new HashSet<>();
  private Set<EmailAddress> emailAddresses = new HashSet<>();
  private String password;
  private Set<UserNote> userNotes;
  private Set<ClanMembership> clanMemberships = new HashSet<>();
  private Set<NameRecord> nameRecords = new HashSet<>();
  private Set<AccountVerification> accountVerifications = new HashSet<>();
  private Set<AvatarAssignment> avatarAssignments = new HashSet<>();
  private Set<ModerationReport> reporterOnModerationReports = new HashSet<>();
  private Set<ModerationReport> reportedOnModerationReports = new HashSet<>();
  private Set<UserGroup> userGroups = new HashSet<>();
  private String lastLoginUserAgent;
  private String lastLoginIpAddress;
  private OffsetDateTime lastLoginTime;

  // Permission is managed by ClanMembership class
  @UpdatePermission(expression = Prefab.ALL)
  @OneToMany(mappedBy = "member")
  @BatchSize(size = 1000)
  public Set<ClanMembership> getClanMemberships() {
    return this.clanMemberships;
  }

  @Transient
  public Clan getClan() {
    return getClanMemberships().stream()
      .findFirst()
      .map(ClanMembership::getClan)
      .orElse(null);
  }

  // Permission is managed by NameRecord class
  @UpdatePermission(expression = Prefab.ALL)
  @OneToMany(mappedBy = "account")
  public Set<NameRecord> getNameRecords() {
    return this.nameRecords;
  }

  // Permission is managed by AvatarAssignment class
  @UpdatePermission(expression = Prefab.ALL)
  @OneToMany(mappedBy = "account")
  @BatchSize(size = 1000)
  public Set<AvatarAssignment> getAvatarAssignments() {
    return avatarAssignments;
  }

  @UpdatePermission(expression = Prefab.ALL)
  @OneToMany(mappedBy = "account", cascade = CascadeType.ALL)
  @BatchSize(size = 1000)
  public Set<AccountVerification> getAccountVerifications() {
    return accountVerifications;
  }

  @ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + AdminModerationReportCheck.EXPRESSION)
  // Permission is managed by Moderation reports class
  @UpdatePermission(expression = Prefab.ALL)
  @OneToMany(mappedBy = "reporter")
  @BatchSize(size = 1000)
  public Set<ModerationReport> getReporterOnModerationReports() {
    return reporterOnModerationReports;
  }

  @ReadPermission(expression = AdminModerationReportCheck.EXPRESSION)
  // Permission is managed by Moderation reports class
  @UpdatePermission(expression = Prefab.ALL)
  @ManyToMany(mappedBy = "reportedUsers")
  public Set<ModerationReport> getReportedOnModerationReports() {
    return reportedOnModerationReports;
  }

  @ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + ReadUserGroupCheck.EXPRESSION)
  @UpdatePermission(expression = Prefab.ALL)
  @ManyToMany(mappedBy = "members")
  public Set<UserGroup> getUserGroups() {
    return userGroups;
  }

  @Column(name = "display_name")
  public String getDisplayName() {
    return displayName;
  }

  @OneToMany(mappedBy = "account", fetch = FetchType.EAGER)
  @ReadPermission(expression = AdminAccountBanCheck.EXPRESSION)
  // Permission is managed by Ban class
  @UpdatePermission(expression = Prefab.ALL)
  public Set<Ban> getBans() {
    return this.bans;
  }

  @OneToMany(mappedBy = "account", fetch = FetchType.EAGER)
  @ReadPermission(expression = AdminAccountNoteCheck.EXPRESSION)
  // Permission is managed by UserNote class
  @UpdatePermission(expression = Prefab.ALL)
  public Set<UserNote> getUserNotes() {
    return this.userNotes;
  }

  @Transient
  @ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + ReadAccountPrivateDetailsCheck.EXPRESSION)
  @ComputedAttribute
  public String getPrimaryEmailAddress() {
    return getEmailAddresses().stream()
      .filter(EmailAddress::isPrimary)
      .findFirst()
      .map(EmailAddress::getPlain)
      .orElse(null);
  }

  @OneToMany(fetch = FetchType.EAGER, mappedBy = "owner", cascade = CascadeType.ALL)
  @ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + ReadAccountPrivateDetailsCheck.EXPRESSION)
  @Fetch(FetchMode.JOIN)
  public Set<EmailAddress> getEmailAddresses() {
    return emailAddresses;
  }

  @Column(name = "password")
  @ReadPermission(expression = Prefab.NONE)
  public String getPassword() {
    return password;
  }

  @Column(name = "last_login_user_agent")
  @ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + ReadAccountPrivateDetailsCheck.EXPRESSION)
  public String getLastLoginUserAgent() {
    return lastLoginUserAgent;
  }

  @Column(name = "last_login_ip_address")
  @ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + ReadAccountPrivateDetailsCheck.EXPRESSION)
  public String getLastLoginIpAddress() {
    return lastLoginIpAddress;
  }

  @Column(name = "last_login_time")
  @ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + ReadAccountPrivateDetailsCheck.EXPRESSION)
  public OffsetDateTime getLastLoginTime() {
    return lastLoginTime;
  }

  @Transient
  private Set<Ban> getActiveBans() {
    return getBans().stream().filter(ban -> ban.getBanStatus() == BanStatus.ACTIVE).collect(Collectors.toSet());
  }

  @Transient
  public boolean isGlobalBanned() {
    return getActiveBans().stream().anyMatch(ban -> ban.getScope() == BanScope.GLOBAL);
  }

  @Override
  @Transient
  @JsonIgnore
  public Account getEntityOwner() {
    return this;
  }
}
