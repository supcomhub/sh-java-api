package com.faforever.api.data.domain;

import com.faforever.api.data.checks.IsEntityOwner;
import com.faforever.api.data.type.PostgreSQLEnumType;
import com.faforever.api.security.elide.permission.ReadAccountPrivateDetailsCheck;
import com.yahoo.elide.annotation.Audit;
import com.yahoo.elide.annotation.Audit.Action;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.ReadPermission;
import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "account_verification")
@Include(rootLevel = true, type = AccountVerification.TYPE_NAME)
@NoArgsConstructor
@Setter
@AllArgsConstructor
@EqualsAndHashCode(of = {"account", "type", "value"}, callSuper = false)
@Audit(action = Action.CREATE, logStatement = "Account verification ''{0}'' has been assigned to account ''{1}''", logExpressions = {"${accountVerification.type}", "${accountVerification.account.id}"})
@Audit(action = Action.DELETE, logStatement = "Account verification ''{0}'' has been removed from account ''{1}''", logExpressions = {"${accountVerification.type}", "${accountVerification.account.id}"})
public class AccountVerification extends AbstractIntegerIdEntity implements OwnableEntity {
  public static final String TYPE_NAME = "accountVerification";

  private Account account;
  private Type type;
  /** See {@link Event.Type} for information about which type sets which value. */
  private String value;

  @ManyToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "account_id")
  public Account getAccount() {
    return account;
  }

  @Column(name = "type")
  @Enumerated(EnumType.STRING)
  @org.hibernate.annotations.Type(type = PostgreSQLEnumType.TYPE_NAME)
  public Type getType() {
    return type;
  }

  @ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + ReadAccountPrivateDetailsCheck.EXPRESSION)
  @Column(name = "value")
  public String getValue() {
    return value;
  }

  @Override
  @Transient
  public Account getEntityOwner() {
    return account;
  }

  public enum Type {
    /**
     * The account was migrated from FAF.
     */
    FAF,
    /**
     * The account holder proved that he also owns a Steam account that has Forged Alliance: Forever. The value to be
     * stored is the Steam account ID.
     */
    STEAM,
    /** The account holder verified his identity via Telegram. The value to be stored in the Telegram ID. */
    TELEGRAM,
    /**
     * The account holder verified himself personally, e.g. is a known member of the community who others have spoken
     * to. The value to be stored is a free text, e.g. "Well-known community member". This is for internal use only and
     * not meant to be displayed to other users.
     */
    PERSONAL
  }
}
