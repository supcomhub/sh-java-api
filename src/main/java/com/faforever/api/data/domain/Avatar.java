package com.faforever.api.data.domain;

import com.faforever.api.data.checks.Prefab;
import com.faforever.api.security.elide.permission.WriteAvatarCheck;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.List;

@Entity
@Table(name = "avatar")
@Include(rootLevel = true, type = org.supcomhub.api.dto.Avatar.TYPE_NAME)
@Setter
public class Avatar extends AbstractIntegerIdEntity {

  private String url;
  private String description;
  private List<AvatarAssignment> assignments;

  @Column(name = "url")
  public @NotNull String getUrl() {
    return url;
  }

  @Column(name = "description")
  @UpdatePermission(expression = WriteAvatarCheck.EXPRESSION)
  public String getDescription() {
    return description;
  }

  // Cascading is needed for Create & Delete
  @OneToMany(mappedBy = "avatar", cascade = CascadeType.ALL, orphanRemoval = true)
  // Permission is managed by AvatarAssignment class
  @UpdatePermission(expression = Prefab.ALL)
  public List<AvatarAssignment> getAssignments() {
    return this.assignments;
  }

}
