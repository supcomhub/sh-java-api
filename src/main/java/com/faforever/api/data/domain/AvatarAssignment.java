package com.faforever.api.data.domain;

import com.faforever.api.data.checks.IsEntityOwner;
import com.faforever.api.data.checks.Prefab;
import com.faforever.api.security.elide.permission.WriteAvatarCheck;
import com.github.jasminb.jsonapi.annotations.Relationship;
import com.yahoo.elide.annotation.Audit;
import com.yahoo.elide.annotation.Audit.Action;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
@Table(name = "avatar_assignment")
@Include(rootLevel = true, type = org.supcomhub.api.dto.AvatarAssignment.TYPE_NAME)
@CreatePermission(expression = WriteAvatarCheck.EXPRESSION)
@DeletePermission(expression = WriteAvatarCheck.EXPRESSION)
@Audit(action = Action.CREATE, logStatement = "Avatar ''{0}'' has been assigned to account ''{1}''", logExpressions = {"${avatarAssignment.avatar.id}", "${avatarAssignment.account.id}"})
@Audit(action = Action.DELETE, logStatement = "Avatar ''{0}'' has been revoked from account ''{1}''", logExpressions = {"${avatarAssignment.avatar.id}", "${avatarAssignment.account.id}"})
@Setter
public class AvatarAssignment extends AbstractIntegerIdEntity implements OwnableEntity {
  public static final String TYPE_NAME = "avatarAssignment";

  private Boolean selected = Boolean.FALSE;
  private OffsetDateTime expiresAt;
  @Relationship(org.supcomhub.api.dto.Account.TYPE_NAME)
  private Account account;

  @Relationship(org.supcomhub.api.dto.AvatarAssignment.TYPE_NAME)
  private Avatar avatar;

  @Column(name = "selected")
  @UpdatePermission(expression = IsEntityOwner.EXPRESSION)
  @Audit(action = Action.UPDATE, logStatement = "Avatar ''{0}'' has been selected on account ''{1}''", logExpressions = {"${avatarAssignment.avatar.id}", "${avatarAssignment.account.id}"})
  public Boolean isSelected() {
    return selected;
  }

  @Column(name = "expiry_time")
  @UpdatePermission(expression = WriteAvatarCheck.EXPRESSION + " or Prefab.Common.UpdateOnCreate")
  @Audit(action = Action.UPDATE, logStatement = "Expiration of avatar assignment ''{0}'' has been set to ''{1}''", logExpressions = {"${avatarAssignment.id}", "${avatarAssignment.expiresAt}"})
  public OffsetDateTime getExpiresAt() {
    return expiresAt;
  }

  @UpdatePermission(expression = Prefab.UPDATE_ON_CREATE)
  @JoinColumn(name = "avatar_id")
  @ManyToOne(fetch = FetchType.LAZY)
  public @NotNull Avatar getAvatar() {
    return avatar;
  }

  @UpdatePermission(expression = Prefab.UPDATE_ON_CREATE)
  @JoinColumn(name = "account_id")
  @ManyToOne(fetch = FetchType.LAZY)
  public @NotNull Account getAccount() {
    return account;
  }

  @Override
  @Transient
  public Account getEntityOwner() {
    return getAccount();
  }
}
