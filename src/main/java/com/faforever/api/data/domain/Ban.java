package com.faforever.api.data.domain;

import com.faforever.api.data.checks.Prefab;
import com.faforever.api.data.type.PostgreSQLEnumType;
import com.faforever.api.security.FafUserDetails;
import com.faforever.api.security.elide.permission.AdminAccountBanCheck;
import com.yahoo.elide.annotation.Audit;
import com.yahoo.elide.annotation.Audit.Action;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.OnCreatePreSecurity;
import com.yahoo.elide.annotation.OnUpdatePreSecurity;
import com.yahoo.elide.annotation.ReadPermission;
import com.yahoo.elide.annotation.UpdatePermission;
import com.yahoo.elide.core.RequestScope;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.time.OffsetDateTime;

@Entity
@Table(name = "ban")
@Include(rootLevel = true, type = org.supcomhub.api.dto.Ban.TYPE_NAME)
// Bans can never be deleted, only disabled over BanDisableData
@DeletePermission(expression = Prefab.NONE)
@ReadPermission(expression = AdminAccountBanCheck.EXPRESSION)
@CreatePermission(expression = AdminAccountBanCheck.EXPRESSION)
@UpdatePermission(expression = AdminAccountBanCheck.EXPRESSION)
@Audit(action = Action.CREATE, logStatement = "Applied ban with id '{0}' for account '{1}'", logExpressions = {"${ban.id}", "${ban.account}"})
@Audit(action = Action.UPDATE, logStatement = "Updated ban with id '{0}' for account '{1}'", logExpressions = {"${ban.id}", "${ban.account}"})
@Setter
public class Ban extends AbstractIntegerIdEntity {

  private Account account;
  private Account author;
  private String reason;
  private OffsetDateTime expiryTime;
  private OffsetDateTime revocationTime;
  private String revocationReason;
  private Account revocationAuthor;
  private BanScope scope;
  private ModerationReport moderationReport;

  @JoinColumn(name = "account_id")
  @ManyToOne
  public @NotNull Account getAccount() {
    return account;
  }

  @JoinColumn(name = "author_id")
  @ManyToOne
  public @NotNull Account getAuthor() {
    return author;
  }

  @Column(name = "reason")
  public @NotNull String getReason() {
    return reason;
  }

  @Column(name = "scope")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  public BanScope getScope() {
    return scope;
  }

  @ManyToOne
  @JoinColumn(name = "report_id")
  public ModerationReport getModerationReport() {
    return moderationReport;
  }

  @Column(name = "expiry_time")
  public OffsetDateTime getExpiryTime() {
    return expiryTime;
  }

  @Column(name = "revocation_time")
  public OffsetDateTime getRevocationTime() {
    return revocationTime;
  }

  @Column(name = "revocation_reason")
  public String getRevocationReason() {
    return revocationReason;
  }

  @ManyToOne
  @JoinColumn(name = "revocation_author_id")
  public Account getRevocationAuthor() {
    return revocationAuthor;
  }

  @Transient
  public BanStatus getBanStatus() {
    if (revocationTime != null && revocationTime.isBefore(OffsetDateTime.now())) {
      return BanStatus.REVOKED;
    }
    if (expiryTime != null && !expiryTime.isAfter(OffsetDateTime.now())) {
      return BanStatus.EXPIRED;
    }
    return BanStatus.ACTIVE;
  }

  @OnCreatePreSecurity
  public void assignReporter(RequestScope scope) {
    final Object caller = scope.getUser().getOpaqueUser();
    if (caller instanceof FafUserDetails) {
      final FafUserDetails fafUser = (FafUserDetails) caller;
      final Account author = new Account();
      author.setId(fafUser.getId());
      this.author = author;
    }
  }

  @OnUpdatePreSecurity("revocationTime")
  public void revokeTimeUpdated(RequestScope scope) {
    assignRevokeAuthor(scope);
  }

  @OnUpdatePreSecurity("revocationReason")
  public void revokeReasonUpdated(RequestScope scope) {
    assignRevokeAuthor(scope);
  }

  private void assignRevokeAuthor(RequestScope scope) {
    final Object caller = scope.getUser().getOpaqueUser();
    if (caller instanceof FafUserDetails) {
      final FafUserDetails fafUser = (FafUserDetails) caller;
      final Account author = new Account();
      author.setId(fafUser.getId());
      this.revocationAuthor = author;
    }
  }
}
