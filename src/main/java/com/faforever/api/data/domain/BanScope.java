package com.faforever.api.data.domain;

public enum BanScope {
  CHAT, GLOBAL
}
