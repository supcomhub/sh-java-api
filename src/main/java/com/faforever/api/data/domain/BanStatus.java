package com.faforever.api.data.domain;

public enum BanStatus {
  ACTIVE, REVOKED, EXPIRED
}
