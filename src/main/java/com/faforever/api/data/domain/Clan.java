package com.faforever.api.data.domain;

import com.faforever.api.data.checks.IsEntityOwner;
import com.faforever.api.data.checks.Prefab;
import com.faforever.api.data.listeners.ClanEnricherListener;
import com.faforever.api.data.validation.IsLeaderInClan;
import com.yahoo.elide.annotation.ComputedAttribute;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.SharePermission;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Entity
@Table(name = "clan")
@Include(rootLevel = true, type = Clan.TYPE_NAME)
@SharePermission
@DeletePermission(expression = IsEntityOwner.EXPRESSION)
@CreatePermission(expression = Prefab.ALL)
@Setter
@IsLeaderInClan
@EntityListeners(ClanEnricherListener.class)
public class Clan extends AbstractIntegerIdEntity implements OwnableEntity {

  public static final String TYPE_NAME = "clan";

  private String name;
  private String tag;
  private Account founder;
  private Account leader;
  private String description;
  private String websiteUrl;
  private List<ClanMembership> memberships;

  @UpdatePermission(expression = IsEntityOwner.EXPRESSION)
  @Column(name = "name")
  public @NotNull String getName() {
    return name;
  }

  @UpdatePermission(expression = IsEntityOwner.EXPRESSION)
  @Column(name = "tag")
  public @Size(max = 3) @NotNull String getTag() {
    return tag;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "founder_id")
  public Account getFounder() {
    return founder;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "leader_id")
  @UpdatePermission(expression = IsEntityOwner.EXPRESSION)
  public Account getLeader() {
    return leader;
  }

  @Column(name = "description")
  @UpdatePermission(expression = IsEntityOwner.EXPRESSION)
  public String getDescription() {
    return description;
  }

  // Cascading is needed for Create & Delete
  @UpdatePermission(expression = Prefab.ALL)
  @OneToMany(mappedBy = "clan", cascade = CascadeType.ALL, orphanRemoval = true)
  // Permission is managed by ClanMembership class
  public @NotEmpty(message = "At least the leader should be in the clan") List<ClanMembership> getMemberships() {
    return this.memberships;
  }

  @Transient
  @ComputedAttribute
  public String getWebsiteUrl() {
    return websiteUrl;
  }

  @Override
  @Transient
  public Account getEntityOwner() {
    return getLeader();
  }
}
