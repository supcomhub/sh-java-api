package com.faforever.api.data.domain;

import com.faforever.api.data.listeners.CoopMapEnricher;
import com.faforever.api.data.type.PostgreSQLEnumType;
import com.yahoo.elide.annotation.ComputedAttribute;
import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "coop_mission")
@EntityListeners(CoopMapEnricher.class)
@Include(rootLevel = true, type = "coopMission")
@Setter
public class CoopMission {

  private Integer id;
  private MissionType type;
  private String name;
  private String description;
  private MapVersion map;
  // Set by CoopMapEnhancer
  private String downloadUrl;
  private String thumbnailUrlLarge;
  private String thumbnailUrlSmall;

  @Column(name = "type")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  public MissionType getType() {
    return type;
  }

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  @Column(name = "name")
  public String getName() {
    return name;
  }

  @Column(name = "description")
  public String getDescription() {
    return description;
  }

  @ManyToOne
  @JoinColumn(name = "map_version_id")
  public MapVersion getMap() {
    return map;
  }

  @Transient
  @ComputedAttribute
  public String getDownloadUrl() {
    return downloadUrl;
  }

  @Transient
  @ComputedAttribute
  public String getThumbnailUrlLarge() {
    return thumbnailUrlLarge;
  }

  @Transient
  @ComputedAttribute
  public String getThumbnailUrlSmall() {
    return thumbnailUrlSmall;
  }

  private enum MissionType {
    FA, AEON, CYBRAN, UEF, CUSTOM
  }
}
