package com.faforever.api.data.domain;

import com.yahoo.elide.annotation.Include;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.time.Duration;

@Entity
@Table(name = "coop_leaderboard")
@Include(rootLevel = true, type = CoopResult.TYPE_NAME)
@Setter
public class CoopResult {

  public static final String TYPE_NAME = "coopResult";

  private Integer id;
  private CoopMission mission;
  private Game game;
  private boolean secondaryObjectives;
  private Duration duration;
  private short playerCount;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  @ManyToOne
  @JoinColumn(name = "mission")
  public CoopMission getMission() {
    return mission;
  }

  @OneToOne
  @JoinColumn(name = "game_id")
  public Game getGame() {
    return game;
  }

  @Column(name = "secondary_targets")
  public boolean getSecondaryObjectives() {
    return secondaryObjectives;
  }

  @Column(name = "time")
  @Convert(converter = TimeConverter.class)
  public Duration getDuration() {
    return duration;
  }

  @Column(name = "player_count")
  public short getPlayerCount() {
    return playerCount;
  }
}
