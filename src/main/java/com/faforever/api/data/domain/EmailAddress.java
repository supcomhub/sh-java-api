package com.faforever.api.data.domain;

import com.yahoo.elide.annotation.Include;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Setter
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "email_address")
@Include(type = EmailAddress.TYPE_NAME)
public class EmailAddress extends AbstractIntegerIdEntity {

  public static final String TYPE_NAME = "emailAddress";

  private String plain;
  private String distilled;
  private Boolean primary;
  private Account owner;

  @Column(name = "plain")
  public String getPlain() {
    return plain;
  }

  @Column(name = "distilled")
  public String getDistilled() {
    return distilled;
  }

  @Column(name = "\"primary\"")
  public Boolean isPrimary() {
    return primary;
  }

  @ManyToOne
  @JoinColumn(name = "account_id")
  public Account getOwner() {
    return owner;
  }
}
