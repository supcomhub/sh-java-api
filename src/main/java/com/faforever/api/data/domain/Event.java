package com.faforever.api.data.domain;

import com.faforever.api.data.listeners.EventLocalizationListener;
import com.faforever.api.data.type.PostgreSQLEnumType;
import com.yahoo.elide.annotation.ComputedAttribute;
import com.yahoo.elide.annotation.Exclude;
import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "event_definition")
@Include(rootLevel = true, type = Event.TYPE_NAME)
@Setter
@EntityListeners(EventLocalizationListener.class)
@ToString(callSuper = true)
public class Event extends AbstractUuidIdEntity {

  public static final String TYPE_NAME = org.supcomhub.api.dto.Event.TYPE_NAME;
  private String nameKey;
  private Type type;

  // Set by EventLocalizationListener
  private String name;

  @Column(name = "name_key")
  @Exclude
  public String getNameKey() {
    return nameKey;
  }

  @Transient
  @ComputedAttribute
  public String getName() {
    return name;
  }

  @Column(name = "type")
  @Enumerated(EnumType.STRING)
  @org.hibernate.annotations.Type(type = PostgreSQLEnumType.TYPE_NAME)
  public Type getType() {
    return type;
  }

  public enum Type {
    NUMERIC, TIME
  }
}
