package com.faforever.api.data.domain;

import com.yahoo.elide.annotation.ComputedAttribute;
import com.yahoo.elide.annotation.Include;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name = "featured_mod")
@Include(rootLevel = true, type = FeaturedMod.TYPE_NAME)
@Setter
@EntityListeners(FeaturedModEnricher.class)
public class FeaturedMod extends AbstractIntegerIdEntity {

  public static final String TYPE_NAME = org.supcomhub.api.dto.FeaturedMod.TYPE_NAME;

  private String shortName;
  private String description;
  private String displayName;
  private boolean visible;
  private int ordinal;
  private String gitUrl;
  private String gitBranch;
  private Boolean allowOverride;
  private String bireusUrl;

  @Column(name = "short_name")
  public String getShortName() {
    return shortName;
  }

  @Column(name = "description")
  public String getDescription() {
    return description;
  }

  @Column(name = "display_name")
  public String getDisplayName() {
    return displayName;
  }

  @Column(name = "public")
  public boolean isVisible() {
    return visible;
  }

  @Column(name = "ordinal")
  public int getOrdinal() {
    return ordinal;
  }

  @Column(name = "git_url")
  public String getGitUrl() {
    return gitUrl;
  }

  @Column(name = "git_branch")
  public String getGitBranch() {
    return gitBranch;
  }

  @Column(name = "allow_override")
  public Boolean isAllowOverride() {
    return allowOverride;
  }

  @Transient
  @ComputedAttribute
  // Enriched by FeaturedModEnricher
  public String getBireusUrl() {
    return bireusUrl;
  }
}
