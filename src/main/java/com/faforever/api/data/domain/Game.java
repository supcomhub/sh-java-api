package com.faforever.api.data.domain;

import com.faforever.api.data.checks.Prefab;
import com.faforever.api.data.listeners.GameEnricher;
import com.faforever.api.data.type.PostgreSQLEnumType;
import com.yahoo.elide.annotation.ComputedAttribute;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Type;
import org.jetbrains.annotations.Nullable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Table(name = "game")
@Include(rootLevel = true, type = "game")
@Immutable
@Setter
@EntityListeners(GameEnricher.class)
public class Game extends AbstractIntegerIdEntity {

  private OffsetDateTime startTime;
  private OffsetDateTime endTime;
  private VictoryCondition victoryCondition;
  private FeaturedMod featuredMod;
  private Account host;
  private MapVersion mapVersion;
  private String name;
  private Validity validity;
  private List<GameParticipant> gameParticipants;
  private String replayUrl;
  private List<GameReview> reviews;
  private GameReviewSummary reviewSummary;

  @Id
  @Column(name = "id")
  public Integer getId() {
    return id;
  }

  @Column(name = "start_time")
  public OffsetDateTime getStartTime() {
    return startTime;
  }

  @Column(name = "victory_condition")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  public VictoryCondition getVictoryCondition() {
    return victoryCondition;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "featured_mod_id")
  public FeaturedMod getFeaturedMod() {
    return featuredMod;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "host_id")
  public Account getHost() {
    return host;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "map_version_id")
  public MapVersion getMapVersion() {
    return mapVersion;
  }

  @Column(name = "name")
  public String getName() {
    return name;
  }

  @Column(name = "validity")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  public Validity getValidity() {
    return validity;
  }

  @OneToMany(mappedBy = "game")
  public List<GameParticipant> getGameParticipants() {
    return gameParticipants;
  }

  @Column(name = "end_time")
  @Nullable
  public OffsetDateTime getEndTime() {
    return endTime;
  }

  @Transient
  @ComputedAttribute
  public String getReplayUrl() {
    return replayUrl;
  }

  @OneToMany(mappedBy = "game")
  @UpdatePermission(expression = Prefab.ALL)
  public List<GameReview> getReviews() {
    return reviews;
  }

  @OneToOne(mappedBy = "game")
  @UpdatePermission(expression = Prefab.ALL)
  public GameReviewSummary getReviewSummary() {
    return reviewSummary;
  }
}
