package com.faforever.api.data.domain;

import com.faforever.api.data.type.PostgreSQLEnumType;
import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.Type;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.OffsetDateTime;

@Entity
@Table(name = "game_participant")
@Include(rootLevel = true, type = org.supcomhub.api.dto.GameParticipant.TYPE_NAME)
@Immutable
@Setter
public class GameParticipant extends AbstractIntegerIdEntity {

  private Account participant;
  private Faction faction;
  private byte color;
  private byte team;
  private byte startSpot;
  private Byte score;
  private OffsetDateTime finishTime;
  private Game game;
  private GameParticipantOutcome outcome;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "participant_id")
  public Account getParticipant() {
    return participant;
  }

  @Column(name = "faction")
  public Faction getFaction() {
    return faction;
  }

  @Column(name = "color")
  public byte getColor() {
    return color;
  }

  @Column(name = "team")
  public byte getTeam() {
    return team;
  }

  @Column(name = "start_spot")
  public byte getStartSpot() {
    return startSpot;
  }

  @Column(name = "score")
  public Byte getScore() {
    return score;
  }

  @Column(name = "finish_time")
  public OffsetDateTime getFinishTime() {
    return finishTime;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "game_id")
  public Game getGame() {
    return game;
  }

  @JoinColumn(name = "outcome")
  @Enumerated(EnumType.STRING)
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  public GameParticipantOutcome getOutcome() {
    return outcome;
  }
}
