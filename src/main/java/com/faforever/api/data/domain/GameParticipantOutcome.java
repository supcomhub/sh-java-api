package com.faforever.api.data.domain;


public enum GameParticipantOutcome {
  VICTORY,
  DRAW,
  DEFEAT
}
