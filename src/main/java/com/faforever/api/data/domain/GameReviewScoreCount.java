package com.faforever.api.data.domain;

import com.faforever.api.data.checks.Prefab;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Setter
@Include(type = "gameReviewScores")
@Entity
@Table(name = "game_review_scores")
@CreatePermission(expression = Prefab.ALL)
@Immutable
public class GameReviewScoreCount extends ReviewScoreCount {

  private GameReviewSummary gameReviewSummary;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "game_id", referencedColumnName = "game_id")
  public GameReviewSummary getGameReviewSummary() {
    return gameReviewSummary;
  }
}
