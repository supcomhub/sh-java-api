package com.faforever.api.data.domain;

import com.faforever.api.data.checks.Prefab;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Setter
@Entity
@Table(name = "leaderboard")
@Include(rootLevel = true, type = org.supcomhub.api.dto.Leaderboard.TYPE_NAME)
@DeletePermission(expression = Prefab.NONE)
@CreatePermission(expression = Prefab.NONE)
@UpdatePermission(expression = Prefab.NONE)
@Immutable
public class Leaderboard extends AbstractIntegerIdEntity {

  private String technicalName;

  private String nameKey;

  private String descriptionKey;

  private List<LeaderboardEntry> leaderboardEntries;

  @Column(name = "technical_name")
  public String getTechnicalName() {
    return technicalName;
  }

  @Column(name = "name_key")
  public String getNameKey() {
    return nameKey;
  }

  @Column(name = "description_key")
  public String getDescriptionKey() {
    return descriptionKey;
  }

  @OneToMany(mappedBy = "leaderboard")
  public List<LeaderboardEntry> getLeaderboardEntries() {
    return leaderboardEntries;
  }
}
