package com.faforever.api.data.domain;

import com.faforever.api.data.checks.Prefab;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Setter
@Entity
@Table(name = "leaderboard_entry")
@Include(rootLevel = true, type = org.supcomhub.api.dto.LeaderboardEntry.TYPE_NAME)
@DeletePermission(expression = Prefab.NONE)
@CreatePermission(expression = Prefab.NONE)
@UpdatePermission(expression = Prefab.NONE)
@Immutable
public class LeaderboardEntry extends AbstractIntegerIdEntity {

  private Account account;
  private String playerName;
  private Integer rating;
  private Integer totalGames;
  private Integer wonGames;
  private Leaderboard leaderboard;
  private int position;

  @ManyToOne
  @JoinColumn(name = "player_id")
  public Account getAccount() {
    return account;
  }

  @Column(name = "player_name")
  public String getPlayerName() {
    return playerName;
  }

  @Column(name = "rating")
  public Integer getRating() {
    return rating;
  }

  @Column(name = "total_games")
  public Integer getTotalGames() {
    return totalGames;
  }

  @Column(name = "won_games")
  public Integer getWonGames() {
    return wonGames;
  }

  @ManyToOne
  @JoinColumn(name = "leaderboard_id")
  public Leaderboard getLeaderboard() {
    return leaderboard;
  }

  @Column(name = "position")
  public int getPosition() {
    return position;
  }
}
