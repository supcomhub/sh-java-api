package com.faforever.api.data.domain;

import com.faforever.api.data.checks.IsEntityOwner;
import com.faforever.api.data.checks.Prefab;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Setter
@Include(rootLevel = true, type = "mapReviewScores")
@Entity
@Table(name = "map_review_scores")
@CreatePermission(expression = Prefab.ALL)
@DeletePermission(expression = IsEntityOwner.EXPRESSION)
@Immutable
public class MapReviewScoreCount extends ReviewScoreCount {

  private MapReviewSummary mapReviewSummary;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "map_id", referencedColumnName = "map_id")
  public MapReviewSummary getMapReviewSummary() {
    return mapReviewSummary;
  }
}
