package com.faforever.api.data.domain;

import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import org.hibernate.annotations.BatchSize;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.List;

@Entity
@Setter
@Table(name = "map_version_review_summary")
@Include(type = org.supcomhub.api.dto.MapVersionReviewSummary.TYPE_NAME)
@Immutable
public class MapVersionReviewSummary extends AbstractIntegerIdEntity implements Serializable {

  /*
   * Serializable is needed because of an Hibernate bug see:
   * https://hibernate.atlassian.net/browse/HHH-7668
   * */
  private float positive;
  private float negative;
  private float score;
  private int reviews;
  private float lowerBound;
  private MapVersion mapVersion;
  private List<MapVersionReviewScoreCount> scoreCounts;

  @Column(name = "positive")
  public float getPositive() {
    return positive;
  }

  @Column(name = "negative")
  public float getNegative() {
    return negative;
  }

  @Column(name = "score")
  public float getScore() {
    return score;
  }

  @Column(name = "reviews")
  public int getReviews() {
    return reviews;
  }

  @Column(name = "lower_bound")
  public float getLowerBound() {
    return lowerBound;
  }

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "map_version_id", insertable = false, updatable = false)
  @BatchSize(size = 1000)
  public MapVersion getMapVersion() {
    return mapVersion;
  }

  @OneToMany(mappedBy = "mapVersionReviewSummary")
  @BatchSize(size = 1000)
  public List<MapVersionReviewScoreCount> getScoreCounts() {
    return scoreCounts;
  }
}
