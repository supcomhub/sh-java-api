package com.faforever.api.data.domain;

import com.faforever.api.security.elide.permission.WriteMatchmakerMapCheck;
import com.yahoo.elide.annotation.Audit;
import com.yahoo.elide.annotation.Audit.Action;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@CreatePermission(expression = WriteMatchmakerMapCheck.EXPRESSION)
@DeletePermission(expression = WriteMatchmakerMapCheck.EXPRESSION)
@Entity
@Setter
@Table(name = "matchmaker_map")
@Include(rootLevel = true, type = org.supcomhub.api.dto.MatchmakerMap.TYPE_NAME)
@Immutable
@Audit(
  action = Action.CREATE,
  logStatement = "Added matchmaker map '{0}' with version '{1}' to pool '{2}'",
  logExpressions = {"${matchmakerMap.mapVersion.map.displayName}", "${matchmakerMap.mapVersion.version}", "${matchmakerMap.matchmakerPool.technicalName}"}
)
@Audit(
  action = Action.DELETE,
  logStatement = "Removed matchmaker map '{0}' with version '{1}' from pool '{2}'",
  logExpressions = {"${matchmakerMap.mapVersion.map.displayName}", "${matchmakerMap.mapVersion.version}", "${matchmakerMap.matchmakerPool.technicalName}"}
)
public class MatchmakerMap extends AbstractIntegerIdEntity {

  private MapVersion mapVersion;
  private MatchmakerPool matchmakerPool;

  @OneToOne
  @JoinColumn(name = "map_version_id")
  @UpdatePermission(expression = WriteMatchmakerMapCheck.EXPRESSION)
  public MapVersion getMapVersion() {
    return mapVersion;
  }

  @OneToOne
  @JoinColumn(name = "matchmaker_pool_id")
  @UpdatePermission(expression = WriteMatchmakerMapCheck.EXPRESSION)
  public MatchmakerPool getMatchmakerPool() {
    return matchmakerPool;
  }
}
