package com.faforever.api.data.domain;

import com.faforever.api.security.elide.permission.WriteMatchmakerMapCheck;
import com.faforever.api.security.elide.permission.WriteMatchmakerPoolCheck;
import com.yahoo.elide.annotation.Audit;
import com.yahoo.elide.annotation.Audit.Action;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@CreatePermission(expression = WriteMatchmakerPoolCheck.EXPRESSION)
@DeletePermission(expression = WriteMatchmakerPoolCheck.EXPRESSION)
@Entity
@Setter
@Table(name = "matchmaker_pool")
@Include(rootLevel = true, type = org.supcomhub.api.dto.MatchmakerPool.TYPE_NAME)
@Immutable
@Audit(action = Action.CREATE, logStatement = "Added matchmaker pool '{0}'", logExpressions = {"${matchmakerPool.technicalName}"})
@Audit(action = Action.DELETE, logStatement = "Removed matchmaker pool '{0}'", logExpressions = {"${matchmakerPool.technicalName}"})
public class MatchmakerPool extends AbstractIntegerIdEntity {
  private FeaturedMod featuredMod;
  private Leaderboard leaderboard;
  private String nameKey;
  private String technicalName;

  @OneToOne
  @JoinColumn(name = "featured_mod_id")
  @UpdatePermission(expression = WriteMatchmakerMapCheck.EXPRESSION)
  public FeaturedMod getFeaturedMod() {
    return featuredMod;
  }

  @OneToOne
  @JoinColumn(name = "leaderboard_id")
  @UpdatePermission(expression = WriteMatchmakerMapCheck.EXPRESSION)
  public Leaderboard getLeaderboard() {
    return leaderboard;
  }

  @Column(name = "name_key")
  public String getNameKey() {
    return nameKey;
  }

  @Column(name = "technical_name")
  public String getTechnicalName() {
    return technicalName;
  }
}
