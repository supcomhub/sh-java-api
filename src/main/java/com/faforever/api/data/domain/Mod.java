package com.faforever.api.data.domain;

import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import org.hibernate.annotations.Formula;
import org.hibernate.annotations.Immutable;
import org.hibernate.annotations.JoinColumnOrFormula;
import org.hibernate.annotations.JoinColumnsOrFormulas;
import org.hibernate.annotations.JoinFormula;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.OffsetDateTime;
import java.util.List;

@Entity
@Table(name = "\"mod\"")
@Include(rootLevel = true, type = Mod.TYPE_NAME)
@Immutable
@Setter
public class Mod extends AbstractIntegerIdEntity {

  public static final String TYPE_NAME = "mod";

  private String displayName;
  private String author;
  private List<ModVersion> versions;
  private ModVersion latestVersion;
  private Account uploader;

  @Column(name = "display_name")
  public @Size(max = 100) @NotNull String getDisplayName() {
    return displayName;
  }

  @Column(name = "author")
  public @Size(max = 100) @NotNull String getAuthor() {
    return author;
  }

  @ManyToOne
  @JoinColumn(name = "uploader_id")
  public Account getUploader() {
    return uploader;
  }

  @Formula("(SELECT MAX(mod_version.update_time) FROM mod_version WHERE mod_version.mod_id = id)")
  public OffsetDateTime getUpdateTime() {
    return updateTime;
  }

  @OneToMany(mappedBy = "mod", cascade = CascadeType.ALL, orphanRemoval = true)
  public @NotEmpty @Valid List<ModVersion> getVersions() {
    return versions;
  }

  @ManyToOne
  @JoinColumnsOrFormulas(@JoinColumnOrFormula(
    formula = @JoinFormula(
      value = "(SELECT mod_version.id FROM mod_version WHERE mod_version.mod_id = id ORDER BY mod_version.version DESC LIMIT 1)",
      referencedColumnName = "id")
  ))
  public ModVersion getLatestVersion() {
    return latestVersion;
  }
}
