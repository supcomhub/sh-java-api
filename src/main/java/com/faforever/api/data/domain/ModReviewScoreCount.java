package com.faforever.api.data.domain;

import com.faforever.api.data.checks.IsEntityOwner;
import com.faforever.api.data.checks.Prefab;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Setter
@Include(rootLevel = true, type = "modReviewScores")
@Entity
@Table(name = "mod_review_scores")
@CreatePermission(expression = Prefab.ALL)
@DeletePermission(expression = IsEntityOwner.EXPRESSION)
@Immutable
public class ModReviewScoreCount extends ReviewScoreCount {

  private ModReviewSummary modReviewSummary;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "mod_id", referencedColumnName = "mod_id")
  public ModReviewSummary getModReviewSummary() {
    return modReviewSummary;
  }
}
