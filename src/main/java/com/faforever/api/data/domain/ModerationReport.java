package com.faforever.api.data.domain;

import com.faforever.api.data.checks.IsEntityOwner;
import com.faforever.api.data.checks.IsInAwaitingState;
import com.faforever.api.data.checks.Prefab;
import com.faforever.api.data.type.PostgreSQLEnumType;
import com.faforever.api.security.FafUserDetails;
import com.faforever.api.security.elide.permission.AdminModerationReportCheck;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.yahoo.elide.annotation.Audit;
import com.yahoo.elide.annotation.Audit.Action;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.OnCreatePreSecurity;
import com.yahoo.elide.annotation.OnUpdatePreSecurity;
import com.yahoo.elide.annotation.ReadPermission;
import com.yahoo.elide.annotation.UpdatePermission;
import com.yahoo.elide.core.RequestScope;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.Type;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Set;

@Entity
@Table(name = "moderation_report")
@Setter
@ToString(exclude = {"reportedUsers", "bans"})
@Include(rootLevel = true, type = org.supcomhub.api.dto.ModerationReport.TYPE_NAME)
@ReadPermission(expression = IsEntityOwner.EXPRESSION + " OR " + AdminModerationReportCheck.EXPRESSION)
@DeletePermission(expression = Prefab.NONE)
@CreatePermission(expression = Prefab.ALL)
@Audit(action = Action.CREATE, logStatement = "Moderation report ''{0}'' has been reported", logExpressions = "${moderationReport}")
@Audit(action = Action.UPDATE, logStatement = "Moderation report ''{0}'' has been updated", logExpressions = "${moderationReport}")
public class ModerationReport extends AbstractIntegerIdEntity implements OwnableEntity {

  private ModerationReportStatus reportStatus;
  private Account reporter;
  private String reportDescription;
  private String gameIncidentTimecode;
  private Game game;
  private String publicNote;
  private String privateNote;
  private Account lastModerator;
  private Set<Account> reportedUsers;
  private Collection<Ban> bans;

  @UpdatePermission(expression = AdminModerationReportCheck.EXPRESSION)
  @CreatePermission(expression = Prefab.NONE)
  @Enumerated(EnumType.STRING)
  @Column(name = "report_status")
  @Type(type = PostgreSQLEnumType.TYPE_NAME)
  public @NotNull ModerationReportStatus getReportStatus() {
    return reportStatus;
  }

  @CreatePermission(expression = Prefab.ALL_AND_UPDATE_ON_CREATE)
  @JoinColumn(name = "reporter_id", referencedColumnName = "id")
  @ManyToOne(fetch = FetchType.LAZY)
  public @NotNull Account getReporter() {
    return reporter;
  }

  @UpdatePermission(expression = IsEntityOwner.EXPRESSION + " and " + IsInAwaitingState.EXPRESSION)
  @Column(name = "report_description")
  public @NotNull String getReportDescription() {
    return reportDescription;
  }

  @Column(name = "game_incident_timecode")
  @UpdatePermission(expression = IsEntityOwner.EXPRESSION + " and " + IsInAwaitingState.EXPRESSION)
  public String getGameIncidentTimecode() {
    return gameIncidentTimecode;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "game_id", referencedColumnName = "id")
  @UpdatePermission(expression = IsEntityOwner.EXPRESSION + " and " + IsInAwaitingState.EXPRESSION)
  public Game getGame() {
    return game;
  }

  @Column(name = "public_note")
  @CreatePermission(expression = Prefab.NONE)
  @UpdatePermission(expression = AdminModerationReportCheck.EXPRESSION)
  public String getPublicNote() {
    return publicNote;
  }

  @Column(name = "private_note")
  @ReadPermission(expression = AdminModerationReportCheck.EXPRESSION)
  @CreatePermission(expression = Prefab.NONE)
  @UpdatePermission(expression = AdminModerationReportCheck.EXPRESSION)
  public String getPrivateNote() {
    return privateNote;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "last_moderator_id", referencedColumnName = "id")
  @CreatePermission(expression = Prefab.NONE)
  @UpdatePermission(expression = AdminModerationReportCheck.EXPRESSION)
  public Account getLastModerator() {
    return lastModerator;
  }

  @UpdatePermission(expression = IsEntityOwner.EXPRESSION + " and " + IsInAwaitingState.EXPRESSION + " or " + Prefab.UPDATE_ON_CREATE)
  @JoinTable(name = "reported_user",
    joinColumns = @JoinColumn(name = "report_id"),
    inverseJoinColumns = @JoinColumn(name = "account_id")
  )
  @ManyToMany(cascade = {
    CascadeType.PERSIST,
    CascadeType.MERGE
  })
  public @Size(min = 1) @NotNull @Valid Set<Account> getReportedUsers() {
    return reportedUsers;
  }

  @OneToMany(mappedBy = "moderationReport")
  @ReadPermission(expression = AdminModerationReportCheck.EXPRESSION)
  // Permission is managed by Ban class
  @UpdatePermission(expression = Prefab.ALL)
  public Collection<Ban> getBans() {
    return bans;
  }

  @Override
  @Transient
  @JsonIgnore
  public Account getEntityOwner() {
    return getReporter();
  }

  @OnCreatePreSecurity
  public void assignReporter(RequestScope scope) {
    this.setReportStatus(ModerationReportStatus.AWAITING);
    final Object caller = scope.getUser().getOpaqueUser();
    if (caller instanceof FafUserDetails) {
      final FafUserDetails fafUser = (FafUserDetails) caller;
      final Account reporter = new Account();
      reporter.setId(fafUser.getId());
      this.reporter = reporter;
    }
  }

  @OnUpdatePreSecurity
  public void updateLastModerator(RequestScope scope) {
    final Object caller = scope.getUser().getOpaqueUser();
    if (caller instanceof FafUserDetails) {
      final FafUserDetails fafUser = (FafUserDetails) caller;
      if (fafUser.hasPermission(GroupPermission.ROLE_ADMIN_MODERATION_REPORT)) {
        final Account lastModerator = new Account();
        lastModerator.setId(fafUser.getId());
        this.lastModerator = lastModerator;
      }
    }
  }
}
