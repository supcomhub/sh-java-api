package com.faforever.api.data.domain;

import com.faforever.api.data.checks.Prefab;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "name_record")
@Include(rootLevel = true, type = "nameRecord")
@DeletePermission(expression = Prefab.NONE)
@UpdatePermission(expression = Prefab.NONE)
@Setter
public class NameRecord extends AbstractIntegerIdEntity {

  private Account account;
  private String newName;
  private String previousName;

  @JoinColumn(name = "account_id")
  @ManyToOne(fetch = FetchType.LAZY)
  public @NotNull Account getAccount() {
    return account;
  }

  @Column(name = "previous_name")
  public @NotNull String getPreviousName() {
    return previousName;
  }

  @Column(name = "new_name")
  public @NotNull String getNewName() {
    return newName;
  }
}
