package com.faforever.api.data.domain;

import com.faforever.api.security.FafUserDetails;
import com.faforever.api.security.elide.permission.WriteNewsPostCheck;
import com.yahoo.elide.annotation.Audit;
import com.yahoo.elide.annotation.Audit.Action;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.OnCreatePreSecurity;
import com.yahoo.elide.annotation.UpdatePermission;
import com.yahoo.elide.core.RequestScope;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "news_post")
@Include(rootLevel = true, type = org.supcomhub.api.dto.NewsPost.TYPE_NAME)
@Setter
@CreatePermission(expression = WriteNewsPostCheck.EXPRESSION)
@UpdatePermission(expression = WriteNewsPostCheck.EXPRESSION)
@DeletePermission(expression = WriteNewsPostCheck.EXPRESSION)
@Audit(action = Action.CREATE, logStatement = "News Post ''{0}'' has been created", logExpressions = "${newsPost.id}")
@Audit(action = Action.UPDATE, logStatement = "News Post ''{0}'' has been updated", logExpressions = "${newsPost.id}")
public class NewsPost extends AbstractIntegerIdEntity {
  private Account author;
  private String headline;
  private String body;
  private String tags;
  private Boolean pinned;

  @ManyToOne
  @JoinColumn(name = "author_id", nullable = false)
  public Account getAuthor() {
    return author;
  }

  @Column(name = "headline", nullable = false)
  public String getHeadline() {
    return headline;
  }

  @Column(name = "body", nullable = false)
  public String getBody() {
    return body;
  }

  @Column(name = "tags")
  public String getTags() {
    return tags;
  }

  @Column(name = "pinned", nullable = false)
  public Boolean getPinned() {
    return pinned;
  }

  @OnCreatePreSecurity
  public void assignReporter(RequestScope scope) {
    final Object caller = scope.getUser().getOpaqueUser();
    if (caller instanceof FafUserDetails) {
      final FafUserDetails fafUser = (FafUserDetails) caller;
      final Account author = new Account();
      author.setId(fafUser.getId());
      this.author = author;
    }
  }
}
