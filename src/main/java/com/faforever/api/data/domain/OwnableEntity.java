package com.faforever.api.data.domain;

public interface OwnableEntity {
  Account getEntityOwner();
}
