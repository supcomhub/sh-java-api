package com.faforever.api.data.domain;

import com.yahoo.elide.annotation.Exclude;
import com.yahoo.elide.annotation.Include;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "player_event")
@Include(rootLevel = true, type = org.supcomhub.api.dto.PlayerEvent.TYPE_NAME)
@Setter
@ToString(callSuper = true)
public class PlayerEvent extends AbstractIntegerIdEntity {

  private int playerId;
  private Event event;
  private int count;

  @Exclude
  @Column(name = "player_id")
  public int getPlayerId() {
    return playerId;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "event_id", updatable = false)
  public Event getEvent() {
    return event;
  }

  @Column(name = "count")
  public int getCount() {
    return count;
  }
}
