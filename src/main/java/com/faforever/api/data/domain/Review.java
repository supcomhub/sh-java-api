package com.faforever.api.data.domain;

import com.faforever.api.data.checks.IsEntityOwner;
import com.faforever.api.data.checks.Prefab;
import com.yahoo.elide.annotation.UpdatePermission;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

@Setter
@MappedSuperclass
public class Review extends AbstractIntegerIdEntity implements OwnableEntity {
  private String text;
  private Byte score;
  private Account reviewer;

  @Column(name = "text")
  @UpdatePermission(expression = IsEntityOwner.EXPRESSION)
  public String getText() {
    return text;
  }

  @UpdatePermission(expression = IsEntityOwner.EXPRESSION)
  @Column(name = "score")
  public @DecimalMin("1") @DecimalMax("5") Byte getScore() {
    return score;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "reviewer_id")
  @UpdatePermission(expression = Prefab.ALL_AND_UPDATE_ON_CREATE)
  public Account getReviewer() {
    return reviewer;
  }

  @Override
  @Transient
  public Account getEntityOwner() {
    return getReviewer();
  }
}
