package com.faforever.api.data.domain;

import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;

@Setter
@MappedSuperclass
public class ReviewScoreCount {
  private int count;
  private byte score;
  private String id;

  @Id
  @Column(name = "id")
  public String getId() {
    return id;
  }

  @Column(name = "score")
  public @DecimalMin("1") @DecimalMax("5") Byte getScore() {
    return score;
  }

  @Column(name = "count")
  public int getCount() {
    return count;
  }
}
