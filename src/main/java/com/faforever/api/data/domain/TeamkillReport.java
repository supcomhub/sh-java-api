package com.faforever.api.data.domain;

import com.faforever.api.security.elide.permission.ReadTeamkillReportCheck;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.ReadPermission;
import lombok.Setter;
import org.hibernate.annotations.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Setter
@Table(name = "teamkill_report")
@Include(rootLevel = true, type = org.supcomhub.api.dto.TeamkillReport.TYPE_NAME)
@Immutable
@ReadPermission(expression = ReadTeamkillReportCheck.EXPRESSION)
public class TeamkillReport extends AbstractIntegerIdEntity {

  private Account teamkiller;
  private Account victim;
  private Game game;
  /** How many seconds into the game, in simulation time. */
  private Integer gameTime;

  @ManyToOne
  @JoinColumn(name = "teamkiller_id")
  public Account getTeamkiller() {
    return teamkiller;
  }

  @ManyToOne
  @JoinColumn(name = "victim_id")
  public Account getVictim() {
    return victim;
  }

  @ManyToOne
  @JoinColumn(name = "game_id")
  public Game getGame() {
    return game;
  }

  @Column(name = "game_time")
  public Integer getGameTime() {
    return gameTime;
  }
}
