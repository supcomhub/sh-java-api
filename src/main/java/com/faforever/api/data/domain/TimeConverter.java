package com.faforever.api.data.domain;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.sql.Time;
import java.time.Duration;
import java.util.Optional;

@Converter
public class TimeConverter implements AttributeConverter<Duration, Time> {
  @Override
  public Time convertToDatabaseColumn(Duration duration) {
    return Optional.ofNullable(duration)
      .map(aLong -> new Time(duration.toMillis()))
      .orElse(null);
  }

  @Override
  public Duration convertToEntityAttribute(Time dbData) {
    return Optional.ofNullable(dbData)
      .map(time -> Duration.ofMillis(time.getTime()))
      .orElse(null);
  }
}
