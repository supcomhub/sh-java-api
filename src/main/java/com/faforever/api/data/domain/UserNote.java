package com.faforever.api.data.domain;

import com.faforever.api.data.checks.Prefab;
import com.faforever.api.security.FafUserDetails;
import com.faforever.api.security.elide.permission.AdminAccountNoteCheck;
import com.yahoo.elide.annotation.Audit;
import com.yahoo.elide.annotation.Audit.Action;
import com.yahoo.elide.annotation.CreatePermission;
import com.yahoo.elide.annotation.DeletePermission;
import com.yahoo.elide.annotation.Include;
import com.yahoo.elide.annotation.OnCreatePreSecurity;
import com.yahoo.elide.annotation.ReadPermission;
import com.yahoo.elide.annotation.UpdatePermission;
import com.yahoo.elide.core.RequestScope;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "account_note")
@Include(rootLevel = true, type = org.supcomhub.api.dto.UserNote.TYPE_NAME)
@ReadPermission(expression = AdminAccountNoteCheck.EXPRESSION)
@CreatePermission(expression = AdminAccountNoteCheck.EXPRESSION)
@UpdatePermission(expression = AdminAccountNoteCheck.EXPRESSION)
@DeletePermission(expression = Prefab.NONE)
@Audit(action = Action.CREATE, logStatement = "Note '{0}' for user '{1}' added (watched='{2}') with text: {3}", logExpressions = {"${userNote.id}", "${userNote.account.id}", "${userNote.watched}", "${userNote.note}"})
@Setter
public class UserNote extends AbstractIntegerIdEntity {
  private Account account;
  private Account author;
  private boolean watched;
  private String note;

  @JoinColumn(name = "account_id")
  @ManyToOne(fetch = FetchType.LAZY)
  public @NotNull Account getAccount() {
    return account;
  }

  @JoinColumn(name = "author_id")
  @ManyToOne(fetch = FetchType.LAZY)
  public @NotNull Account getAuthor() {
    return author;
  }

  @Audit(action = Action.UPDATE, logStatement = "Note '{0}' for user '{1}' update with watched: {2}'", logExpressions = {"${userNote.id}", "${userNote.account.id}", "${userNote.watched}"})
  @Column(name = "watched")
  public boolean isWatched() {
    return watched;
  }

  @Column(name = "note")
  @Audit(action = Action.UPDATE, logStatement = "Note '{0}' for user '{1}' updated with text: {2}", logExpressions = {"${userNote.id}", "${userNote.account.id}", "${userNote.note}"})
  public @NotNull String getNote() {
    return note;
  }

  @SuppressWarnings("Duplicates")
  @OnCreatePreSecurity
  public void assignAuthor(RequestScope scope) {
    final Object caller = scope.getUser().getOpaqueUser();
    if (caller instanceof FafUserDetails) {
      final FafUserDetails fafUser = (FafUserDetails) caller;
      final Account author = new Account();
      author.setId(fafUser.getId());
      this.author = author;
    }
  }
}
