/**
 * Contains JPA entity classes, adapted to the needs of JSON-API.
 */

@TypeDef(
  name = PostgreSQLEnumType.TYPE_NAME,
  typeClass = PostgreSQLEnumType.class
)
package com.faforever.api.data.domain;

import com.faforever.api.data.type.PostgreSQLEnumType;
import org.hibernate.annotations.TypeDef;
