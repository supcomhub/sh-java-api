package com.faforever.api.data.listeners;

import com.faforever.api.data.domain.CoopMission;
import org.springframework.stereotype.Component;

import javax.persistence.PostLoad;

@Component
public class CoopMapEnricher {

  @PostLoad
  public void enhance(CoopMission coopMission) {
    coopMission.setDownloadUrl(coopMission.getDownloadUrl());
    coopMission.setThumbnailUrlSmall(coopMission.getThumbnailUrlSmall());
    coopMission.setThumbnailUrlLarge(coopMission.getThumbnailUrlLarge());
  }
}
