package com.faforever.api.email;

import com.faforever.api.data.domain.EmailAddress;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface EmailAddressRepository extends JpaRepository<EmailAddress, Integer> {

  List<EmailAddress> findAllByDistilledIgnoreCase(String emailAddress);

  boolean existsByDistilledIgnoreCase(String email);
}
