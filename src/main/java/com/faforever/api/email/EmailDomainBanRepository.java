package com.faforever.api.email;

import com.faforever.api.data.domain.EmailDomainBan;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmailDomainBanRepository extends JpaRepository<EmailDomainBan, Integer> {
  boolean existsByDomain(String domain);
}
