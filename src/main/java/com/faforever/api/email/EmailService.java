package com.faforever.api.email;

import com.faforever.api.config.FafApiProperties;
import com.faforever.api.config.FafApiProperties.PasswordReset;
import com.faforever.api.config.FafApiProperties.Registration;
import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.EmailAddress;
import com.faforever.api.error.ApiException;
import com.faforever.api.error.Error;
import com.faforever.api.error.ErrorCode;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.MessageFormat;
import java.util.Optional;
import java.util.regex.Pattern;

@Service
@Slf4j
public class EmailService {
  private static final Pattern EMAIL_PATTERN = Pattern.compile(".+@.+\\..+$");
  private final EmailDomainBanRepository emailDomainBanRepository;
  private final FafApiProperties properties;
  private final EmailSender emailSender;
  private final EmailAddressRepository emailAddressRepository;

  public EmailService(EmailDomainBanRepository emailDomainBanRepository, FafApiProperties properties, EmailSender emailSender, EmailAddressRepository emailAddressRepository) {
    this.emailDomainBanRepository = emailDomainBanRepository;
    this.properties = properties;
    this.emailSender = emailSender;
    this.emailAddressRepository = emailAddressRepository;
  }

  /**
   * Checks whether the specified email address as a valid format and its domain is not blacklisted.
   */
  @Transactional(readOnly = true)
  public void validateEmailAddress(String email) {
    if (!EMAIL_PATTERN.matcher(email).matches()) {
      throw new ApiException(new Error(ErrorCode.EMAIL_INVALID, email));
    }
    if (emailDomainBanRepository.existsByDomain(email.substring(email.lastIndexOf('@') + 1))) {
      throw new ApiException(new Error(ErrorCode.EMAIL_BLACKLISTED, email));
    }
  }

  @SneakyThrows
  public void sendActivationMail(String username, String email, String activationUrl) {
    Registration registration = properties.getRegistration();
    emailSender.sendMail(
      properties.getMail().getFromEmailAddress(),
      properties.getMail().getFromEmailName(),
      email,
      registration.getSubject(),
      MessageFormat.format(registration.getHtmlFormat(), username, activationUrl)
    );
  }

  @SneakyThrows
  public void sendPasswordResetMail(String username, String email, String passwordResetUrl) {
    PasswordReset passwordReset = properties.getPasswordReset();
    emailSender.sendMail(
      properties.getMail().getFromEmailAddress(),
      properties.getMail().getFromEmailName(),
      email,
      passwordReset.getSubject(),
      MessageFormat.format(passwordReset.getHtmlFormat(), username, passwordResetUrl)
    );
  }

  /**
   * Some providers allow multiple notations for the same email address. For instance, Gmail delivers messages to {@code
   * foo.bar+baz@gmail.com} and {@code f.o.o.b.a.r@gmail.com} to the same email box.
   * <p>
   * This method distills such email addresses in a provider-specific way. For the example above, the result is {@code
   * foobar@gmail.com}.
   */
  public String distillEmailAddress(String email) {
    if (email.endsWith("@gmail.com") || email.endsWith("@googlemail.com")) {
      int atIndex = email.indexOf('@');
      return email
        .substring(0, atIndex)
        .replace(".", "")
        .replaceAll("\\+[^@]+", "")
        + email.substring(atIndex);
    }
    return email;
  }

  public Optional<Account> findOwnerOfAddress(String emailAddress) {
    return emailAddressRepository.findAllByDistilledIgnoreCase(distillEmailAddress(emailAddress)).stream()
      .map(EmailAddress::getOwner)
      .findFirst();
  }

  public boolean emailExists(String email) {
    return emailAddressRepository.existsByDistilledIgnoreCase(distillEmailAddress(email));
  }
}
