package com.faforever.api.event;

import com.faforever.api.data.domain.Event;
import com.faforever.api.data.domain.PlayerEvent;
import com.faforever.api.error.ApiException;
import com.faforever.api.error.Error;
import com.faforever.api.error.ErrorCode;
import com.faforever.api.user.AccountService;
import com.google.common.base.MoreObjects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.inject.Inject;
import java.util.Objects;
import java.util.UUID;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

@Service
@Slf4j
public class EventService {

  private final EventRepository eventRepository;
  private final AccountService accountService;
  private final PlayerEventRepository playerEventRepository;

  @Inject
  public EventService(EventRepository eventRepository, AccountService accountService, PlayerEventRepository playerEventRepository) {
    this.eventRepository = eventRepository;
    this.accountService = accountService;
    this.playerEventRepository = playerEventRepository;
  }

  UpdatedEventResponse increment(int playerId, UUID eventId, int steps) {
    BiFunction<Integer, Integer, Integer> stepsFunction = (currentSteps, newSteps) -> currentSteps + newSteps;
    accountService.getById(playerId);
    Event event = eventRepository.findById(eventId)
      .orElseThrow(() -> new ApiException(new Error(ErrorCode.ENTITY_NOT_FOUND, eventId)));

    PlayerEvent playerEvent = getOrCreatePlayerEvent(playerId, event);

    int currentSteps1 = MoreObjects.firstNonNull(playerEvent.getCount(), 0);
    int newCurrentCount = stepsFunction.apply(currentSteps1, steps);

    playerEvent.setCount(newCurrentCount);

    log.info("Current list of events: {}", playerEventRepository.findAll().stream().map(Objects::toString).collect(Collectors.joining(",")));
    log.info("New event: {}", playerEvent);

    playerEventRepository.save(playerEvent);

    return new UpdatedEventResponse(playerEvent.getId(), eventId, newCurrentCount);
  }

  private PlayerEvent getOrCreatePlayerEvent(int playerId, Event event) {
    return playerEventRepository.findOneByEventIdAndPlayerId(event.getId(), playerId)
      .orElseGet(() -> new PlayerEvent()
        .setPlayerId(playerId)
        .setEvent(event));
  }
}
