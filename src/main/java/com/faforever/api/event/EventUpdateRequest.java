package com.faforever.api.event;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
class EventUpdateRequest {

  private int playerId;
  private UUID eventId;
  @Min(0)
  private int count;

}
