package com.faforever.api.event;

import com.faforever.api.data.domain.PlayerEvent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface PlayerEventRepository extends JpaRepository<PlayerEvent, UUID> {

  Optional<PlayerEvent> findOneByEventIdAndPlayerId(UUID eventId, int playerId);
}
