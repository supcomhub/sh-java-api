package com.faforever.api.event;

import lombok.Data;

import java.util.UUID;

@Data
class UpdatedEventResponse {

  private final int id;
  private final UUID eventId;
  private final Integer currentCount;

}
