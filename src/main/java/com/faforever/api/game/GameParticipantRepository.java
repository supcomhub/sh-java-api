package com.faforever.api.game;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.GameParticipant;
import com.faforever.api.data.domain.Validity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameParticipantRepository extends JpaRepository<GameParticipant, Integer> {
  int countByParticipantAndGameValidity(Account participant, Validity validity);
}
