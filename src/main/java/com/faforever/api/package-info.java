/**
 * Root package of the SH-API. Must not contain any classes but the main class.
 */
package com.faforever.api;
