package com.faforever.api.security;

import com.yahoo.elide.audit.AuditLogger;
import com.yahoo.elide.audit.LogMessage;
import com.yahoo.elide.core.RequestScope;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class ExtendedAuditLogger extends AuditLogger {
  private final AuditService auditService;

  public ExtendedAuditLogger(AuditService auditService) {
    this.auditService = auditService;
  }

  @Override
  public void commit(RequestScope requestScope) {
    try {
      for (LogMessage message : messages.get()) {
        auditService.logMessage(message.getMessage());
      }
    } finally {
      messages.get().clear();
    }
  }
}
