package com.faforever.api.security;

import com.faforever.api.data.domain.Account;
import com.google.common.base.Strings;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.stream.Collectors;

@Getter
public class FafUserDetails extends org.springframework.security.core.userdetails.User {

  private final Integer id;

  public FafUserDetails(Account account, Collection<? extends GrantedAuthority> authorities) {
    this(
      account.getId(),
      account.getDisplayName(),
      account.getPassword(),
      !account.isGlobalBanned(),
      authorities
    );
  }

  public FafUserDetails(Integer id, String username, String password, boolean accountNonLocked, Collection<? extends GrantedAuthority> authorities) {
    super(username, Strings.nullToEmpty(password), true, true, true, accountNonLocked, authorities);
    this.id = id;
  }

  public boolean hasPermission(String permission) {
    Collection<String> authorities = this.getAuthorities().stream()
      .map(GrantedAuthority::getAuthority)
      .collect(Collectors.toList());

    // We have no admin-only permissions yet
    return authorities.contains(permission) || authorities.contains(UserRole.ADMINISTRATOR.getAuthority());
  }
}
