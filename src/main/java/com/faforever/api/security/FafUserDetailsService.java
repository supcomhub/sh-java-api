package com.faforever.api.security;

import com.faforever.api.data.domain.Account;
import com.faforever.api.user.AccountService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Adapter between Spring's {@link UserDetailsService} and FAF's {@code login} table.
 */
@Service
@Slf4j
public class FafUserDetailsService implements UserDetailsService {

  private final AccountService accountService;

  public FafUserDetailsService(AccountService accountService) {
    this.accountService = accountService;
  }

  @Override
  @Transactional
  public FafUserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Account account = accountService.findAccountByEmail(email)
      .orElseThrow(() -> new UsernameNotFoundException("User could not be found: " + email));

    ArrayList<GrantedAuthority> authorities = new ArrayList<>();

    authorities.addAll(getUserRoles(account));
    authorities.addAll(getPermissionRoles(account));

    return new FafUserDetails(account, authorities);
  }

  private Collection<GrantedAuthority> getUserRoles(Account account) {
    Set<GrantedAuthority> userRoles = new HashSet<>();

    userRoles.add(UserRole.USER);

    account.getUserGroups().stream()
      .map(userGroup -> UserRole.fromString(userGroup.getTechnicalName()))
      .filter(Optional::isPresent)
      .map(Optional::get)
      .forEach(userRoles::add);

    return userRoles;
  }

  private Collection<GrantedAuthority> getPermissionRoles(Account account) {
    return account.getUserGroups().stream()
      .flatMap(userGroup -> userGroup.getPermissions().stream())
      .collect(Collectors.toSet());
  }
}
