package com.faforever.api.user;

import com.faforever.api.data.domain.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

  boolean existsByDisplayNameIgnoreCase(String displayName);
}
