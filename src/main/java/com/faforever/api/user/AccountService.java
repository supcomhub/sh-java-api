package com.faforever.api.user;

import com.faforever.api.config.FafApiProperties;
import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.AccountVerification;
import com.faforever.api.data.domain.AccountVerification.Type;
import com.faforever.api.data.domain.EmailAddress;
import com.faforever.api.data.domain.NameRecord;
import com.faforever.api.email.EmailService;
import com.faforever.api.error.ApiException;
import com.faforever.api.error.Error;
import com.faforever.api.error.ErrorCode;
import com.faforever.api.mautic.MauticService;
import com.faforever.api.security.FafTokenService;
import com.faforever.api.security.FafTokenType;
import com.faforever.api.security.FafUserDetails;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import lombok.SneakyThrows;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Request.Builder;
import okhttp3.Response;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.security.core.Authentication;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.regex.Pattern;

import static com.faforever.api.error.ErrorCode.ACCOUNT_MIGRATION_FAILED;
import static com.faforever.api.error.ErrorCode.STEAM_ID_ALREADY_LINKED;
import static com.faforever.api.error.ErrorCode.TOKEN_INVALID;

@Service
@Slf4j
public class AccountService {
  static final String KEY_USERNAME = "username";
  static final String KEY_EMAIL = "email";
  static final String KEY_PASSWORD = "password";
  static final String KEY_USER_ID = "id";
  static final String KEY_STEAM_LINK_CALLBACK_URL = "callbackUrl";
  private static final Pattern USERNAME_PATTERN = Pattern.compile("[A-Za-z][A-Za-z0-9_-]{2,15}$");
  private final EmailService emailService;
  private final AccountRepository accountRepository;
  private final NameRecordRepository nameRecordRepository;
  private final FafApiProperties properties;
  private final PasswordEncoder passwordEncoder;
  private final AnopeUserRepository anopeUserRepository;
  private final FafTokenService fafTokenService;
  private final SteamService steamService;
  private final Optional<MauticService> mauticService;
  private final AccountVerificationService accountVerificationService;
  private final OkHttpClient okHttpClient;

  public AccountService(EmailService emailService,
                        AccountRepository accountRepository,
                        NameRecordRepository nameRecordRepository,
                        FafApiProperties properties,
                        PasswordEncoder passwordEncoder,
                        AnopeUserRepository anopeUserRepository,
                        FafTokenService fafTokenService,
                        SteamService steamService,
                        Optional<MauticService> mauticService,
                        AccountVerificationService accountVerificationService, OkHttpClient okHttpClient) {
    this.emailService = emailService;
    this.accountRepository = accountRepository;
    this.nameRecordRepository = nameRecordRepository;
    this.properties = properties;
    this.passwordEncoder = passwordEncoder;
    this.anopeUserRepository = anopeUserRepository;
    this.fafTokenService = fafTokenService;
    this.steamService = steamService;
    this.mauticService = mauticService;
    this.accountVerificationService = accountVerificationService;
    this.okHttpClient = okHttpClient;
  }

  public Account getById(Integer accountId) {
    return accountRepository.findById(accountId)
      .orElseThrow(() -> new ApiException(new Error(ErrorCode.ENTITY_NOT_FOUND, accountId)));
  }

  void register(String username, String email, String password) {
    log.debug("Registration requested for user: {}", username);

    validateDisplayName(username, true);
    validateEmail(email);

    String token = fafTokenService.createToken(FafTokenType.REGISTRATION,
      Duration.ofSeconds(properties.getRegistration().getLinkExpirationSeconds()),
      ImmutableMap.of(
        KEY_USERNAME, username,
        KEY_EMAIL, email,
        KEY_PASSWORD, passwordEncoder.encode(password)
      ));

    String activationUrl = String.format(properties.getRegistration().getActivationUrlFormat(), token);

    emailService.sendActivationMail(username, email, activationUrl);
  }

  private void validateDisplayName(String displayName, boolean checkReservation) {
    if (!USERNAME_PATTERN.matcher(displayName).matches()) {
      throw new ApiException(new Error(ErrorCode.USERNAME_INVALID, displayName));
    }
    if (accountRepository.existsByDisplayNameIgnoreCase(displayName)) {
      throw new ApiException(new Error(ErrorCode.USERNAME_TAKEN, displayName));
    }

    if (checkReservation) {
      if (properties.getFafApi().getReserveNamesUntil().isAfter(LocalDateTime.now())) {
        checkFafUsernameReservation(displayName);
      }

      int usernameReservationTimeInMonths = properties.getUser().getUsernameReservationTimeInMonths();
      nameRecordRepository.getLastUsernameOwnerWithinMonths(displayName, usernameReservationTimeInMonths)
        .ifPresent(reservedByUserId -> {
          throw new ApiException(new Error(ErrorCode.USERNAME_RESERVED, displayName, usernameReservationTimeInMonths));
        });
    }
  }

  /**
   * Creates a new user based on the information in the activation token.
   *
   * @param token the JWT in the format: <pre>
   *   {
   *     "action": "activate",
   *     "expiry": "2011-12-03T10:15:30Z",
   *     "
   *   }
   * </pre>
   */
  @SneakyThrows
  @SuppressWarnings("unchecked")
  @Transactional
  void activate(String token, String ipAddress) {
    Map<String, String> claims = fafTokenService.resolveToken(FafTokenType.REGISTRATION, token);

    String username = claims.get(KEY_USERNAME);
    String email = claims.get(KEY_EMAIL);
    String password = claims.get(KEY_PASSWORD);

    // TODO set recent IP address?
    Account account = new Account()
      .setDisplayName(username)
      .setPassword(password)
      .setLastLoginIpAddress(ipAddress);

    account.setEmailAddresses(new HashSet<>(Collections.singleton(
      new EmailAddress(email, emailService.distillEmailAddress(email), true, account)
    )));

    account = accountRepository.save(account);

    log.debug("Account has been activated: {}", account);

    createOrUpdateMauticContact(account, ipAddress);
  }

  void changePassword(String currentPassword, String newPassword, Account account) {
    verifyPassword(currentPassword, account, ErrorCode.PASSWORD_CHANGE_FAILED_WRONG_PASSWORD);
    setPassword(account, newPassword);
  }

  @Transactional
  public void changeLogin(String newLogin, Account account, String ipAddress) {
    internalChangeDisplayName(newLogin, account, ipAddress, false);
  }

  @Transactional
  public void changeLoginForced(String newLogin, Account account, String ipAddress) {
    internalChangeDisplayName(newLogin, account, ipAddress, true);
  }

  private void internalChangeDisplayName(String newLogin, Account account, String ipAddress, boolean force) {
    validateDisplayName(newLogin, false);

    if (!force) {
      int minDaysBetweenChange = properties.getUser().getMinimumDaysBetweenUsernameChange();
      nameRecordRepository.getDaysSinceLastNewRecord(account.getId(), minDaysBetweenChange)
        .ifPresent(daysSinceLastRecord -> {
          throw new ApiException(new Error(ErrorCode.USERNAME_CHANGE_TOO_EARLY, minDaysBetweenChange - daysSinceLastRecord.intValue()));
        });

      int usernameReservationTimeInMonths = properties.getUser().getUsernameReservationTimeInMonths();
      nameRecordRepository.getLastUsernameOwnerWithinMonths(newLogin, usernameReservationTimeInMonths)
        .ifPresent(reservedByUserId -> {
          if (!Objects.equals(reservedByUserId, account.getId())) {
            throw new ApiException(new Error(ErrorCode.USERNAME_RESERVED, newLogin, usernameReservationTimeInMonths));
          }
        });

    }
    log.debug("Changing username for user ''{}'' to ''{}'', forced:''{}''", account, newLogin, force);
    NameRecord nameRecord = new NameRecord()
      .setPreviousName(account.getDisplayName())
      .setNewName(newLogin)
      .setAccount(accountRepository.getOne(account.getId()));
    nameRecordRepository.save(nameRecord);

    account.setDisplayName(newLogin);

    createOrUpdateMauticContact(accountRepository.save(account), ipAddress);
  }

  private void createOrUpdateMauticContact(Account account, String ipAddress) {
    mauticService.ifPresent(service -> service.createOrUpdateContact(
      account.getPrimaryEmailAddress(),
      String.valueOf(account.getId()),
      account.getDisplayName(),
      ipAddress,
      OffsetDateTime.now()
    )
      .thenAccept(result -> log.debug("Updated contact in Mautic: {}", account.getPrimaryEmailAddress()))
      .exceptionally(throwable -> {
        log.warn("Could not update contact in Mautic: {}", account, throwable);
        return null;
      }));
  }

  public void changeEmail(String currentPassword, String newEmail, Account account, String ipAddress) {
    verifyPassword(currentPassword, account, ErrorCode.EMAIL_CHANGE_FAILED_WRONG_PASSWORD);

    emailService.validateEmailAddress(newEmail);

    log.debug("Changing email for user ''{}'' to ''{}''", account, newEmail);

    Set<EmailAddress> emailAddresses = account.getEmailAddresses();
    emailAddresses.stream()
      .filter(emailAddress -> Objects.equals(emailAddress.getPlain(), account.getPrimaryEmailAddress()))
      .findFirst()
      .ifPresent(emailAddresses::remove);

    emailAddresses.add(new EmailAddress(newEmail, emailService.distillEmailAddress(newEmail), true, account));

    createOrUpdateUser(account, ipAddress);
  }

  private void verifyPassword(String currentPassword, Account account, ErrorCode emailChangeFailedWrongPassword) {
    if (!passwordEncoder.matches(currentPassword, account.getPassword())) {
      throw new ApiException(new Error(emailChangeFailedWrongPassword));
    }
  }

  private void createOrUpdateUser(Account account, String ipAddress) {
    createOrUpdateMauticContact(accountRepository.save(account), ipAddress);
  }

  void resetPassword(String emailAddress, String newPassword) {
    log.debug("Password reset requested for account with email: {}", emailAddress);

    Account account = emailService.findOwnerOfAddress(emailAddress)
      .orElseThrow(() -> new ApiException(new Error(ErrorCode.UNKNOWN_EMAIL_ADDRESS, emailAddress)));

    String token = fafTokenService.createToken(FafTokenType.PASSWORD_RESET,
      Duration.ofSeconds(properties.getRegistration().getLinkExpirationSeconds()),
      ImmutableMap.of(KEY_USER_ID, String.valueOf(account.getId()),
        KEY_PASSWORD, newPassword));

    String passwordResetUrl = String.format(properties.getPasswordReset().getPasswordResetUrlFormat(), token);

    emailService.sendPasswordResetMail(account.getDisplayName(), account.getPrimaryEmailAddress(), passwordResetUrl);
  }

  @SneakyThrows
  void claimPasswordResetToken(String token) {
    log.debug("Trying to reset password with token: {}", token);
    Map<String, String> claims = fafTokenService.resolveToken(FafTokenType.PASSWORD_RESET, token);

    Integer userId = Integer.valueOf(claims.get(KEY_USER_ID));
    String newPassword = claims.get(KEY_PASSWORD);
    Account account = accountRepository.findById(userId)
      .orElseThrow(() -> new ApiException(new Error(TOKEN_INVALID)));

    setPassword(account, newPassword);
  }

  private void setPassword(Account account, String password) {
    log.debug("Updating FAF password for user: {}", account);

    account.setPassword(passwordEncoder.encode(password));

    accountRepository.save(account);
    log.debug("Updating anope password for user: {}", account);
    anopeUserRepository.updatePassword(account.getDisplayName(), Hashing.md5().hashString(password, StandardCharsets.UTF_8).toString());
  }

  public Account getAccount(Authentication authentication) {
    if (authentication != null
      && authentication.getPrincipal() != null
      && authentication.getPrincipal() instanceof FafUserDetails) {
      return getAccount(((FafUserDetails) authentication.getPrincipal()).getId());
    }
    throw new ApiException(new Error(TOKEN_INVALID));
  }

  public Account getAccount(Integer userId) {
    return accountRepository.findById(userId).orElseThrow(() -> new ApiException(new Error(TOKEN_INVALID)));
  }

  public String buildSteamLinkUrl(Account account, String callbackUrl) {
    log.debug("Building Steam link url for user id: {}", account.getId());

    if (steamService.isSteamVerified(account)) {
      log.debug("Account with id '{}' has already verified via  Steam", account.getId());
      throw new ApiException(new Error(ErrorCode.STEAM_ID_UNCHANGEABLE));
    }

    String token = fafTokenService.createToken(FafTokenType.LINK_TO_STEAM,
      Duration.ofHours(1),
      ImmutableMap.of(
        KEY_USER_ID, String.valueOf(account.getId()),
        KEY_STEAM_LINK_CALLBACK_URL, callbackUrl
      )
    );

    return steamService.buildLoginUrl(String.format(properties.getLinkToSteam().getSteamRedirectUrlFormat(), token));
  }

  @SneakyThrows
  @Transactional
  public SteamLinkResult linkToSteam(String token, String steamId) {
    log.debug("linkToSteam requested for steamId '{}' with token: {}", steamId, token);
    List<Error> errors = new ArrayList<>();
    Map<String, String> attributes = fafTokenService.resolveToken(FafTokenType.LINK_TO_STEAM, token);

    Account account = accountRepository.findById(Integer.valueOf(attributes.get(KEY_USER_ID)))
      .orElseThrow(() -> new ApiException(new Error(TOKEN_INVALID)));

    if (!steamService.ownsForgedAlliance(steamId)) {
      errors.add(new Error(ErrorCode.STEAM_LINK_NO_FA_GAME));
    }

    accountVerificationService.findBySteamId(steamId).ifPresent(userWithSameId ->
      errors.add(new Error(ErrorCode.STEAM_ID_ALREADY_LINKED))
    );

    if (errors.isEmpty()) {
      accountVerificationService.setAccountVerifiedBySteam(account, steamId);
    }

    String callbackUrl = attributes.get(KEY_STEAM_LINK_CALLBACK_URL);
    return new SteamLinkResult(callbackUrl, errors);
  }

  public Optional<Account> findAccountByEmail(String email) {
    return emailService.findOwnerOfAddress(email);
  }

  @SneakyThrows
  public void migrateFromFaf(String username, String password, String ipAddress) {
    Request tokenRequest = new Request.Builder()
      .addHeader("Content-Type", "application/x-www-form-urlencoded")
      .url(properties.getFafApi().getApiTokenUrl())
      .post(
        new MultipartBody.Builder()
          .setType(MultipartBody.FORM)
          .addFormDataPart("grant_type", "password")
          .addFormDataPart("client_id", properties.getFafApi().getOauthClientId())
          .addFormDataPart("client_secret", properties.getFafApi().getOauthClientSecret())
          .addFormDataPart("username", username)
          .addFormDataPart("password", password)
          .build()
      ).build();

    // We are using OkHttpClient here, because Spring RestTemplate always fails on evaluating the current security
    // context, while we especially want to use it for anonymous users.
    Response response = okHttpClient.newCall(tokenRequest).execute();

    if (!response.isSuccessful()) {
      log.error("User {} could not be logged in to FAF", username);
      throw new ApiException(new Error(ACCOUNT_MIGRATION_FAILED,
        "We were unable to login to FAF with your given credentials. The migration failed, no account was created."));
    }

    String accessToken = new JSONObject(response.body().string()).getString("access_token");

    Request meRequest = new Request.Builder()
      .addHeader("Authorization", "Bearer " + accessToken)
      .url(properties.getFafApi().getApiMeUrl())
      .get()
      .build();

    response = okHttpClient.newCall(meRequest).execute();

    if (!response.isSuccessful()) {
      log.error("Fetching meResult from FAF API failed");
      throw new ApiException(new Error(ACCOUNT_MIGRATION_FAILED,
        "We were unable fetch your account data from FAF. Please contact an administrator."));
    }

    JSONObject root = new JSONObject(response.body().string());
    JSONObject data = root.getJSONObject("data");
    JSONObject attributes = data.getJSONObject("attributes");

    String playerId = data.getString("id");
    String steamId = attributes.get("steamId") == JSONObject.NULL ? null : attributes.getString("steamId");
    String displayName = attributes.getString("login");
    String email = attributes.getString("email");

    validateDisplayName(displayName, false);
    validateEmail(email);

    accountVerificationService.findByFafId(playerId).ifPresent(accountVerification -> {
      throw new ApiException(new Error(ACCOUNT_MIGRATION_FAILED,
        "This FAF account has already been migrated."));
    });

    if (steamId != null) {
      accountVerificationService.findBySteamId(steamId).ifPresent(accountVerification -> {
        throw new ApiException(new Error(STEAM_ID_ALREADY_LINKED));
      });
    }

    Account account = new Account()
      .setDisplayName(displayName)
      .setPassword(passwordEncoder.encode(password))
      .setLastLoginIpAddress(ipAddress);

    account.setEmailAddresses(new HashSet<>(Collections.singleton(
      new EmailAddress(email, emailService.distillEmailAddress(email), true, account)
    )));

    Set<AccountVerification> accountVerifications = new HashSet<>();
    account.setAccountVerifications(accountVerifications);
    accountVerifications.add(new AccountVerification(account, Type.FAF, playerId));
    if (steamId != null) {
      accountVerifications.add(new AccountVerification(account, Type.STEAM, steamId));
    }

    account = accountRepository.save(account);

    log.debug("Account has been activated: {}", account);
    createOrUpdateMauticContact(account, ipAddress);
  }

  private void checkFafUsernameReservation(String name) {
    Request meRequest = new Builder()
      .url(String.format(properties.getFafApi().getApiPlayerUrl(), name))
      .get()
      .build();

    // We are using OkHttpClient here, because Spring RestTemplate always fails on evaluating the current security
    // context, while we especially want to use it for anonymous users.
    try {
      Response response = okHttpClient.newCall(meRequest).execute();

      if (!response.isSuccessful()) {
        throw new IOException("We were unable fetch your account data from FAF.");
      }

      JSONObject root = new JSONObject(response.body().string());
      JSONArray data = root.getJSONArray("data");

      if (!data.isEmpty()) {
        throw new ApiException(new Error(ErrorCode.USERNAME_RESERVED_FAF, name));
      }

    } catch (ApiException e) {
      throw e;
    } catch (Exception e) {
      log.error("Username {} was not checked against FAF", e);
    }
  }

  private void validateEmail(String email) throws ApiException {
    emailService.validateEmailAddress(email);

    if (emailService.emailExists(email)) {
      throw new ApiException(new Error(ErrorCode.EMAIL_REGISTERED, email));
    }
  }

  @Value
  static class SteamLinkResult {
    String callbackUrl;
    List<Error> errors;
  }
}
