package com.faforever.api.user;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.AccountVerification;
import com.faforever.api.data.domain.AccountVerification.Type;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AccountVerificationRepository extends JpaRepository<AccountVerification, Integer> {

  Optional<AccountVerification> findOneByTypeEqualsAndValueEqualsIgnoreCase(Type type, String value);

  List<AccountVerification> findAllByAccount(Account account);
}
