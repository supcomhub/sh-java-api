package com.faforever.api.user;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.AccountVerification;
import com.faforever.api.data.domain.AccountVerification.Type;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class AccountVerificationService {
  private final AccountVerificationRepository accountVerificationRepository;

  public AccountVerificationService(AccountVerificationRepository accountVerificationRepository) {
    this.accountVerificationRepository = accountVerificationRepository;
  }

  public Optional<AccountVerification> findByFafId(String fafId) {
    return accountVerificationRepository.findOneByTypeEqualsAndValueEqualsIgnoreCase(Type.FAF, fafId);
  }

  public Optional<AccountVerification> findBySteamId(String steamId) {
    return accountVerificationRepository.findOneByTypeEqualsAndValueEqualsIgnoreCase(Type.STEAM, steamId);
  }

  public List<AccountVerification> findByAccount(Account account) {
    return accountVerificationRepository.findAllByAccount(account);
  }

  public void setAccountVerifiedBySteam(Account account, String steamId) {
    AccountVerification verification = accountVerificationRepository.save(new AccountVerification(account, Type.STEAM, steamId));
    account.getAccountVerifications().add(verification);
  }
}
