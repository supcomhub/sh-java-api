package com.faforever.api.user;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.UserGroup;
import com.faforever.api.security.FafUserDetails;
import com.faforever.api.web.JsonApiSingleResource;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import lombok.Value;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * Provides the route {@code /me} which returns the currently logged in user's information.
 */
@RestController
public class MeController {
  private final AccountService accountService;

  public MeController(AccountService accountService) {
    this.accountService = accountService;
  }

  @RequestMapping(method = RequestMethod.GET, value = "/me")
  @ApiOperation("Returns the authentication object of the current user")
  @ApiResponse(code = 200, message = "Success with JsonApi compliant MeResult")
  @Secured("ROLE_USER")
  public JsonApiSingleResource<MeResult> me(@AuthenticationPrincipal FafUserDetails authentication) {

    Account account = accountService.getAccount(authentication.getId());
    Set<String> grantedAuthorities = authentication.getAuthorities().stream()
      .map(GrantedAuthority::getAuthority)
      .collect(Collectors.toSet());

    Set<String> groups = account.getUserGroups().stream()
      .map(UserGroup::getTechnicalName)
      .collect(Collectors.toSet());

    return new MeResult(account.getId(), account.getDisplayName(), account.getPrimaryEmailAddress(),
      groups, grantedAuthorities).asJsonApi();
  }

  @Value
  public static class MeResult {
    Integer userId;
    String displayName;
    String primaryEmailAddress;
    Set<String> groups;
    Set<String> permissions;

    JsonApiSingleResource<MeResult> asJsonApi() {
      return JsonApiSingleResource.ofProxy(
        () -> "me",
        () -> "me",
        () -> this
      );
    }
  }
}
