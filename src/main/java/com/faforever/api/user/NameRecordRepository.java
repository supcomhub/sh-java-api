package com.faforever.api.user;

import com.faforever.api.data.domain.NameRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigInteger;
import java.util.Optional;

public interface NameRecordRepository extends JpaRepository<NameRecord, Integer> {
  @Query(value = "SELECT DATE_PART('day', now()) - DATE_PART('day', create_time) FROM name_record WHERE account_id = :userId AND DATE_PART('day', now()) - DATE_PART('day', create_time) <= :maximumDaysAgo ORDER BY create_time DESC LIMIT 1", nativeQuery = true)
  Optional<BigInteger> getDaysSinceLastNewRecord(@Param("userId") Integer userId, @Param("maximumDaysAgo") Integer maximumDaysAgo);

  // https://stackoverflow.com/a/43767356/513451
  @Query(value = "SELECT account_id FROM name_record WHERE previous_name = :name AND CURRENT_DATE > CURRENT_DATE - (INTERVAL '1' MONTH) * :months ORDER BY create_time DESC LIMIT 1", nativeQuery = true)
  Optional<Integer> getLastUsernameOwnerWithinMonths(@Param("name") String name, @Param("months") Integer months);
}
