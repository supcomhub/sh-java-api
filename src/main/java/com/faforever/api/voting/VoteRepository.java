package com.faforever.api.voting;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.Vote;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface VoteRepository extends JpaRepository<Vote, Integer> {
  Optional<Vote> findByVoterAndVotingSubjectId(Account voter, Integer votingSubjectId);
}
