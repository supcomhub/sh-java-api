package com.faforever.api.voting;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.Validity;
import com.faforever.api.data.domain.Vote;
import com.faforever.api.data.domain.VotingAnswer;
import com.faforever.api.data.domain.VotingChoice;
import com.faforever.api.data.domain.VotingSubject;
import com.faforever.api.error.ApiException;
import com.faforever.api.error.Error;
import com.faforever.api.error.ErrorCode;
import com.faforever.api.game.GameParticipantRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.transaction.Transactional;
import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class VotingService {
  private final VoteRepository voteRepository;
  private final VotingSubjectRepository votingSubjectRepository;
  private final GameParticipantRepository gameParticipantRepository;
  private final VotingChoiceRepository votingChoiceRepository;

  public VotingService(VoteRepository voteRepository, VotingSubjectRepository votingSubjectRepository, GameParticipantRepository gameParticipantRepository, VotingChoiceRepository votingChoiceRepository) {
    this.voteRepository = voteRepository;
    this.votingSubjectRepository = votingSubjectRepository;
    this.gameParticipantRepository = gameParticipantRepository;
    this.votingChoiceRepository = votingChoiceRepository;
  }

  @Transactional
  public void saveVote(Vote vote, Account voter) {
    vote.setVoter(voter);
    Assert.notNull(vote.getVotingSubject(), "You must specify a subject");
    List<Error> errors = ableToVote(voter, vote.getVotingSubject().getId());

    if (vote.getVotingAnswers() == null) {
      vote.setVotingAnswers(Collections.emptySet());
    }

    VotingSubject subject = votingSubjectRepository.findById(vote.getVotingSubject().getId())
      .orElseThrow(() -> new IllegalArgumentException("Subject of vote not found"));

    vote.getVotingAnswers().forEach(votingAnswer -> {
      VotingChoice votingChoice = votingAnswer.getVotingChoice();
      VotingChoice one = votingChoiceRepository.findById(votingChoice.getId())
        .orElseThrow(() -> new ApiException(new Error(ErrorCode.VOTING_CHOICE_DOES_NOT_EXIST, votingChoice.getId())));
      votingAnswer.setVotingChoice(one);
      votingAnswer.setVote(vote);
    });

    subject.getVotingQuestions().forEach(votingQuestion -> {
      List<VotingAnswer> votingAnswers = vote.getVotingAnswers().stream()
        .filter(votingAnswer -> votingAnswer.getVotingChoice().getVotingQuestion().equals(votingQuestion))
        .collect(Collectors.toList());
      long countOfAnswers = votingAnswers.size();
      int maxAnswers = votingQuestion.getMaxAnswers();
      if (maxAnswers < countOfAnswers) {
        errors.add(new Error(ErrorCode.TOO_MANY_ANSWERS, countOfAnswers, maxAnswers));
      }

      if (votingQuestion.isAlternativeQuestion()) {
        for (int i = 0; i < countOfAnswers; i++) {
          int finalI = i;
          long answersWithOrdinal = votingAnswers.stream()
            .filter(votingAnswer -> Objects.equals(votingAnswer.getAlternativeOrdinal(), finalI))
            .count();
          if (answersWithOrdinal == 1) {
            continue;
          }
          errors.add(new Error(ErrorCode.MALFORMATTED_ALTERNATIVE_ORDINALS));
        }
      }
    });
    if (!errors.isEmpty()) {
      throw new ApiException(errors.toArray(new Error[0]));
    }
    voteRepository.save(vote);
  }

  private List<Error> ableToVote(Account voter, Integer votingSubjectId) {
    VotingSubject subject = votingSubjectRepository.findById(votingSubjectId)
      .orElseThrow(() -> new ApiException(new Error(ErrorCode.VOTING_SUBJECT_DOES_NOT_EXIST, votingSubjectId)));

    List<Error> errors = new ArrayList<>();
    Optional<Vote> byPlayerAndVotingSubject = voteRepository.findByVoterAndVotingSubjectId(voter, votingSubjectId);
    if (byPlayerAndVotingSubject.isPresent()) {
      errors.add(new Error(ErrorCode.VOTED_TWICE));
    }

    int gamesPlayed = gameParticipantRepository.countByParticipantAndGameValidity(voter, Validity.VALID);

    if (subject.getBeginOfVoteTime().isAfter(OffsetDateTime.now())) {
      errors.add(new Error(ErrorCode.VOTE_DID_NOT_START_YET, subject.getBeginOfVoteTime()));
    }

    if (subject.getEndOfVoteTime().isBefore(OffsetDateTime.now())) {
      errors.add(new Error(ErrorCode.VOTE_ALREADY_ENDED, subject.getEndOfVoteTime()));
    }

    if (gamesPlayed < subject.getMinGamesToVote()) {
      errors.add(new Error(ErrorCode.NOT_ENOUGH_GAMES, gamesPlayed, subject.getMinGamesToVote()));
    }
    return errors;
  }

  List<VotingSubject> votingSubjectsAbleToVote(Account voter) {
    List<VotingSubject> all = votingSubjectRepository.findAll();
    return all.stream()
      .filter(votingSubject -> ableToVote(voter, votingSubject.getId()).isEmpty())
      .collect(Collectors.toList());
  }
}
