package com.faforever.api.clan;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.Clan;
import lombok.Builder;

public class ClanFactory {
  private static final String DEFAULT_CLAN_NAME = "JUnitClan_ClanFactory";
  private static final String DEFAULT_CLAN_TAG = "123";

  @Builder
  private static Clan create(Integer id, String name, String tag,
                             Account founder, Account leader, String description) {
    return (Clan) new Clan()
      .setName(name != null ? name : DEFAULT_CLAN_NAME)
      .setTag(tag != null ? tag : DEFAULT_CLAN_TAG)
      .setFounder(founder)
      .setLeader(leader)
      .setDescription(description)
      .setId(id);
  }
}
