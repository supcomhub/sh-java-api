package com.faforever.api.clan;


import com.faforever.api.clan.result.InvitationResult;
import com.faforever.api.config.FafApiProperties;
import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.Clan;
import com.faforever.api.data.domain.ClanMembership;
import com.faforever.api.error.ApiException;
import com.faforever.api.error.ErrorCode;
import com.faforever.api.error.ProgrammingError;
import com.faforever.api.security.JwtService;
import com.faforever.api.user.AccountRepository;
import com.faforever.api.user.AccountService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.jwt.Jwt;

import java.io.IOException;
import java.util.Collections;
import java.util.Optional;

import static com.faforever.api.error.ApiExceptionWithCode.apiExceptionWithCode;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClanServiceTest {

  @Rule
  public ExpectedException expectedException = ExpectedException.none();

  private ClanService instance;
  @Mock
  private ClanRepository clanRepository;
  @Mock
  private AccountRepository accountRepository;
  @Mock
  private FafApiProperties fafApiProperties;
  @Mock
  private JwtService jwtService;
  @Mock
  private AccountService accountService;
  @Mock
  private ClanMembershipRepository clanMembershipRepository;

  @Before
  public void setUp() {
    instance = new ClanService(clanRepository, accountRepository, fafApiProperties, jwtService, accountService, clanMembershipRepository, new ObjectMapper());
  }

  @Test
  public void createClanWhereLeaderIsAlreadyInAClan() {
    String clanName = "My cool Clan";
    String tag = "123";
    String description = "A cool clan for testing";
    Account creator = new Account();
    creator.setId(1);
    creator.getClanMemberships().add(new ClanMembership());
    try {
      instance.create(clanName, tag, description, creator);
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_CREATE_CREATOR_IS_IN_A_CLAN));
    }
    verify(clanRepository, Mockito.never()).save(any(Clan.class));
  }

  @Test
  public void createClanWithSameName() {
    String clanName = "My cool Clan";
    String tag = "123";
    String description = "A cool clan for testing";

    Account creator = new Account();
    creator.setId(1);

    when(clanRepository.findOneByName(clanName)).thenReturn(Optional.of(new Clan()));
    try {
      instance.create(clanName, tag, description, creator);
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_NAME_EXISTS));
    }

    ArgumentCaptor<Clan> clanCaptor = ArgumentCaptor.forClass(Clan.class);
    verify(clanRepository, Mockito.times(0)).save(clanCaptor.capture());
  }

  @Test
  public void createClanWithSameTag() {
    String clanName = "My cool Clan";
    String tag = "123";
    String description = "A cool clan for testing";

    Account creator = new Account();
    creator.setId(1);

    when(clanRepository.findOneByName(clanName)).thenReturn(Optional.empty());
    when(clanRepository.findOneByTag(tag)).thenReturn(Optional.of(new Clan()));

    try {
      instance.create(clanName, tag, description, creator);
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_TAG_EXISTS));
    }

    ArgumentCaptor<Clan> clanCaptor = ArgumentCaptor.forClass(Clan.class);
    verify(clanRepository, Mockito.times(0)).save(clanCaptor.capture());
  }

  @Test
  public void createClanSuccessful() {
    String clanName = "My cool Clan";
    String tag = "123";
    String description = "A cool clan for testing";

    Account creator = new Account();
    creator.setId(1);

    when(clanRepository.findOneByName(clanName)).thenReturn(Optional.empty());
    when(clanRepository.findOneByTag(tag)).thenReturn(Optional.empty());


    instance.create(clanName, tag, description, creator);
    ArgumentCaptor<Clan> clanCaptor = ArgumentCaptor.forClass(Clan.class);
    verify(clanRepository, Mockito.times(1)).save(clanCaptor.capture());
    assertEquals(clanName, clanCaptor.getValue().getName());
    assertEquals(tag, clanCaptor.getValue().getTag());
    assertEquals(description, clanCaptor.getValue().getDescription());
    assertEquals(creator, clanCaptor.getValue().getLeader());
    assertEquals(creator, clanCaptor.getValue().getFounder());
    assertEquals(1, clanCaptor.getValue().getMemberships().size());
    assertEquals(creator, clanCaptor.getValue().getMemberships().get(0).getMember());
  }

  @Test
  public void generatePlayerInvitationTokenWithInvalidClan() throws IOException {
    Account requester = new Account();
    requester.setId(1);

    try {
      instance.generatePlayerInvitationToken(requester, 45, 42);
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_NOT_EXISTS));
    }
    verify(jwtService, Mockito.never()).sign(any());
  }

  @Test
  public void generatePlayerInvitationTokenFromNonLeader() throws IOException {
    Account requester = new Account();
    requester.setId(1);

    Account newMember = new Account();
    newMember.setId(2);

    Account leader = new Account();
    leader.setId(3);

    Clan clan = ClanFactory.builder().leader(leader).build();

    when(clanRepository.findById(clan.getId())).thenReturn(Optional.of(clan));

    try {
      instance.generatePlayerInvitationToken(requester, newMember.getId(), clan.getId());
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_NOT_LEADER));
    }
    verify(jwtService, Mockito.never()).sign(any());
  }

  @Test
  public void generatePlayerInvitationTokenInvalidPlayer() throws IOException {
    Account requester = new Account();
    requester.setId(1);

    Clan clan = ClanFactory.builder().leader(requester).build();

    when(clanRepository.findById(clan.getId())).thenReturn(Optional.of(clan));

    try {
      instance.generatePlayerInvitationToken(requester, 42, clan.getId());
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_GENERATE_LINK_PLAYER_NOT_FOUND));
    }
    verify(jwtService, Mockito.never()).sign(any());
  }

  @Test
  public void generatePlayerInvitationToken() throws IOException {
    Account requester = new Account();
    requester.setId(1);

    Account newMember = new Account();
    newMember.setId(2);

    Clan clan = ClanFactory.builder().leader(requester).build();

    FafApiProperties props = new FafApiProperties();

    when(clanRepository.findById(clan.getId())).thenReturn(Optional.of(clan));
    when(accountRepository.findById(newMember.getId())).thenReturn(Optional.of(newMember));
    when(fafApiProperties.getClan()).thenReturn(props.getClan());

    instance.generatePlayerInvitationToken(requester, newMember.getId(), clan.getId());
    ArgumentCaptor<InvitationResult> captor = ArgumentCaptor.forClass(InvitationResult.class);
    verify(jwtService, Mockito.times(1)).sign(captor.capture());
    assertThat("expire",
        captor.getValue().getExpire(),
        greaterThan(System.currentTimeMillis()));
    assertEquals(newMember.getId(), captor.getValue().getNewMember().getId());
    assertEquals(newMember.getDisplayName(), captor.getValue().getNewMember().getDisplayName());
    assertEquals(clan.getId(), captor.getValue().getClan().getId());
    assertEquals(clan.getTag(), captor.getValue().getClan().getTag());
    assertEquals(clan.getName(), captor.getValue().getClan().getName());
  }

  @Test
  public void acceptPlayerInvitationTokenExpire() {
    String stringToken = "1234";
    long expire = System.currentTimeMillis();
    Jwt jwtToken = Mockito.mock(Jwt.class);

    when(jwtToken.getClaims()).thenReturn(
        String.format("{\"expire\":%s}", expire));
    when(jwtService.decodeAndVerify(any())).thenReturn(jwtToken);

    try {
      instance.acceptPlayerInvitationToken(stringToken, null);
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_ACCEPT_TOKEN_EXPIRE));
    }
    verify(clanMembershipRepository, Mockito.never()).save(any(ClanMembership.class));
  }

  @Test
  public void acceptPlayerInvitationTokenInvalidClan() {
    String stringToken = "1234";

    long expire = System.currentTimeMillis() + 1000 * 3;
    Jwt jwtToken = Mockito.mock(Jwt.class);

    when(jwtToken.getClaims()).thenReturn(
        String.format("{\"expire\":%s,\"clan\":{\"id\":42}}", expire));
    when(jwtService.decodeAndVerify(any())).thenReturn(jwtToken);

    try {
      instance.acceptPlayerInvitationToken(stringToken, null);
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_NOT_EXISTS));
    }
    verify(clanMembershipRepository, Mockito.never()).save(any(ClanMembership.class));
  }


  @Test
  public void acceptPlayerInvitationTokenInvalidPlayer() {
    String stringToken = "1234";
    Clan clan = ClanFactory.builder().build();

    long expire = System.currentTimeMillis() + 1000 * 3;
    Jwt jwtToken = Mockito.mock(Jwt.class);

    when(jwtToken.getClaims()).thenReturn(
        String.format("{\"expire\":%s,\"newMember\":{\"id\":2},\"clan\":{\"id\":%s}}",
            expire, clan.getId()));
    when(jwtService.decodeAndVerify(any())).thenReturn(jwtToken);
    when(clanRepository.findById(clan.getId())).thenReturn(Optional.of(clan));

    try {
      instance.acceptPlayerInvitationToken(stringToken, null);
      fail();
    } catch (ProgrammingError e) {
      assertEquals("ClanMember does not exist: 2", e.getMessage());
    }
    verify(clanMembershipRepository, Mockito.never()).save(any(ClanMembership.class));
  }

  @Test
  public void acceptPlayerInvitationTokenWrongPlayer() {
    String stringToken = "1234";

    Account newMember = new Account();
    newMember.setId(2);

    Clan clan = ClanFactory.builder().build();

    Account otherAccount = new Account();
    otherAccount.setId(3);

    long expire = System.currentTimeMillis() + 1000 * 3;
    Jwt jwtToken = Mockito.mock(Jwt.class);

    when(jwtToken.getClaims()).thenReturn(
        String.format("{\"expire\":%s,\"newMember\":{\"id\":%s},\"clan\":{\"id\":%s}}",
            expire, newMember.getId(), clan.getId()));
    when(jwtService.decodeAndVerify(any())).thenReturn(jwtToken);
    when(clanRepository.findById(clan.getId())).thenReturn(Optional.of(clan));
    when(accountRepository.findById(newMember.getId())).thenReturn(Optional.of(newMember));
    when(accountService.getAccount(any(Authentication.class))).thenReturn(otherAccount);

    try {
      instance.acceptPlayerInvitationToken(stringToken, new TestingAuthenticationToken("JUnit", "pwd"));
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_ACCEPT_WRONG_PLAYER));
    }
    verify(clanMembershipRepository, Mockito.never()).save(any(ClanMembership.class));
  }

  @Test
  public void acceptPlayerInvitationTokenPlayerIAlreadyInAClan() {
    String stringToken = "1234";

    Clan clan = ClanFactory.builder().build();

    Account newMember = new Account();
    newMember.setId(2);
    newMember.setClanMemberships(
      Collections.singleton(new ClanMembership().setClan(clan).setMember(newMember)));

    long expire = System.currentTimeMillis() + 1000 * 3;
    Jwt jwtToken = Mockito.mock(Jwt.class);

    when(jwtToken.getClaims()).thenReturn(
        String.format("{\"expire\":%s,\"newMember\":{\"id\":%s},\"clan\":{\"id\":%s}}",
            expire, newMember.getId(), clan.getId()));
    when(jwtService.decodeAndVerify(any())).thenReturn(jwtToken);
    when(clanRepository.findById(clan.getId())).thenReturn(Optional.of(clan));
    when(accountRepository.findById(newMember.getId())).thenReturn(Optional.of(newMember));
    when(accountService.getAccount(any(Authentication.class))).thenReturn(newMember);

    try {
      instance.acceptPlayerInvitationToken(stringToken, new TestingAuthenticationToken("JUnit", "pwd"));
      fail();
    } catch (ApiException e) {
      assertThat(e, apiExceptionWithCode(ErrorCode.CLAN_ACCEPT_PLAYER_IN_A_CLAN));
    }
    verify(clanMembershipRepository, Mockito.never()).save(any(ClanMembership.class));
  }

  @Test
  public void acceptPlayerInvitationToken() {
    String stringToken = "1234";
    Clan clan = ClanFactory.builder().build();
    Account newMember = new Account();
    newMember.setId(2);
    long expire = System.currentTimeMillis() + 1000 * 3;
    Jwt jwtToken = Mockito.mock(Jwt.class);

    when(jwtToken.getClaims()).thenReturn(
        String.format("{\"expire\":%s,\"newMember\":{\"id\":%s},\"clan\":{\"id\":%s}}",
            expire, newMember.getId(), clan.getId()));
    when(jwtService.decodeAndVerify(any())).thenReturn(jwtToken);
    when(clanRepository.findById(clan.getId())).thenReturn(Optional.of(clan));
    when(accountRepository.findById(newMember.getId())).thenReturn(Optional.of(newMember));
    when(accountService.getAccount(any(Authentication.class))).thenReturn(newMember);

    instance.acceptPlayerInvitationToken(stringToken, new TestingAuthenticationToken("JUnit", "pwd"));

    ArgumentCaptor<ClanMembership> captor = ArgumentCaptor.forClass(ClanMembership.class);
    verify(clanMembershipRepository, Mockito.times(1)).save(captor.capture());
    assertEquals(newMember.getId(), captor.getValue().getMember().getId());
    assertEquals(clan.getId(), captor.getValue().getClan().getId());
  }
}
