package com.faforever.api.data.listeners;

import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.Vote;
import com.faforever.api.data.domain.VotingAnswer;
import com.faforever.api.data.domain.VotingChoice;
import com.faforever.api.data.domain.VotingQuestion;
import com.faforever.api.data.domain.VotingSubject;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.context.support.MessageSourceAccessor;

import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.HashSet;

import static org.hamcrest.Matchers.hasItem;
import static org.junit.Assert.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class VotingSubjectEnricherTest {
  private VotingSubjectEnricher instance;
  @Mock
  private MessageSourceAccessor messageSourceAccessor;

  @Before
  public void setUp() {
    instance = new VotingSubjectEnricher();
    instance.init(messageSourceAccessor);
  }

  @Test
  public void testQuestionEnhancing() {
    VotingQuestion votingQuestion = new VotingQuestion();
    votingQuestion.setAlternativeQuestion(true);
    votingQuestion.setQuestionKey("abc");
    VotingSubject votingSubject = new VotingSubject();
    votingSubject.setEndOfVoteTime(OffsetDateTime.MIN);
    votingSubject.setRevealWinner(true);
    votingQuestion.setVotingSubject(votingSubject);

    Vote vote1 = new Vote();
    Account voter1 = new Account();
    vote1.setVoter(voter1);

    Vote vote2 = new Vote();
    Account voter2 = new Account();
    vote2.setVoter(voter2);

    Vote vote3 = new Vote();
    Account voter3 = new Account();
    vote1.setVoter(voter3);

    Vote vote4 = new Vote();
    Account voter4 = new Account();
    vote1.setVoter(voter4);

    Vote vote5 = new Vote();
    Account voter5 = new Account();
    vote1.setVoter(voter5);

    VotingChoice votingChoice = new VotingChoice();
    votingChoice.setId(1);
    votingChoice.setVotingQuestion(votingQuestion);

    addAnswerToChoice(votingChoice, votingQuestion, vote1, 0);
    addAnswerToChoice(votingChoice, votingQuestion, vote2, 0);

    VotingChoice votingChoice2 = new VotingChoice();
    votingChoice2.setId(2);
    votingChoice2.setVotingQuestion(votingQuestion);

    addAnswerToChoice(votingChoice2, votingQuestion, vote3, 0);
    addAnswerToChoice(votingChoice2, votingQuestion, vote4, 0);
    addAnswerToChoice(votingChoice2, votingQuestion, vote5, 1);

    VotingChoice votingChoice3 = new VotingChoice();
    votingChoice3.setId(3);
    votingChoice3.setVotingQuestion(votingQuestion);

    addAnswerToChoice(votingChoice2, votingQuestion, vote5, 0);

    instance.calculateWinners(votingQuestion);

    assertThat(votingQuestion.getWinners(), hasItem(votingChoice2));
  }

  @Test
  public void testQuestionEnhancingDraw() {
    VotingQuestion votingQuestion = new VotingQuestion();
    votingQuestion.setId(1);
    votingQuestion.setAlternativeQuestion(true);
    votingQuestion.setQuestionKey("abc");
    VotingSubject votingSubject = new VotingSubject();
    votingSubject.setId(1);
    votingSubject.setEndOfVoteTime(OffsetDateTime.MIN);
    votingSubject.setRevealWinner(true);
    votingQuestion.setVotingSubject(votingSubject);

    Vote vote1 = (Vote) new Vote().setId(1);
    Account voter1 = (Account) new Account().setId(1);
    vote1.setVoter(voter1);

    Vote vote2 = (Vote) new Vote().setId(2);
    Account voter2 = (Account) new Account().setId(2);
    vote2.setVoter(voter2);

    Vote vote3 = (Vote) new Vote().setId(3);
    Account voter3 = (Account) new Account().setId(3);
    vote3.setVoter(voter3);

    Vote vote4 = (Vote) new Vote().setId(4);
    Account voter4 = (Account) new Account().setId(4);
    vote4.setVoter(voter4);

    Vote vote5 = (Vote) new Vote().setId(5);
    Account voter5 = (Account) new Account().setId(5);
    vote5.setVoter(voter5);

    Vote vote6 = (Vote) new Vote().setId(6);
    Account voter6 = (Account) new Account().setId(6);
    vote6.setVoter(voter6);


    VotingChoice votingChoice = new VotingChoice();
    votingChoice.setId(1);
    votingChoice.setVotingQuestion(votingQuestion);

    addAnswerToChoice(votingChoice, votingQuestion, vote1, 0);
    addAnswerToChoice(votingChoice, votingQuestion, vote2, 0);
    addAnswerToChoice(votingChoice, votingQuestion, vote6, 0);


    VotingChoice votingChoice2 = new VotingChoice();
    votingChoice2.setId(2);
    votingChoice2.setVotingQuestion(votingQuestion);

    addAnswerToChoice(votingChoice2, votingQuestion, vote4, 0);
    addAnswerToChoice(votingChoice2, votingQuestion, vote3, 0);
    addAnswerToChoice(votingChoice2, votingQuestion, vote5, 1);

    VotingChoice votingChoice3 = new VotingChoice();
    votingChoice3.setId(3);
    votingChoice3.setVotingQuestion(votingQuestion);

    addAnswerToChoice(votingChoice3, votingQuestion, vote5, 0);

    instance.calculateWinners(votingQuestion);

    assertThat(votingQuestion.getWinners(), Matchers.allOf(hasItem(votingChoice2), hasItem(votingChoice)));
  }

  private void addAnswerToChoice(VotingChoice votingChoice, VotingQuestion votingQuestion, Vote vote, int alternativeOrdinal) {
    VotingAnswer votingAnswer = new VotingAnswer();
    votingAnswer.setAlternativeOrdinal(alternativeOrdinal);
    votingAnswer.setVote(vote);
    votingAnswer.setVotingChoice(votingChoice);

    if (vote.getVotingAnswers() != null) {
      vote.getVotingAnswers().add(votingAnswer);
    } else {
      vote.setVotingAnswers(new HashSet<>(Collections.singleton(votingAnswer)));
    }

    if (votingChoice.getVotingAnswers() != null) {
      votingChoice.getVotingAnswers().add(votingAnswer);
    } else {
      votingChoice.setVotingAnswers(new HashSet<>(Collections.singleton(votingAnswer)));
    }

    if (votingQuestion.getVotingChoices() != null) {
      votingQuestion.getVotingChoices().add(votingChoice);
    } else {
      votingQuestion.setVotingChoices(new HashSet<>(Collections.singleton(votingChoice)));
    }
  }

}
