package com.faforever.api.security;

import com.faforever.api.data.domain.Account;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.faforever.api.security.FafUserAuthenticationConverter.NON_LOCKED_KEY;
import static com.faforever.api.security.FafUserAuthenticationConverter.USER_ID_KEY;
import static junit.framework.TestCase.assertNull;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.security.oauth2.provider.token.UserAuthenticationConverter.AUTHORITIES;
import static org.springframework.security.oauth2.provider.token.UserAuthenticationConverter.USERNAME;

@RunWith(MockitoJUnitRunner.class)
public class FafUserAuthenticationConverterTest {
  private FafUserAuthenticationConverter instance;

  @Before
  public void setup() {
    instance = new FafUserAuthenticationConverter();
  }

  @Test(expected = ClassCastException.class)
  public void testConvertUserAuthenticationWithWrongUserDetails() {
    Authentication authentication = mock(Authentication.class);
    when(authentication.getPrincipal()).thenReturn(new Object());
    instance.convertUserAuthentication(authentication);
  }

  @Test
  public void testConvertUserAuthenticationWithValidUserDetails() {
    Account account = new Account();
    account
      .setDisplayName("AccountName")
      .setPassword("SomePassword")
      .setId(123);

    List<GrantedAuthority> authorities = new ArrayList<>();

    Authentication authentication = mock(Authentication.class);
    when(authentication.getPrincipal()).thenReturn(new FafUserDetails(account, authorities));
    when(authentication.getName()).thenReturn("authenticationName");
    Map<String, ?> result = instance.convertUserAuthentication(authentication);

    assertThat(result.get(USERNAME), is("authenticationName"));
    assertThat(result.get(USER_ID_KEY), is(123));
    assertThat(result.get(NON_LOCKED_KEY), is(true));
  }

  @Test
  public void testExtractAuthenticationEmptyMap() {
    assertNull(instance.extractAuthentication(new HashMap<>()));
  }

  @Test
  public void testExtractAuthenticationFullUser() {
    Map<String, Object> authenticationMap = new HashMap<>();
    authenticationMap.put(USER_ID_KEY, 123);
    authenticationMap.put(USERNAME, "authenticationName");
    authenticationMap.put(NON_LOCKED_KEY, true);
    authenticationMap.put(AUTHORITIES, "ROLE_UPDATE_AVATAR,ROLE_UPDATE_ACCOUNT_NOTE");

    Authentication result = instance.extractAuthentication(authenticationMap);

    assertThat(result instanceof UsernamePasswordAuthenticationToken, is(true));
    assertThat(result.getAuthorities(), hasSize(2));
    assertThat(result.isAuthenticated(), is(true));
  }

}
