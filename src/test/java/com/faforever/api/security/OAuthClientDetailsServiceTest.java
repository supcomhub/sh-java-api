package com.faforever.api.security;

import com.faforever.api.client.OAuthClientRepository;
import com.faforever.api.config.FafApiProperties;
import com.faforever.api.data.domain.OAuthClient;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientRegistrationException;

import java.util.Optional;
import java.util.UUID;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class OAuthClientDetailsServiceTest {

  private OAuthClientDetailsService instance;

  @Mock
  private OAuthClientRepository oAuthClientRepository;

  @Before
  public void setUp() {
    instance = new OAuthClientDetailsService(oAuthClientRepository, new FafApiProperties());
  }

  @Test
  public void loadClientByClientId() {
    UUID clientId = UUID.fromString("00000000-0000-0000-0000-000000000123");
    when(oAuthClientRepository.findByClientId(clientId))
      .thenReturn(Optional.of(new OAuthClient().setDefaultScope("").setRedirectUris("").setClientId(clientId)));

    ClientDetails result = instance.loadClientByClientId(clientId.toString());

    assertThat(result, notNullValue());
  }

  @Test(expected = ClientRegistrationException.class)
  public void loadClientByClientIdThrowsClientRegistrationExceptionIfNotExists() {
    when(oAuthClientRepository.findByClientId(any()))
      .thenReturn(Optional.empty());

    instance.loadClientByClientId("00000000-0000-0000-0000-000000000123");
  }
}
