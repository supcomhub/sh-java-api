package com.faforever.api.user;

import com.faforever.api.config.FafApiProperties;
import com.faforever.api.data.domain.Account;
import com.faforever.api.data.domain.AccountVerification;
import com.faforever.api.data.domain.AccountVerification.Type;
import com.faforever.api.data.domain.EmailAddress;
import com.faforever.api.data.domain.NameRecord;
import com.faforever.api.email.EmailService;
import com.faforever.api.error.ApiExceptionWithCode;
import com.faforever.api.error.ErrorCode;
import com.faforever.api.mautic.MauticService;
import com.faforever.api.security.FafTokenService;
import com.faforever.api.security.FafTokenType;
import com.faforever.api.user.AccountService.SteamLinkResult;
import com.google.common.collect.ImmutableMap;
import com.google.common.hash.Hashing;
import okhttp3.Call;
import okhttp3.OkHttpClient;
import okhttp3.Response;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Answers;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.security.crypto.password.NoOpPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.OffsetDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

import static com.faforever.api.user.AccountService.KEY_PASSWORD;
import static com.faforever.api.user.AccountService.KEY_STEAM_LINK_CALLBACK_URL;
import static com.faforever.api.user.AccountService.KEY_USER_ID;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.emptyArray;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AccountServiceTest {
  private static final String INVALID_PASSWORD = "invalid password";
  private static final String TEST_SECRET = "apple";
  private static final Integer TEST_USERID = 5;
  private static final String TEST_USERNAME = "Junit";
  private static final String TEST_USERNAME_CHANGED = "newLogin";
  private static final String TEST_CURRENT_EMAIL = "junit@example.com";
  private static final String TEST_NEW_EMAIL = "junit@example.com";
  private static final String TEST_CURRENT_PASSWORD = "oldPassword";
  private static final String TEST_NEW_PASSWORD = "newPassword";
  private static final String TOKEN_VALUE = "someToken";
  private static final String PASSWORD_RESET_URL_FORMAT = "http://www.example.com/resetPassword/%s";
  private static final String ACTIVATION_URL_FORMAT = "http://www.example.com/%s";
  private static final String STEAM_ID = "someSteamId";
  private static final String IP_ADDRESS = "127.0.0.1";
  private static final PasswordEncoder passwordEncoder = NoOpPasswordEncoder.getInstance();

  @Rule
  public ExpectedException expectedException = ExpectedException.none();
  private AccountService instance;
  @Mock
  private EmailService emailService;
  @Mock
  private AccountRepository accountRepository;
  @Mock
  private NameRecordRepository nameRecordRepository;
  @Mock
  private AnopeUserRepository anopeUserRepository;
  @Mock
  private SteamService steamService;
  @Mock
  private FafTokenService fafTokenService;
  @Mock
  private MauticService mauticService;
  @Mock
  private AccountVerificationService accountVerificationService;
  @Mock
  private OkHttpClient okHttpClient;

  private FafApiProperties properties;

  private static Account createUser(Integer id, String displayName, String password, String email) {
    return (Account) new Account()
      .setDisplayName(displayName)
      .setPassword(passwordEncoder.encode(password))
      .setEmailAddresses(new HashSet<>(Collections.singleton(new EmailAddress(email, email, true, null))))
      .setId(id);
  }

  @Before
  public void setUp() {
    properties = new FafApiProperties();
    properties.getJwt().setSecret(TEST_SECRET);
    properties.getLinkToSteam().setSteamRedirectUrlFormat("%s");
    properties.getFafApi()
      .setApiTokenUrl("http://someTokenUrl")
      .setApiMeUrl("http://someMeUrl")
      .setApiPlayerUrl("http://somePlayerUrl/%s")
      .setOauthClientId("someOAuthClientId")
      .setOauthClientSecret("someOAuthClientSecret")
      .setReserveNamesUntil(LocalDateTime.now().minusDays(1L));
    instance = new AccountService(
      emailService,
      accountRepository,
      nameRecordRepository,
      properties,
      passwordEncoder, anopeUserRepository,
      fafTokenService,
      steamService,
      Optional.of(mauticService),
      accountVerificationService,
      okHttpClient);

    when(fafTokenService.createToken(any(), any(), any())).thenReturn(TOKEN_VALUE);
    when(accountRepository.save(any(Account.class))).then(invocation -> ((Account) invocation.getArgument(0)).setId(TEST_USERID));

    when(mauticService.createOrUpdateContact(any(), any(), any(), any(), any()))
      .thenReturn(CompletableFuture.completedFuture(null));
  }

  @Test
  @SuppressWarnings("unchecked")
  public void register() {
    properties.getRegistration().setActivationUrlFormat(ACTIVATION_URL_FORMAT);

    instance.register(TEST_USERNAME, TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);

    verify(emailService).validateEmailAddress(TEST_CURRENT_EMAIL);
    verify(emailService).emailExists(TEST_CURRENT_EMAIL);

    ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);
    verify(emailService).sendActivationMail(eq(TEST_USERNAME), eq(TEST_CURRENT_EMAIL), urlCaptor.capture());
    assertThat(urlCaptor.getValue(), is(String.format(ACTIVATION_URL_FORMAT, TOKEN_VALUE)));

    verifyZeroInteractions(mauticService, okHttpClient);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void registerWithFAFCheck() throws Exception {
    properties.getRegistration().setActivationUrlFormat(ACTIVATION_URL_FORMAT);
    properties.getFafApi()
      .setReserveNamesUntil(LocalDateTime.now().plusDays(1L))
      .setApiPlayerUrl("https://someUrl/?%s");

    Response response = mock(Response.class, Answers.RETURNS_DEEP_STUBS);
    Call call = mock(Call.class);
    when(okHttpClient.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.isSuccessful()).thenReturn(true);
    when(response.body().string()).thenReturn("{\"data\":[]}");

    instance.register(TEST_USERNAME, TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);

    verify(emailService).validateEmailAddress(TEST_CURRENT_EMAIL);
    verify(emailService).emailExists(TEST_CURRENT_EMAIL);

    ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);
    verify(emailService).sendActivationMail(eq(TEST_USERNAME), eq(TEST_CURRENT_EMAIL), urlCaptor.capture());
    assertThat(urlCaptor.getValue(), is(String.format(ACTIVATION_URL_FORMAT, TOKEN_VALUE)));

    verify(okHttpClient).newCall(any());
    verifyZeroInteractions(mauticService);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void registerWithFAFCheck_NameReserved() throws Exception {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_RESERVED_FAF, TEST_USERNAME));

    properties.getRegistration().setActivationUrlFormat(ACTIVATION_URL_FORMAT);
    properties.getFafApi()
      .setReserveNamesUntil(LocalDateTime.now().plusDays(1L))
      .setApiPlayerUrl("https://someUrl/?%s");

    Response response = mock(Response.class, Answers.RETURNS_DEEP_STUBS);
    Call call = mock(Call.class);
    when(okHttpClient.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.isSuccessful()).thenReturn(true);
    when(response.body().string()).thenReturn("{\"data\":[{}]}");

    instance.register(TEST_USERNAME, TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);
  }

  @Test
  public void registerEmailAlreadyRegistered() {
    when(emailService.emailExists(TEST_CURRENT_EMAIL)).thenReturn(true);
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.EMAIL_REGISTERED, TEST_CURRENT_EMAIL));

    instance.register("junit", TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);
  }

  @Test
  public void registerInvalidUsernameWithComma() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_INVALID, "junit,"));
    instance.register("junit,", TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);
  }

  @Test
  public void registerInvalidUsernameStartsUnderscore() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_INVALID, "_junit"));
    instance.register("_junit", TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);
  }

  @Test
  public void registerInvalidUsernameTooShort() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_INVALID, "ju"));
    instance.register("ju", TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);
  }

  @Test
  public void registerUsernameTaken() {
    when(accountRepository.existsByDisplayNameIgnoreCase("junit")).thenReturn(true);
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_TAKEN, "junit"));
    instance.register("junit", TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);
  }

  @Test
  public void registerUsernameReserved() {
    when(nameRecordRepository.getLastUsernameOwnerWithinMonths(any(), anyInt())).thenReturn(Optional.of(1));
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_RESERVED, "junit", 6));
    instance.register("junit", TEST_CURRENT_EMAIL, TEST_CURRENT_PASSWORD);
  }

  @Test
  public void activate() {
    final String TEST_IP_ADDRESS = IP_ADDRESS;
    when(fafTokenService.resolveToken(FafTokenType.REGISTRATION, TOKEN_VALUE)).thenReturn(ImmutableMap.of(
      AccountService.KEY_USERNAME, TEST_USERNAME,
      AccountService.KEY_EMAIL, TEST_CURRENT_EMAIL,
      AccountService.KEY_PASSWORD, passwordEncoder.encode(TEST_NEW_PASSWORD)
    ));

    instance.activate(TOKEN_VALUE, TEST_IP_ADDRESS);

    ArgumentCaptor<Account> captor = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository).save(captor.capture());

    Account account = captor.getValue();
    assertThat(account.getDisplayName(), is(TEST_USERNAME));
    assertThat(account.getPrimaryEmailAddress(), is(TEST_CURRENT_EMAIL));
    assertThat(account.getPassword(), is(passwordEncoder.encode(TEST_NEW_PASSWORD)));
    assertThat(account.getLastLoginIpAddress(), is(TEST_IP_ADDRESS));

    verify(mauticService).createOrUpdateContact(eq(TEST_NEW_EMAIL), eq(String.valueOf(TEST_USERID)), eq(TEST_USERNAME), eq(IP_ADDRESS), any(OffsetDateTime.class));
  }

  @Test
  public void changePassword() {
    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);

    instance.changePassword(TEST_CURRENT_PASSWORD, TEST_NEW_PASSWORD, account);
    ArgumentCaptor<Account> captor = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository).save(captor.capture());
    assertEquals(captor.getValue().getPassword(), passwordEncoder.encode(TEST_NEW_PASSWORD));
    verify(anopeUserRepository).updatePassword(TEST_USERNAME, Hashing.md5().hashString(TEST_NEW_PASSWORD, StandardCharsets.UTF_8).toString());

    verifyZeroInteractions(mauticService);
  }

  @Test
  public void changePasswordInvalidPassword() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.PASSWORD_CHANGE_FAILED_WRONG_PASSWORD));

    Account account = createUser(TEST_USERID, TEST_USERNAME, INVALID_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changePassword(TEST_CURRENT_PASSWORD, TEST_NEW_PASSWORD, account);
  }

  @Test
  public void changeEmail() {
    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);

    instance.changeEmail(TEST_CURRENT_PASSWORD, TEST_NEW_EMAIL, account, IP_ADDRESS);
    verify(emailService).validateEmailAddress(TEST_NEW_EMAIL);
    ArgumentCaptor<Account> captor = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository).save(captor.capture());
    assertEquals(captor.getValue().getPrimaryEmailAddress(), TEST_NEW_EMAIL);

    verify(mauticService).createOrUpdateContact(eq(TEST_NEW_EMAIL), eq(String.valueOf(TEST_USERID)), eq(TEST_USERNAME), eq(IP_ADDRESS), any(OffsetDateTime.class));
  }

  @Test
  public void changeEmailInvalidPassword() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.EMAIL_CHANGE_FAILED_WRONG_PASSWORD));

    Account account = createUser(TEST_USERID, TEST_USERNAME, INVALID_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changeEmail(TEST_CURRENT_PASSWORD, TEST_NEW_PASSWORD, account, IP_ADDRESS);
  }

  @Test
  public void changeLogin() {
    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(nameRecordRepository.getDaysSinceLastNewRecord(any(), anyInt())).thenReturn(Optional.empty());

    instance.changeLogin(TEST_USERNAME_CHANGED, account, IP_ADDRESS);
    ArgumentCaptor<Account> captorUser = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository).save(captorUser.capture());
    assertEquals(captorUser.getValue().getDisplayName(), TEST_USERNAME_CHANGED);
    ArgumentCaptor<NameRecord> captorNameRecord = ArgumentCaptor.forClass(NameRecord.class);
    verify(nameRecordRepository).save(captorNameRecord.capture());
    assertEquals(captorNameRecord.getValue().getPreviousName(), TEST_USERNAME);

    verify(mauticService).createOrUpdateContact(eq(TEST_NEW_EMAIL), eq(String.valueOf(TEST_USERID)), eq(TEST_USERNAME_CHANGED), eq(IP_ADDRESS), any(OffsetDateTime.class));
  }

  @Test
  public void changeLoginWithUsernameInUse() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_TAKEN, TEST_USERNAME_CHANGED));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(accountRepository.existsByDisplayNameIgnoreCase(TEST_USERNAME_CHANGED)).thenReturn(true);
    instance.changeLogin(TEST_USERNAME_CHANGED, account, IP_ADDRESS);
  }

  @Test
  public void changeLoginWithUsernameInUseButForced() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_TAKEN, TEST_USERNAME_CHANGED));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(accountRepository.existsByDisplayNameIgnoreCase(TEST_USERNAME_CHANGED)).thenReturn(true);
    instance.changeLoginForced(TEST_USERNAME_CHANGED, account, IP_ADDRESS);
  }

  @Test
  public void changeLoginWithInvalidUsername() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_INVALID, "$%&"));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changeLogin("$%&", account, IP_ADDRESS);
  }

  @Test
  public void changeLoginWithInvalidUsernameButForced() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_INVALID, "$%&"));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changeLoginForced("$%&", account, IP_ADDRESS);
  }

  @Test
  public void changeLoginTooEarly() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_CHANGE_TOO_EARLY, 25));
    when(nameRecordRepository.getDaysSinceLastNewRecord(any(), anyInt())).thenReturn(Optional.of(BigInteger.valueOf(5)));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changeLogin(TEST_USERNAME_CHANGED, account, IP_ADDRESS);
  }

  @Test
  public void changeLoginTooEarlyButForce() {
    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changeLoginForced(TEST_USERNAME_CHANGED, account, IP_ADDRESS);
    verify(mauticService).createOrUpdateContact(eq(TEST_NEW_EMAIL), eq(String.valueOf(TEST_USERID)), eq(TEST_USERNAME_CHANGED), eq(IP_ADDRESS), any(OffsetDateTime.class));
  }

  @Test
  public void changeLoginUsernameReserved() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.USERNAME_RESERVED, TEST_USERNAME_CHANGED, 6));
    when(nameRecordRepository.getLastUsernameOwnerWithinMonths(any(), anyInt())).thenReturn(Optional.of(6));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changeLogin(TEST_USERNAME_CHANGED, account, IP_ADDRESS);
  }

  @Test
  public void changeLoginUsernameReservedButForced() {
    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changeLoginForced(TEST_USERNAME_CHANGED, account, IP_ADDRESS);
    verify(mauticService).createOrUpdateContact(eq(TEST_NEW_EMAIL), eq(String.valueOf(TEST_USERID)), eq(TEST_USERNAME_CHANGED), eq(IP_ADDRESS), any(OffsetDateTime.class));
  }

  @Test
  public void changeLoginUsernameReservedBySelf() {
    when(nameRecordRepository.getLastUsernameOwnerWithinMonths(any(), anyInt())).thenReturn(Optional.of(TEST_USERID));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    instance.changeLogin(TEST_USERNAME_CHANGED, account, IP_ADDRESS);

    verify(mauticService).createOrUpdateContact(eq(TEST_NEW_EMAIL), eq(String.valueOf(TEST_USERID)), eq(TEST_USERNAME_CHANGED), eq(IP_ADDRESS), any(OffsetDateTime.class));
  }

  @Test
  @SuppressWarnings("unchecked")
  public void resetPasswordByLogin() {
    properties.getPasswordReset().setPasswordResetUrlFormat(PASSWORD_RESET_URL_FORMAT);

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(emailService.findOwnerOfAddress(TEST_CURRENT_EMAIL)).thenReturn(Optional.of(account));

    instance.resetPassword(TEST_CURRENT_EMAIL, TEST_NEW_PASSWORD);

    verify(emailService).findOwnerOfAddress(TEST_CURRENT_EMAIL);

    ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);
    verify(emailService).sendPasswordResetMail(eq(TEST_USERNAME), eq(TEST_CURRENT_EMAIL), urlCaptor.capture());
    assertThat(urlCaptor.getValue(), is(String.format(PASSWORD_RESET_URL_FORMAT, TOKEN_VALUE)));

    ArgumentCaptor<Map<String, String>> attributesMapCaptor = ArgumentCaptor.forClass(Map.class);
    verify(fafTokenService).createToken(eq(FafTokenType.PASSWORD_RESET), any(), attributesMapCaptor.capture());
    Map<String, String> tokenAttributes = attributesMapCaptor.getValue();
    assertThat(tokenAttributes.size(), is(2));
    assertThat(tokenAttributes.get(KEY_USER_ID), is(String.valueOf(TEST_USERID)));
    assertThat(tokenAttributes.get(KEY_PASSWORD), is(String.valueOf(TEST_NEW_PASSWORD)));
  }

  @Test
  @SuppressWarnings("unchecked")
  public void resetPasswordByEmail() {
    properties.getPasswordReset().setPasswordResetUrlFormat(PASSWORD_RESET_URL_FORMAT);

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);

    when(emailService.findOwnerOfAddress(TEST_CURRENT_EMAIL)).thenReturn(Optional.of(account));
    instance.resetPassword(TEST_CURRENT_EMAIL, TEST_NEW_PASSWORD);

    verify(emailService).findOwnerOfAddress(TEST_CURRENT_EMAIL);

    ArgumentCaptor<String> urlCaptor = ArgumentCaptor.forClass(String.class);
    verify(emailService).sendPasswordResetMail(eq(TEST_USERNAME), eq(TEST_CURRENT_EMAIL), urlCaptor.capture());
    assertThat(urlCaptor.getValue(), is(String.format(PASSWORD_RESET_URL_FORMAT, TOKEN_VALUE)));

    ArgumentCaptor<Map<String, String>> attributesMapCaptor = ArgumentCaptor.forClass(Map.class);
    verify(fafTokenService).createToken(eq(FafTokenType.PASSWORD_RESET), any(), attributesMapCaptor.capture());
    Map<String, String> tokenAttributes = attributesMapCaptor.getValue();
    assertThat(tokenAttributes.size(), is(2));
    assertThat(tokenAttributes.get(KEY_USER_ID), is(String.valueOf(TEST_USERID)));
    assertThat(tokenAttributes.get(KEY_PASSWORD), is(String.valueOf(TEST_NEW_PASSWORD)));
  }

  @Test
  public void resetPasswordUnknownUsernameAndEmail() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.UNKNOWN_EMAIL_ADDRESS, TEST_CURRENT_EMAIL));

    when(emailService.findOwnerOfAddress(TEST_CURRENT_EMAIL)).thenReturn(Optional.empty());
    when(emailService.findOwnerOfAddress(TEST_CURRENT_EMAIL)).thenReturn(Optional.empty());
    instance.resetPassword(TEST_CURRENT_EMAIL, TEST_NEW_PASSWORD);
  }

  @Test
  public void claimPasswordResetToken() {
    when(fafTokenService.resolveToken(FafTokenType.PASSWORD_RESET, TOKEN_VALUE))
      .thenReturn(ImmutableMap.of(KEY_USER_ID, "5",
        KEY_PASSWORD, TEST_NEW_PASSWORD));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(accountRepository.findById(TEST_USERID)).thenReturn(Optional.of(account));

    instance.claimPasswordResetToken(TOKEN_VALUE);

    ArgumentCaptor<Account> captor = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository).save(captor.capture());
    assertEquals(captor.getValue().getPassword(), passwordEncoder.encode(TEST_NEW_PASSWORD));
    verify(anopeUserRepository).updatePassword(TEST_USERNAME, Hashing.md5().hashString(TEST_NEW_PASSWORD, StandardCharsets.UTF_8).toString());
    verifyZeroInteractions(mauticService);
  }

  @Test
  @SuppressWarnings("unchecked")
  public void buildSteamLinkUrl() {
    when(steamService.buildLoginUrl(any())).thenReturn("steamLoginUrl");

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    String url = instance.buildSteamLinkUrl(account, "http://example.com/callback");

    ArgumentCaptor<String> loginUrlCaptor = ArgumentCaptor.forClass(String.class);
    verify(steamService).buildLoginUrl(loginUrlCaptor.capture());

    ArgumentCaptor<Map<String, String>> attributesCaptor = ArgumentCaptor.forClass(Map.class);

    verify(fafTokenService).createToken(
      eq(FafTokenType.LINK_TO_STEAM),
      eq(Duration.ofHours(1)),
      attributesCaptor.capture()
    );

    assertThat(url, is("steamLoginUrl"));
    assertThat(loginUrlCaptor.getValue(), is(TOKEN_VALUE));
    assertThat(attributesCaptor.getValue(), is(ImmutableMap.of(
      KEY_USER_ID, String.valueOf(account.getId()),
      "callbackUrl", "http://example.com/callback"
    )));
  }

  @Test
  public void buildSteamLinkUrlAlreadLinked() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.STEAM_ID_UNCHANGEABLE));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(steamService.isSteamVerified(account)).thenReturn(true);

    instance.buildSteamLinkUrl(account, "http://example.com/callback");
  }

  @Test
  public void linkToSteam() {
    when(fafTokenService.resolveToken(FafTokenType.LINK_TO_STEAM, TOKEN_VALUE))
      .thenReturn(ImmutableMap.of(
        KEY_USER_ID, "5",
        KEY_STEAM_LINK_CALLBACK_URL, "callbackUrl"
      ));
    when(steamService.ownsForgedAlliance(any())).thenReturn(true);

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(accountRepository.findById(TEST_USERID)).thenReturn(Optional.of(account));
    when(accountVerificationService.findBySteamId(STEAM_ID)).thenReturn(Optional.empty());

    SteamLinkResult result = instance.linkToSteam(TOKEN_VALUE, STEAM_ID);

    assertThat(result.getCallbackUrl(), is("callbackUrl"));
    assertThat(result.getErrors(), is(empty()));
    verify(accountVerificationService).setAccountVerifiedBySteam(account, STEAM_ID);
    verifyZeroInteractions(mauticService);
  }

  @Test
  public void linkToSteamUnknownUser() {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.TOKEN_INVALID));

    when(fafTokenService.resolveToken(FafTokenType.LINK_TO_STEAM, TOKEN_VALUE)).thenReturn(ImmutableMap.of(KEY_USER_ID, "5"));
    when(accountRepository.findById(TEST_USERID)).thenReturn(Optional.empty());

    instance.linkToSteam(TOKEN_VALUE, STEAM_ID);
    verifyZeroInteractions(mauticService);
  }

  @Test
  public void linkToSteamNoGame() {
    when(fafTokenService.resolveToken(FafTokenType.LINK_TO_STEAM, TOKEN_VALUE))
      .thenReturn(ImmutableMap.of(
        KEY_USER_ID, "5",
        KEY_STEAM_LINK_CALLBACK_URL, "callbackUrl"
      ));
    when(steamService.ownsForgedAlliance(any())).thenReturn(false);
    when(accountVerificationService.findBySteamId(STEAM_ID)).thenReturn(Optional.empty());

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(accountRepository.findById(TEST_USERID)).thenReturn(Optional.of(account));

    SteamLinkResult result = instance.linkToSteam(TOKEN_VALUE, STEAM_ID);

    assertThat(result.getCallbackUrl(), is("callbackUrl"));
    assertThat(result.getErrors(), hasSize(1));
    assertThat(result.getErrors().get(0).getErrorCode(), is(ErrorCode.STEAM_LINK_NO_FA_GAME));
    assertThat(result.getErrors().get(0).getArgs(), emptyArray());
    verifyZeroInteractions(mauticService);
  }

  @Test
  public void linkToSteamAlreadyLinked() {
    when(fafTokenService.resolveToken(FafTokenType.LINK_TO_STEAM, TOKEN_VALUE))
      .thenReturn(ImmutableMap.of(
        KEY_USER_ID, "6",
        KEY_STEAM_LINK_CALLBACK_URL, "callbackUrl"
      ));
    when(steamService.ownsForgedAlliance(any())).thenReturn(true);
    Account otherUser = new Account();
    otherUser.setDisplayName("axel12");
    when(accountVerificationService.findBySteamId(STEAM_ID)).thenReturn(Optional.of(mock(AccountVerification.class)));

    Account account = createUser(TEST_USERID, TEST_USERNAME, TEST_CURRENT_PASSWORD, TEST_CURRENT_EMAIL);
    when(accountRepository.findById(6)).thenReturn(Optional.of(account));

    SteamLinkResult result = instance.linkToSteam(TOKEN_VALUE, STEAM_ID);

    assertThat(result.getCallbackUrl(), is("callbackUrl"));
    assertThat(result.getErrors(), hasSize(1));
    assertThat(result.getErrors().get(0).getErrorCode(), is(ErrorCode.STEAM_ID_ALREADY_LINKED));
    assertThat(result.getErrors().get(0).getArgs(), emptyArray());
    verifyZeroInteractions(mauticService);
  }

  @Test
  public void mauticUpdateFailureDoesNotThrowException() {
    when(mauticService.createOrUpdateContact(any(), any(), any(), any(), any()))
      .thenAnswer(invocation -> {
        CompletableFuture<Object> future = new CompletableFuture<>();
        future.completeExceptionally(new IOException("This exception should be logged but cause no harm."));
        return future;
      });

    activate();

    verify(mauticService).createOrUpdateContact(eq(TEST_NEW_EMAIL), eq(String.valueOf(TEST_USERID)), eq(TEST_USERNAME), eq(IP_ADDRESS), any(OffsetDateTime.class));
  }

  @Test
  public void migrateFromFaf_FailDuringLogin() throws Exception {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.ACCOUNT_MIGRATION_FAILED,
      "We were unable to login to FAF with your given credentials. The migration failed, no account was created."));

    Response response = mock(Response.class);
    Call call = mock(Call.class);
    when(okHttpClient.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.isSuccessful()).thenReturn(false);

    instance.migrateFromFaf("someUser", "somePassword", "someIp");
  }

  @Test
  public void migrateFromFaf_FailDuringFetchingMeResult() throws Exception {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.ACCOUNT_MIGRATION_FAILED,
      "We were unable fetch your account data from FAF. Please contact an administrator."));

    Response response = mock(Response.class, Answers.RETURNS_DEEP_STUBS);
    Call call = mock(Call.class);
    when(okHttpClient.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.isSuccessful()).thenReturn(true).thenReturn(false);
    when(response.body().string()).thenReturn("{\"access_token\":\"someToken\"}");

    instance.migrateFromFaf("someUser", "somePassword", "someIp");
  }

  @Test
  public void migrateFromFaf_AccountAlreadyMigrated() throws Exception {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.ACCOUNT_MIGRATION_FAILED,
      "This FAF account has already been migrated."));

    Response response = mock(Response.class, Answers.RETURNS_DEEP_STUBS);
    Call call = mock(Call.class);
    when(okHttpClient.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.isSuccessful()).thenReturn(true);
    when(response.body().string())
      .thenReturn("{\"access_token\":\"someToken\"}")
      /*
      {
      "data": {
          "type": "player",
          "id": "1",
          "attributes": {
              "createTime": "2013-10-19T16:46:15Z",
              "email": "user76365@example.com",
              "lastLogin": "2019-02-11T22:53:43Z",
              "login": "someFafUser",
              "recentIpAddress": "someIp",
              "steamId": null,
              "updateTime": "2019-02-11T22:53:43Z",
              "userAgent": "downlords-faf-client"
          }
       }}
       */
      .thenReturn(
        "    {\n" +
          "    \"data\": {\n" +
          "        \"type\": \"player\",\n" +
          "        \"id\": \"1\",\n" +
          "        \"attributes\": {\n" +
          "            \"createTime\": \"2013-10-19T16:46:15Z\",\n" +
          "            \"email\": \"user76365@example.com\",\n" +
          "            \"lastLogin\": \"2019-02-11T22:53:43Z\",\n" +
          "            \"login\": \"someFafUser\",\n" +
          "            \"recentIpAddress\": \"someIp\",\n" +
          "            \"steamId\": null,\n" +
          "            \"updateTime\": \"2019-02-11T22:53:43Z\",\n" +
          "            \"userAgent\": \"downlords-faf-client\"\n" +
          "        }\n" +
          "     }}\n"
      );
    when(accountVerificationService.findByFafId("1")).thenReturn(Optional.of(mock(AccountVerification.class)));

    instance.migrateFromFaf("someUser", "somePassword", "someIp");

    verify(okHttpClient, times(2)).newCall(any());
    verify(accountVerificationService).findByFafId("1");
  }

  @Test
  public void migrateFromFaf_SteamAlreadyLinked() throws Exception {
    expectedException.expect(ApiExceptionWithCode.apiExceptionWithCodeAndArgs(ErrorCode.STEAM_ID_ALREADY_LINKED));

    Response response = mock(Response.class, Answers.RETURNS_DEEP_STUBS);
    Call call = mock(Call.class);
    when(okHttpClient.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.isSuccessful()).thenReturn(true);
    when(response.body().string())
      .thenReturn("{\"access_token\":\"someToken\"}")
      /*
      {
      "data": {
          "type": "player",
          "id": "1",
          "attributes": {
              "createTime": "2013-10-19T16:46:15Z",
              "email": "user76365@example.com",
              "lastLogin": "2019-02-11T22:53:43Z",
              "login": "someFafUser",
              "recentIpAddress": "someIp",
              "steamId": "someSteamId",
              "updateTime": "2019-02-11T22:53:43Z",
              "userAgent": "downlords-faf-client"
          }
       }}
       */
      .thenReturn(
        "    {\n" +
          "    \"data\": {\n" +
          "        \"type\": \"player\",\n" +
          "        \"id\": \"1\",\n" +
          "        \"attributes\": {\n" +
          "            \"createTime\": \"2013-10-19T16:46:15Z\",\n" +
          "            \"email\": \"user76365@example.com\",\n" +
          "            \"lastLogin\": \"2019-02-11T22:53:43Z\",\n" +
          "            \"login\": \"someFafUser\",\n" +
          "            \"recentIpAddress\": \"someIp\",\n" +
          "            \"steamId\": \"someSteamId\",\n" +
          "            \"updateTime\": \"2019-02-11T22:53:43Z\",\n" +
          "            \"userAgent\": \"downlords-faf-client\"\n" +
          "        }\n" +
          "     }}\n"
      );
    when(accountVerificationService.findByFafId("1")).thenReturn(Optional.empty());
    when(accountVerificationService.findBySteamId("someSteamId")).thenReturn(Optional.of(mock(AccountVerification.class)));

    instance.migrateFromFaf("someUser", "somePassword", "someIp");

    verify(okHttpClient, times(2)).newCall(any());
    verify(accountVerificationService).findByFafId("1");
    verify(accountVerificationService).findBySteamId("someSteamId");
  }

  @Test
  public void migrateFromFaf_Success() throws Exception {
    Response response = mock(Response.class, Answers.RETURNS_DEEP_STUBS);
    Call call = mock(Call.class);
    when(okHttpClient.newCall(any())).thenReturn(call);
    when(call.execute()).thenReturn(response);
    when(response.isSuccessful()).thenReturn(true);
    when(response.body().string())
      .thenReturn("{\"access_token\":\"someToken\"}")
      /*
      {
      "data": {
          "type": "player",
          "id": "1",
          "attributes": {
              "createTime": "2013-10-19T16:46:15Z",
              "email": "user76365@example.com",
              "lastLogin": "2019-02-11T22:53:43Z",
              "login": "someFafUser",
              "recentIpAddress": "someIp",
              "steamId": "someSteamId",
              "updateTime": "2019-02-11T22:53:43Z",
              "userAgent": "downlords-faf-client"
          }
       }}
       */
      .thenReturn(
        "    {\n" +
          "    \"data\": {\n" +
          "        \"type\": \"player\",\n" +
          "        \"id\": \"1\",\n" +
          "        \"attributes\": {\n" +
          "            \"createTime\": \"2013-10-19T16:46:15Z\",\n" +
          "            \"email\": \"user76365@example.com\",\n" +
          "            \"lastLogin\": \"2019-02-11T22:53:43Z\",\n" +
          "            \"login\": \"someFafUser\",\n" +
          "            \"recentIpAddress\": \"someIp\",\n" +
          "            \"steamId\": \"someSteamId\",\n" +
          "            \"updateTime\": \"2019-02-11T22:53:43Z\",\n" +
          "            \"userAgent\": \"downlords-faf-client\"\n" +
          "        }\n" +
          "     }}\n"
      );
    when(accountVerificationService.findByFafId("1")).thenReturn(Optional.empty());
    when(accountVerificationService.findBySteamId("someSteamId")).thenReturn(Optional.empty());

    instance.migrateFromFaf("someUser", "somePassword", "someIp");

    verify(okHttpClient, times(2)).newCall(any());
    verify(accountVerificationService).findByFafId("1");
    verify(accountVerificationService).findBySteamId("someSteamId");
    verify(accountRepository).existsByDisplayNameIgnoreCase("someFafUser");
    verify(emailService).validateEmailAddress("user76365@example.com");
    verify(emailService).emailExists("user76365@example.com");

    ArgumentCaptor<Account> accountArgumentCaptor = ArgumentCaptor.forClass(Account.class);
    verify(accountRepository).save(accountArgumentCaptor.capture());
    Account account = accountArgumentCaptor.getValue();
    assertThat(account.getDisplayName(), is("someFafUser"));
    assertThat(account.getPrimaryEmailAddress(), is("user76365@example.com"));
    assertThat(account.getAccountVerifications(), containsInAnyOrder(
      new AccountVerification(account, Type.FAF, "1"),
      new AccountVerification(account, Type.STEAM, "someSteamId")));
  }
}
